import {MigrationInterface, QueryRunner} from "typeorm";

export class SporeSerialNull1645482904790 implements MigrationInterface {
    name = 'SporeSerialNull1645482904790'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "spore_nft" ALTER COLUMN "serial_number" DROP NOT NULL`);
        await queryRunner.query(`COMMENT ON COLUMN "spore_nft"."serial_number" IS NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`COMMENT ON COLUMN "spore_nft"."serial_number" IS NULL`);
        await queryRunner.query(`ALTER TABLE "spore_nft" ALTER COLUMN "serial_number" SET NOT NULL`);
    }

}
