import {MigrationInterface, QueryRunner} from "typeorm";

export class giveaway1655326392276 implements MigrationInterface {
    name = 'giveaway1655326392276'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TYPE "give_away_type_enum" AS ENUM('nft', 'sol')`);
        await queryRunner.query(`CREATE TABLE "give_away" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "description" character varying NOT NULL, "value" integer, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "finished_at" TIMESTAMP, "type" "give_away_type_enum" NOT NULL DEFAULT 'nft', "nft_id" integer array, "collections_id" integer array, "owner_user_id" integer NOT NULL, "eventId" integer, CONSTRAINT "REL_57548e5fac1f3965082507980e" UNIQUE ("eventId"), CONSTRAINT "PK_08aaf9d6f4416b66428b04370be" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "community_giveaway\`" ("community_id" integer NOT NULL, "teamId" integer NOT NULL, CONSTRAINT "PK_2bcd01eee41e73f77e856ce242b" PRIMARY KEY ("community_id", "teamId"))`);
        await queryRunner.query(`CREATE INDEX "IDX_54168fc597597f8d9973a418b1" ON "community_giveaway\`" ("community_id") `);
        await queryRunner.query(`CREATE INDEX "IDX_1ee31b4d8bf70c988759b5507a" ON "community_giveaway\`" ("teamId") `);
        await queryRunner.query(`ALTER TABLE "give_away" ADD CONSTRAINT "FK_76503c4761c6cb9c0de41b78730" FOREIGN KEY ("owner_user_id") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "give_away" ADD CONSTRAINT "FK_57548e5fac1f3965082507980eb" FOREIGN KEY ("eventId") REFERENCES "event"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "community_giveaway\`" ADD CONSTRAINT "FK_54168fc597597f8d9973a418b1f" FOREIGN KEY ("community_id") REFERENCES "give_away"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "community_giveaway\`" ADD CONSTRAINT "FK_1ee31b4d8bf70c988759b5507a2" FOREIGN KEY ("teamId") REFERENCES "team"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "community_giveaway\`" DROP CONSTRAINT "FK_1ee31b4d8bf70c988759b5507a2"`);
        await queryRunner.query(`ALTER TABLE "community_giveaway\`" DROP CONSTRAINT "FK_54168fc597597f8d9973a418b1f"`);
        await queryRunner.query(`ALTER TABLE "give_away" DROP CONSTRAINT "FK_57548e5fac1f3965082507980eb"`);
        await queryRunner.query(`ALTER TABLE "give_away" DROP CONSTRAINT "FK_76503c4761c6cb9c0de41b78730"`);
        await queryRunner.query(`DROP INDEX "IDX_1ee31b4d8bf70c988759b5507a"`);
        await queryRunner.query(`DROP INDEX "IDX_54168fc597597f8d9973a418b1"`);
        await queryRunner.query(`DROP TABLE "community_giveaway\`"`);
        await queryRunner.query(`DROP TABLE "give_away"`);
        await queryRunner.query(`DROP TYPE "give_away_type_enum"`);
    }

}
