import {MigrationInterface, QueryRunner} from "typeorm";

export class giveawayCollectionCommunity1655628951263 implements MigrationInterface {
    name = 'giveawayCollectionCommunity1655628951263'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "give_away" ADD "is_all_community_required" boolean NOT NULL DEFAULT false`);
        await queryRunner.query(`ALTER TABLE "give_away" ADD "is_all_collection_required" boolean NOT NULL DEFAULT false`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "give_away" DROP COLUMN "is_all_collection_required"`);
        await queryRunner.query(`ALTER TABLE "give_away" DROP COLUMN "is_all_community_required"`);
    }

}
