import {MigrationInterface, QueryRunner} from "typeorm";

export class userSizeShipping1655413546139 implements MigrationInterface {
    name = 'userSizeShipping1655413546139'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" ADD "shipping" character varying`);
        await queryRunner.query(`ALTER TABLE "user" ADD "hatSize" character varying`);
        await queryRunner.query(`ALTER TABLE "user" ADD "shirtSize" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "shirtSize"`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "hatSize"`);
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "shipping"`);
    }

}
