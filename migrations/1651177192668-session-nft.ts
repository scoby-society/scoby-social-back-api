import {MigrationInterface, QueryRunner} from "typeorm";

export class sessionNft1651177192668 implements MigrationInterface {
    name = 'sessionNft1651177192668'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "session" ADD "nfts_required" integer array`);
        await queryRunner.query(`ALTER TABLE "session" ADD "nft_id" integer`);
        await queryRunner.query(`ALTER TABLE "session" ADD CONSTRAINT "FK_c36439235637695abdd73024586" FOREIGN KEY ("nft_id") REFERENCES "spore_ds"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "session" DROP CONSTRAINT "FK_c36439235637695abdd73024586"`);
        await queryRunner.query(`ALTER TABLE "session" DROP COLUMN "nft_id"`);
        await queryRunner.query(`ALTER TABLE "session" DROP COLUMN "nfts_required"`);
    }

}
