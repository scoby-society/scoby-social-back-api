import {MigrationInterface, QueryRunner} from "typeorm";

export class publicWalletAdress1645142012405 implements MigrationInterface {
    name = 'publicWalletAdress1645142012405'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "public_wallet" ("id" SERIAL NOT NULL, "wallet_adress" character varying NOT NULL, "contract_adress" character varying NOT NULL, "serial_number" integer NOT NULL, "uri" character varying, CONSTRAINT "PK_c4c1ea83d054a5eb9872b6a267a" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "public_wallet"`);
    }

}
