import {MigrationInterface, QueryRunner} from "typeorm";

export class eventsUpdatePrivacy1657659777251 implements MigrationInterface {
    name = 'eventsUpdatePrivacy1657659777251'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "event" DROP COLUMN "type"`);
        await queryRunner.query(`DROP TYPE "public"."event_type_enum"`);
        await queryRunner.query(`CREATE TYPE "event_types_enum" AS ENUM('Open', 'Closed', 'Exclusive')`);
        await queryRunner.query(`ALTER TABLE "event" ADD "types" "event_types_enum" DEFAULT 'Open'`);
        await queryRunner.query(`CREATE TYPE "event_visibility_enum" AS ENUM('Listed', 'Unlisted')`);
        await queryRunner.query(`ALTER TABLE "event" ADD "visibility" "event_visibility_enum" DEFAULT 'Listed'`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "event" DROP COLUMN "visibility"`);
        await queryRunner.query(`DROP TYPE "event_visibility_enum"`);
        await queryRunner.query(`ALTER TABLE "event" DROP COLUMN "types"`);
        await queryRunner.query(`DROP TYPE "event_types_enum"`);
        await queryRunner.query(`CREATE TYPE "public"."event_type_enum" AS ENUM('Private', 'Public', 'Secret')`);
        await queryRunner.query(`ALTER TABLE "event" ADD "type" "event_type_enum" NOT NULL DEFAULT 'Public'`);
    }

}
