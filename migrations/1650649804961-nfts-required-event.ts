import {MigrationInterface, QueryRunner} from "typeorm";

export class nftsRequiredEvent1650649804961 implements MigrationInterface {
    name = 'nftsRequiredEvent1650649804961'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "event" RENAME COLUMN "nft_required" TO "nfts_required"`);
        await queryRunner.query(`ALTER TABLE "series" DROP COLUMN "nft_required"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "series" ADD "nft_required" integer`);
        await queryRunner.query(`ALTER TABLE "event" RENAME COLUMN "nfts_required" TO "nft_required"`);
    }

}
