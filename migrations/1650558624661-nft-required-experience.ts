import {MigrationInterface, QueryRunner} from "typeorm";

export class nftRequiredExperience1650558624661 implements MigrationInterface {
    name = 'nftRequiredExperience1650558624661'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "event" ADD "nft_required" integer`);
        await queryRunner.query(`ALTER TABLE "series" ADD "nft_required" integer`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "series" DROP COLUMN "nft_required"`);
        await queryRunner.query(`ALTER TABLE "event" DROP COLUMN "nft_required"`);
    }

}
