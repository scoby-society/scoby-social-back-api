import {MigrationInterface, QueryRunner} from "typeorm";

export class giveawaySuscribe1655394095379 implements MigrationInterface {
    name = 'giveawaySuscribe1655394095379'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "giveaway_subscribed_users" ("giveaway_id" integer NOT NULL, "user_id" integer NOT NULL, CONSTRAINT "PK_755647dd78b25a94d7bec04dc5a" PRIMARY KEY ("giveaway_id", "user_id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_f2d21e8170bc2a6c60310b62f2" ON "giveaway_subscribed_users" ("giveaway_id") `);
        await queryRunner.query(`CREATE INDEX "IDX_8a4c0ce6714bdb57a06befa841" ON "giveaway_subscribed_users" ("user_id") `);
        await queryRunner.query(`ALTER TABLE "giveaway_subscribed_users" ADD CONSTRAINT "FK_f2d21e8170bc2a6c60310b62f2a" FOREIGN KEY ("giveaway_id") REFERENCES "give_away"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "giveaway_subscribed_users" ADD CONSTRAINT "FK_8a4c0ce6714bdb57a06befa8416" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "giveaway_subscribed_users" DROP CONSTRAINT "FK_8a4c0ce6714bdb57a06befa8416"`);
        await queryRunner.query(`ALTER TABLE "giveaway_subscribed_users" DROP CONSTRAINT "FK_f2d21e8170bc2a6c60310b62f2a"`);
        await queryRunner.query(`DROP INDEX "IDX_8a4c0ce6714bdb57a06befa841"`);
        await queryRunner.query(`DROP INDEX "IDX_f2d21e8170bc2a6c60310b62f2"`);
        await queryRunner.query(`DROP TABLE "giveaway_subscribed_users"`);
    }

}
