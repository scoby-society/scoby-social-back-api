import {MigrationInterface, QueryRunner} from "typeorm";

export class poolWalletAdress1652112721612 implements MigrationInterface {
    name = 'poolWalletAdress1652112721612'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "pool" ADD "wallet_adress" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "pool" DROP COLUMN "wallet_adress"`);
    }

}
