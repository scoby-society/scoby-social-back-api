import {MigrationInterface, QueryRunner} from "typeorm";

export class updateCollectionsProjects1657035023846 implements MigrationInterface {
    name = 'updateCollectionsProjects1657035023846'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "projects_subscribed_users" ("project_id" integer NOT NULL, "user_id" integer NOT NULL, CONSTRAINT "PK_845aec50e7b971e8dbda868c212" PRIMARY KEY ("project_id", "user_id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_1662898b862146e99528426b2e" ON "projects_subscribed_users" ("project_id") `);
        await queryRunner.query(`CREATE INDEX "IDX_375b54ae7bd4c34a7f346e2ca0" ON "projects_subscribed_users" ("user_id") `);
        await queryRunner.query(`ALTER TABLE "projects_subscribed_users" ADD CONSTRAINT "FK_1662898b862146e99528426b2e3" FOREIGN KEY ("project_id") REFERENCES "projects"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "projects_subscribed_users" ADD CONSTRAINT "FK_375b54ae7bd4c34a7f346e2ca0f" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "projects_subscribed_users" DROP CONSTRAINT "FK_375b54ae7bd4c34a7f346e2ca0f"`);
        await queryRunner.query(`ALTER TABLE "projects_subscribed_users" DROP CONSTRAINT "FK_1662898b862146e99528426b2e3"`);
        await queryRunner.query(`DROP INDEX "IDX_375b54ae7bd4c34a7f346e2ca0"`);
        await queryRunner.query(`DROP INDEX "IDX_1662898b862146e99528426b2e"`);
        await queryRunner.query(`DROP TABLE "projects_subscribed_users"`);
    }

}
