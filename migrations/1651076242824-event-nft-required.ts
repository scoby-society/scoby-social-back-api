import {MigrationInterface, QueryRunner} from "typeorm";

export class eventNftRequired1651076242824 implements MigrationInterface {
    name = 'eventNftRequired1651076242824'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "event" ADD "nfts_required" integer array`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "event" DROP COLUMN "nfts_required"`);
    }

}
