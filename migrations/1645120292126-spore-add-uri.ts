import {MigrationInterface, QueryRunner} from "typeorm";

export class sporeAddUri1645120292126 implements MigrationInterface {
    name = 'sporeAddUri1645120292126'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "spore_nft" ADD "uri" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "spore_nft" DROP COLUMN "uri"`);
    }

}
