import {MigrationInterface, QueryRunner} from "typeorm";

export class SporeAddUrl1645222591633 implements MigrationInterface {
    name = 'SporeAddUrl1645222591633'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "public_wallet" ADD "url_image" character varying`);
        await queryRunner.query(`ALTER TABLE "spore_nft" ADD "url_image" character varying`);
        await queryRunner.query(`ALTER TABLE "spore_nft" DROP CONSTRAINT "FK_adfe124860aacb6088e38d4a489"`);
        await queryRunner.query(`ALTER TABLE "spore_nft" ALTER COLUMN "user_id" DROP NOT NULL`);
        await queryRunner.query(`COMMENT ON COLUMN "spore_nft"."user_id" IS NULL`);
        await queryRunner.query(`ALTER TABLE "spore_nft" ADD CONSTRAINT "FK_adfe124860aacb6088e38d4a489" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "spore_nft" DROP CONSTRAINT "FK_adfe124860aacb6088e38d4a489"`);
        await queryRunner.query(`COMMENT ON COLUMN "spore_nft"."user_id" IS NULL`);
        await queryRunner.query(`ALTER TABLE "spore_nft" ALTER COLUMN "user_id" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "spore_nft" ADD CONSTRAINT "FK_adfe124860aacb6088e38d4a489" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "spore_nft" DROP COLUMN "url_image"`);
        await queryRunner.query(`ALTER TABLE "public_wallet" DROP COLUMN "url_image"`);
    }

}
