import {MigrationInterface, QueryRunner} from "typeorm";

export class socialNetwork1652368419553 implements MigrationInterface {
    name = 'socialNetwork1652368419553'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "social_network" ("id" SERIAL NOT NULL, "invitations_requested" integer, "invitation_sent" integer, "creator_royalties" double precision, "total_royalties" double precision, "user_id" integer NOT NULL, CONSTRAINT "PK_db57ec926ad1d548459daae6491" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "social_network" ADD CONSTRAINT "FK_6864d71af3bc09ebb721637bbd9" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "social_network" DROP CONSTRAINT "FK_6864d71af3bc09ebb721637bbd9"`);
        await queryRunner.query(`DROP TABLE "social_network"`);
    }

}
