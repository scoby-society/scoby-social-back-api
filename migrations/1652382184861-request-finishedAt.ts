import {MigrationInterface, QueryRunner} from "typeorm";

export class requestFinishedAt1652382184861 implements MigrationInterface {
    name = 'requestFinishedAt1652382184861'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "request_invitation" ADD "finished_at" TIMESTAMP`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "request_invitation" DROP COLUMN "finished_at"`);
    }

}
