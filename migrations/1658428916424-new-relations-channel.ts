import {MigrationInterface, QueryRunner} from "typeorm";

export class newRelationsChannel1658428916424 implements MigrationInterface {
    name = 'newRelationsChannel1658428916424'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "chat_experience" ADD "project_id" integer`);
        await queryRunner.query(`ALTER TABLE "chat_experience" ADD CONSTRAINT "FK_6f12b09a147400cdba8b99ea7d4" FOREIGN KEY ("project_id") REFERENCES "projects"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "chat_experience" DROP CONSTRAINT "FK_6f12b09a147400cdba8b99ea7d4"`);
        await queryRunner.query(`ALTER TABLE "chat_experience" DROP COLUMN "project_id"`);
    }

}
