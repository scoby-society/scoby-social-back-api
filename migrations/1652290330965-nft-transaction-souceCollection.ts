import {MigrationInterface, QueryRunner} from "typeorm";

export class nftTransactionSouceCollection1652290330965 implements MigrationInterface {
    name = 'nftTransactionSouceCollection1652290330965'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "nfts_transactions" ADD "source_collection" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "nfts_transactions" DROP COLUMN "source_collection"`);
    }

}
