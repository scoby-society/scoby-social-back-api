import {MigrationInterface, QueryRunner} from "typeorm";

export class blockedUsers1652909520175 implements MigrationInterface {
    name = 'blockedUsers1652909520175'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "blocked_users" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "source_user_id" integer NOT NULL, "target_user_id" integer NOT NULL, CONSTRAINT "PK_854b45d0e79d1c87930d50dfea1" PRIMARY KEY ("id", "source_user_id", "target_user_id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_18cde5aa29dfda3a470e7211c3" ON "blocked_users" ("source_user_id") `);
        await queryRunner.query(`CREATE INDEX "IDX_11e69dda4fc3314d0279043029" ON "blocked_users" ("target_user_id") `);
        await queryRunner.query(`ALTER TABLE "blocked_users" ADD CONSTRAINT "FK_18cde5aa29dfda3a470e7211c34" FOREIGN KEY ("source_user_id") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "blocked_users" ADD CONSTRAINT "FK_11e69dda4fc3314d02790430292" FOREIGN KEY ("target_user_id") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "blocked_users" DROP CONSTRAINT "FK_11e69dda4fc3314d02790430292"`);
        await queryRunner.query(`ALTER TABLE "blocked_users" DROP CONSTRAINT "FK_18cde5aa29dfda3a470e7211c34"`);
        await queryRunner.query(`DROP INDEX "IDX_11e69dda4fc3314d0279043029"`);
        await queryRunner.query(`DROP INDEX "IDX_18cde5aa29dfda3a470e7211c3"`);
        await queryRunner.query(`DROP TABLE "blocked_users"`);
    }

}
