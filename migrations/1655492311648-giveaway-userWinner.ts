import {MigrationInterface, QueryRunner} from "typeorm";

export class giveawayUserWinner1655492311648 implements MigrationInterface {
    name = 'giveawayUserWinner1655492311648'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "give_away" ADD "winner_user_id" integer`);
        await queryRunner.query(`ALTER TABLE "give_away" ADD CONSTRAINT "FK_7ee96824c876c747d4694d03260" FOREIGN KEY ("winner_user_id") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "give_away" DROP CONSTRAINT "FK_7ee96824c876c747d4694d03260"`);
        await queryRunner.query(`ALTER TABLE "give_away" DROP COLUMN "winner_user_id"`);
    }

}
