import {MigrationInterface, QueryRunner} from "typeorm";

export class updateRelationsSporeMany1655749453570 implements MigrationInterface {
    name = 'updateRelationsSporeMany1655749453570'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "projects" DROP CONSTRAINT "FK_28d621dbefb203e37d23d12a6c7"`);
        await queryRunner.query(`DROP INDEX "IDX_28d621dbefb203e37d23d12a6c"`);
        await queryRunner.query(`CREATE TABLE "project_nft" ("project_id" integer NOT NULL, "nft_id" integer NOT NULL, CONSTRAINT "PK_559d9efcfc825b2cff76affceb2" PRIMARY KEY ("project_id", "nft_id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_0a19d1478ec3779126b52768ca" ON "project_nft" ("project_id") `);
        await queryRunner.query(`CREATE INDEX "IDX_9e67d16853235798e4a7d9e842" ON "project_nft" ("nft_id") `);
        await queryRunner.query(`ALTER TABLE "projects" DROP COLUMN "id_nft"`);
        await queryRunner.query(`ALTER TABLE "project_nft" ADD CONSTRAINT "FK_0a19d1478ec3779126b52768cab" FOREIGN KEY ("project_id") REFERENCES "projects"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "project_nft" ADD CONSTRAINT "FK_9e67d16853235798e4a7d9e8429" FOREIGN KEY ("nft_id") REFERENCES "spore_ds"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "project_nft" DROP CONSTRAINT "FK_9e67d16853235798e4a7d9e8429"`);
        await queryRunner.query(`ALTER TABLE "project_nft" DROP CONSTRAINT "FK_0a19d1478ec3779126b52768cab"`);
        await queryRunner.query(`ALTER TABLE "projects" ADD "id_nft" integer`);
        await queryRunner.query(`DROP INDEX "IDX_9e67d16853235798e4a7d9e842"`);
        await queryRunner.query(`DROP INDEX "IDX_0a19d1478ec3779126b52768ca"`);
        await queryRunner.query(`DROP TABLE "project_nft"`);
        await queryRunner.query(`CREATE INDEX "IDX_28d621dbefb203e37d23d12a6c" ON "projects" ("id_nft") `);
        await queryRunner.query(`ALTER TABLE "projects" ADD CONSTRAINT "FK_28d621dbefb203e37d23d12a6c7" FOREIGN KEY ("id_nft") REFERENCES "spore_ds"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

}
