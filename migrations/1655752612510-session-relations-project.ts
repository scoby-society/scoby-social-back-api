import {MigrationInterface, QueryRunner} from "typeorm";

export class sessionRelationsProject1655752612510 implements MigrationInterface {
    name = 'sessionRelationsProject1655752612510'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "session" ADD "project_id" integer`);
        await queryRunner.query(`ALTER TABLE "session" ADD CONSTRAINT "FK_e50d5eb396a245c89208650a5f4" FOREIGN KEY ("project_id") REFERENCES "projects"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "session" DROP CONSTRAINT "FK_e50d5eb396a245c89208650a5f4"`);
        await queryRunner.query(`ALTER TABLE "session" DROP COLUMN "project_id"`);
    }

}
