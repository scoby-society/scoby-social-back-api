import {MigrationInterface, QueryRunner} from "typeorm";

export class nftsTransactionWallets1652387311547 implements MigrationInterface {
    name = 'nftsTransactionWallets1652387311547'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "nfts_transactions" ADD "wallet_scoby_source" character varying`);
        await queryRunner.query(`ALTER TABLE "nfts_transactions" ADD "wallet_creator_scout_source" character varying`);
        await queryRunner.query(`ALTER TABLE "nfts_transactions" ADD "wallet_collector_scout_source" character varying`);
        await queryRunner.query(`ALTER TABLE "nfts_transactions" ADD "wallet_connector_scout_source" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "nfts_transactions" DROP COLUMN "wallet_connector_scout_source"`);
        await queryRunner.query(`ALTER TABLE "nfts_transactions" DROP COLUMN "wallet_collector_scout_source"`);
        await queryRunner.query(`ALTER TABLE "nfts_transactions" DROP COLUMN "wallet_creator_scout_source"`);
        await queryRunner.query(`ALTER TABLE "nfts_transactions" DROP COLUMN "wallet_scoby_source"`);
    }

}
