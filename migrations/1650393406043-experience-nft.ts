import {MigrationInterface, QueryRunner} from "typeorm";

export class experienceNft1650393406043 implements MigrationInterface {
    name = 'experienceNft1650393406043'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "event" ADD "nft_id" integer`);
        await queryRunner.query(`ALTER TABLE "series" ADD "nft_id" integer`);
        await queryRunner.query(`ALTER TABLE "event" ADD CONSTRAINT "FK_9380d479563e5a664759359470a" FOREIGN KEY ("nft_id") REFERENCES "spore_ds"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "series" ADD CONSTRAINT "FK_87e7bd8ddc4d645c7dfd60b7f0b" FOREIGN KEY ("nft_id") REFERENCES "spore_ds"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "series" DROP CONSTRAINT "FK_87e7bd8ddc4d645c7dfd60b7f0b"`);
        await queryRunner.query(`ALTER TABLE "event" DROP CONSTRAINT "FK_9380d479563e5a664759359470a"`);
        await queryRunner.query(`ALTER TABLE "series" DROP COLUMN "nft_id"`);
        await queryRunner.query(`ALTER TABLE "event" DROP COLUMN "nft_id"`);
    }

}
