import {MigrationInterface, QueryRunner} from "typeorm";

export class eventGiveaway1655812632594 implements MigrationInterface {
    name = 'eventGiveaway1655812632594'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "event" ADD "giveAwayId" integer`);
        await queryRunner.query(`ALTER TABLE "event" ADD CONSTRAINT "UQ_68de264a81ccaa194d9821fbb12" UNIQUE ("giveAwayId")`);
        await queryRunner.query(`ALTER TABLE "event" ADD CONSTRAINT "FK_68de264a81ccaa194d9821fbb12" FOREIGN KEY ("giveAwayId") REFERENCES "give_away"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "event" DROP CONSTRAINT "FK_68de264a81ccaa194d9821fbb12"`);
        await queryRunner.query(`ALTER TABLE "event" DROP CONSTRAINT "UQ_68de264a81ccaa194d9821fbb12"`);
        await queryRunner.query(`ALTER TABLE "event" DROP COLUMN "giveAwayId"`);
    }

}
