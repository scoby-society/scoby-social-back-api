import {MigrationInterface, QueryRunner} from "typeorm";

export class sporeDsUpdatedField1648740588856 implements MigrationInterface {
    name = 'sporeDsUpdatedField1648740588856'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "spore_ds" ADD "symbol" character varying`);
        await queryRunner.query(`ALTER TABLE "spore_ds" ADD "name" character varying`);
        await queryRunner.query(`ALTER TABLE "spore_ds" ADD "description" character varying`);
        await queryRunner.query(`ALTER TABLE "spore_ds" ADD "web_site" character varying`);
        await queryRunner.query(`ALTER TABLE "spore_ds" ADD "background" character varying`);
        await queryRunner.query(`ALTER TABLE "spore_ds" ADD "meta_data" character varying`);
        await queryRunner.query(`ALTER TABLE "spore_ds" ADD "token_id" character varying`);
        await queryRunner.query(`ALTER TABLE "spore_ds" ADD "block_chain" character varying`);
        await queryRunner.query(`ALTER TABLE "spore_ds" ADD "effect" character varying`);
        await queryRunner.query(`ALTER TABLE "spore_ds" ADD "creator" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "spore_ds" DROP COLUMN "creator"`);
        await queryRunner.query(`ALTER TABLE "spore_ds" DROP COLUMN "effect"`);
        await queryRunner.query(`ALTER TABLE "spore_ds" DROP COLUMN "block_chain"`);
        await queryRunner.query(`ALTER TABLE "spore_ds" DROP COLUMN "token_id"`);
        await queryRunner.query(`ALTER TABLE "spore_ds" DROP COLUMN "meta_data"`);
        await queryRunner.query(`ALTER TABLE "spore_ds" DROP COLUMN "background"`);
        await queryRunner.query(`ALTER TABLE "spore_ds" DROP COLUMN "web_site"`);
        await queryRunner.query(`ALTER TABLE "spore_ds" DROP COLUMN "description"`);
        await queryRunner.query(`ALTER TABLE "spore_ds" DROP COLUMN "name"`);
        await queryRunner.query(`ALTER TABLE "spore_ds" DROP COLUMN "symbol"`);
    }

}
