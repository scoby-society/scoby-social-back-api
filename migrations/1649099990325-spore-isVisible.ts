import {MigrationInterface, QueryRunner} from "typeorm";

export class sporeIsVisible1649099990325 implements MigrationInterface {
    name = 'sporeIsVisible1649099990325'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "spore_nft" ADD "is_visible" boolean NOT NULL DEFAULT true`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "spore_nft" DROP COLUMN "is_visible"`);
    }

}
