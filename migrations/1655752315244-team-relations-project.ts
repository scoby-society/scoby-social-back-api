import {MigrationInterface, QueryRunner} from "typeorm";

export class teamRelationsProject1655752315244 implements MigrationInterface {
    name = 'teamRelationsProject1655752315244'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "team" ADD "project_id" integer`);
        await queryRunner.query(`ALTER TABLE "team" ADD CONSTRAINT "FK_7fa4b2b073ef1e1e786b912eb79" FOREIGN KEY ("project_id") REFERENCES "projects"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "team" DROP CONSTRAINT "FK_7fa4b2b073ef1e1e786b912eb79"`);
        await queryRunner.query(`ALTER TABLE "team" DROP COLUMN "project_id"`);
    }

}
