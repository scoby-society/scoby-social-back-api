import {MigrationInterface, QueryRunner} from "typeorm";

export class referredUsers1656329098681 implements MigrationInterface {
    name = 'referredUsers1656329098681'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "referred" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "source_user_id" integer NOT NULL, "target_user_id" integer NOT NULL, CONSTRAINT "PK_1a7ca20c4dc7cf4f8b7dc69e641" PRIMARY KEY ("id", "source_user_id", "target_user_id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_f8edac2af08781caa78436867d" ON "referred" ("source_user_id") `);
        await queryRunner.query(`CREATE INDEX "IDX_456e376d58954d63be61da5bb6" ON "referred" ("target_user_id") `);
        await queryRunner.query(`ALTER TABLE "referred" ADD CONSTRAINT "FK_f8edac2af08781caa78436867d4" FOREIGN KEY ("source_user_id") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "referred" ADD CONSTRAINT "FK_456e376d58954d63be61da5bb6a" FOREIGN KEY ("target_user_id") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "referred" DROP CONSTRAINT "FK_456e376d58954d63be61da5bb6a"`);
        await queryRunner.query(`ALTER TABLE "referred" DROP CONSTRAINT "FK_f8edac2af08781caa78436867d4"`);
        await queryRunner.query(`DROP INDEX "IDX_456e376d58954d63be61da5bb6"`);
        await queryRunner.query(`DROP INDEX "IDX_f8edac2af08781caa78436867d"`);
        await queryRunner.query(`DROP TABLE "referred"`);
    }

}
