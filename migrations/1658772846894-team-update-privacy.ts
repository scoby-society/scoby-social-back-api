import {MigrationInterface, QueryRunner} from "typeorm";

export class teamUpdatePrivacy1658772846894 implements MigrationInterface {
    name = 'teamUpdatePrivacy1658772846894'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "team" DROP COLUMN "teamType"`);
        await queryRunner.query(`DROP TYPE "public"."team_teamtype_enum"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TYPE "public"."team_teamtype_enum" AS ENUM('Private', 'Public', 'Secret')`);
        await queryRunner.query(`ALTER TABLE "team" ADD "teamType" "team_teamtype_enum" NOT NULL DEFAULT 'Public'`);
    }

}
