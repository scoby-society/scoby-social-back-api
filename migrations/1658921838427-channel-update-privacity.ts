import {MigrationInterface, QueryRunner} from "typeorm";

export class channelUpdatePrivacity1658921838427 implements MigrationInterface {
    name = 'channelUpdatePrivacity1658921838427'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "chat_experience" DROP COLUMN "type"`);
        await queryRunner.query(`DROP TYPE "public"."chat_experience_type_enum"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TYPE "public"."chat_experience_type_enum" AS ENUM('Private', 'Public', 'Secret')`);
        await queryRunner.query(`ALTER TABLE "chat_experience" ADD "type" "chat_experience_type_enum" NOT NULL DEFAULT 'Public'`);
    }

}
