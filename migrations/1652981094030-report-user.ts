import {MigrationInterface, QueryRunner} from "typeorm";

export class reportUser1652981094030 implements MigrationInterface {
    name = 'reportUser1652981094030'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "report_user" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "source_user_id" integer NOT NULL, "target_user_id" integer NOT NULL, CONSTRAINT "PK_1ac7879de6b1cfd679bcc722920" PRIMARY KEY ("id", "source_user_id", "target_user_id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_217c4db9638b96e426c71572a2" ON "report_user" ("source_user_id") `);
        await queryRunner.query(`CREATE INDEX "IDX_a89459efde68efa15b03738e76" ON "report_user" ("target_user_id") `);
        await queryRunner.query(`ALTER TABLE "report_user" ADD CONSTRAINT "FK_217c4db9638b96e426c71572a27" FOREIGN KEY ("source_user_id") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "report_user" ADD CONSTRAINT "FK_a89459efde68efa15b03738e769" FOREIGN KEY ("target_user_id") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "report_user" DROP CONSTRAINT "FK_a89459efde68efa15b03738e769"`);
        await queryRunner.query(`ALTER TABLE "report_user" DROP CONSTRAINT "FK_217c4db9638b96e426c71572a27"`);
        await queryRunner.query(`DROP INDEX "IDX_a89459efde68efa15b03738e76"`);
        await queryRunner.query(`DROP INDEX "IDX_217c4db9638b96e426c71572a2"`);
        await queryRunner.query(`DROP TABLE "report_user"`);
    }

}
