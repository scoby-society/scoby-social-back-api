import {MigrationInterface, QueryRunner} from "typeorm";

export class poolRoyalties1658951358278 implements MigrationInterface {
    name = 'poolRoyalties1658951358278'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "pool" ADD "royalties" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "pool" DROP COLUMN "royalties"`);
    }

}
