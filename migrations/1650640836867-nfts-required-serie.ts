import {MigrationInterface, QueryRunner} from "typeorm";

export class nftsRequiredSerie1650640836867 implements MigrationInterface {
    name = 'nftsRequiredSerie1650640836867'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "series" ADD "nfts_required" integer array`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "series" DROP COLUMN "nfts_required"`);
    }

}
