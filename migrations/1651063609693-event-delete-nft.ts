import {MigrationInterface, QueryRunner} from "typeorm";

export class eventDeleteNft1651063609693 implements MigrationInterface {
    name = 'eventDeleteNft1651063609693'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "event" DROP COLUMN "nfts_required"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "event" ADD "nfts_required" integer`);
    }

}
