import {MigrationInterface, QueryRunner} from "typeorm";

export class nftsTransactionsCreator1652441661362 implements MigrationInterface {
    name = 'nftsTransactionsCreator1652441661362'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "nfts_transactions" ADD "creator" character varying`);
        await queryRunner.query(`ALTER TABLE "nfts_transactions" ADD "wallet_creator" character varying`);
        await queryRunner.query(`ALTER TABLE "nfts_transactions" ADD "wallet_creator_source" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "nfts_transactions" DROP COLUMN "wallet_creator_source"`);
        await queryRunner.query(`ALTER TABLE "nfts_transactions" DROP COLUMN "wallet_creator"`);
        await queryRunner.query(`ALTER TABLE "nfts_transactions" DROP COLUMN "creator"`);
    }

}
