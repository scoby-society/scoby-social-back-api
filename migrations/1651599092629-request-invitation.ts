import {MigrationInterface, QueryRunner} from "typeorm";

export class requestInvitation1651599092629 implements MigrationInterface {
    name = 'requestInvitation1651599092629'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "request_invitation" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "source_user_id" integer NOT NULL, "target_user_id" integer NOT NULL, CONSTRAINT "PK_2af134f584cd74072c6a0ddaf6c" PRIMARY KEY ("id", "source_user_id", "target_user_id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_d56e9c8b078c2998bd07f372f8" ON "request_invitation" ("source_user_id") `);
        await queryRunner.query(`CREATE INDEX "IDX_1682c4414d4b1303384ca65181" ON "request_invitation" ("target_user_id") `);
        await queryRunner.query(`ALTER TABLE "request_invitation" ADD CONSTRAINT "FK_d56e9c8b078c2998bd07f372f8e" FOREIGN KEY ("source_user_id") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "request_invitation" ADD CONSTRAINT "FK_1682c4414d4b1303384ca651812" FOREIGN KEY ("target_user_id") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "request_invitation" DROP CONSTRAINT "FK_1682c4414d4b1303384ca651812"`);
        await queryRunner.query(`ALTER TABLE "request_invitation" DROP CONSTRAINT "FK_d56e9c8b078c2998bd07f372f8e"`);
        await queryRunner.query(`DROP INDEX "IDX_1682c4414d4b1303384ca65181"`);
        await queryRunner.query(`DROP INDEX "IDX_d56e9c8b078c2998bd07f372f8"`);
        await queryRunner.query(`DROP TABLE "request_invitation"`);
    }

}
