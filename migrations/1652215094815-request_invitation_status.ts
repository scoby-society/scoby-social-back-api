import {MigrationInterface, QueryRunner} from "typeorm";

export class requestInvitationStatus1652215094815 implements MigrationInterface {
    name = 'requestInvitationStatus1652215094815'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "request_invitation" ADD "invitation_status" boolean NOT NULL DEFAULT false`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "request_invitation" DROP COLUMN "invitation_status"`);
    }

}
