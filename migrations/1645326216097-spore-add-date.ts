import {MigrationInterface, QueryRunner} from "typeorm";

export class sporeAddDate1645326216097 implements MigrationInterface {
    name = 'sporeAddDate1645326216097'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "spore_nft" ADD "created_at" TIMESTAMP DEFAULT now()`);
        await queryRunner.query(`ALTER TABLE "spore_nft" ADD "updated_at" TIMESTAMP NOT NULL DEFAULT now()`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "spore_nft" DROP COLUMN "updated_at"`);
        await queryRunner.query(`ALTER TABLE "spore_nft" DROP COLUMN "created_at"`);
    }

}
