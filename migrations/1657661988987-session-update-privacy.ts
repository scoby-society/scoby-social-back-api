import {MigrationInterface, QueryRunner} from "typeorm";

export class sessionUpdatePrivacy1657661988987 implements MigrationInterface {
    name = 'sessionUpdatePrivacy1657661988987'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "session" DROP COLUMN "type"`);
        await queryRunner.query(`DROP TYPE "public"."session_type_enum"`);
        await queryRunner.query(`CREATE TYPE "session_types_enum" AS ENUM('Open', 'Closed', 'Exclusive')`);
        await queryRunner.query(`ALTER TABLE "session" ADD "types" "session_types_enum" DEFAULT 'Open'`);
        await queryRunner.query(`CREATE TYPE "session_visibility_enum" AS ENUM('Listed', 'Unlisted')`);
        await queryRunner.query(`ALTER TABLE "session" ADD "visibility" "session_visibility_enum" DEFAULT 'Listed'`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "session" DROP COLUMN "visibility"`);
        await queryRunner.query(`DROP TYPE "session_visibility_enum"`);
        await queryRunner.query(`ALTER TABLE "session" DROP COLUMN "types"`);
        await queryRunner.query(`DROP TYPE "session_types_enum"`);
        await queryRunner.query(`CREATE TYPE "public"."session_type_enum" AS ENUM('Private', 'Public', 'Secret')`);
        await queryRunner.query(`ALTER TABLE "session" ADD "type" "session_type_enum" NOT NULL DEFAULT 'Public'`);
    }

}
