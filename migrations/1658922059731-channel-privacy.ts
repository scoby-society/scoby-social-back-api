import {MigrationInterface, QueryRunner} from "typeorm";

export class channelPrivacy1658922059731 implements MigrationInterface {
    name = 'channelPrivacy1658922059731'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TYPE "chat_experience_types_enum" AS ENUM('Open', 'Closed', 'Exclusive')`);
        await queryRunner.query(`ALTER TABLE "chat_experience" ADD "types" "chat_experience_types_enum" NOT NULL DEFAULT 'Open'`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "chat_experience" DROP COLUMN "types"`);
        await queryRunner.query(`DROP TYPE "chat_experience_types_enum"`);
    }

}
