import {MigrationInterface, QueryRunner} from "typeorm";

export class projects1655242578279 implements MigrationInterface {
    name = 'projects1655242578279'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "projects" ("id" SERIAL NOT NULL, "name" character varying, "description" character varying, "cover_image" character varying, "tile" character varying, "join" character varying, "project" character varying, "created_at" TIMESTAMP DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "user_id" integer, "id_nft" integer, CONSTRAINT "PK_6271df0a7aed1d6c0691ce6ac50" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_28d621dbefb203e37d23d12a6c" ON "projects" ("id_nft") `);
        await queryRunner.query(`ALTER TABLE "projects" ADD CONSTRAINT "FK_bd55b203eb9f92b0c8390380010" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "projects" ADD CONSTRAINT "FK_28d621dbefb203e37d23d12a6c7" FOREIGN KEY ("id_nft") REFERENCES "spore_ds"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "projects" DROP CONSTRAINT "FK_28d621dbefb203e37d23d12a6c7"`);
        await queryRunner.query(`ALTER TABLE "projects" DROP CONSTRAINT "FK_bd55b203eb9f92b0c8390380010"`);
        await queryRunner.query(`DROP INDEX "IDX_28d621dbefb203e37d23d12a6c"`);
        await queryRunner.query(`DROP TABLE "projects"`);
    }

}
