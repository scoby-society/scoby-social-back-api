import {MigrationInterface, QueryRunner} from "typeorm";

export class chatExperience1652559634982 implements MigrationInterface {
    name = 'chatExperience1652559634982'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TYPE "chat_experience_type_enum" AS ENUM('Private', 'Public', 'Secret')`);
        await queryRunner.query(`CREATE TABLE "chat_experience" ("id" SERIAL NOT NULL, "title" character varying NOT NULL, "description" character varying NOT NULL, "background_image" character varying, "avatar" character varying, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "finished_at" TIMESTAMP, "nfts_required" integer array, "type" "chat_experience_type_enum" NOT NULL DEFAULT 'Public', "owner_user_id" integer NOT NULL, "nft_id" integer, CONSTRAINT "PK_4e531d583471e01d9ea80f28b69" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "chats_topics" ("chat_id" integer NOT NULL, "topic_id" integer NOT NULL, CONSTRAINT "PK_79d76a30af5575653b7b0b26994" PRIMARY KEY ("chat_id", "topic_id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_f5ba23305ab1a8d5e74fe9889a" ON "chats_topics" ("chat_id") `);
        await queryRunner.query(`CREATE INDEX "IDX_2a858ffd8738b8d9a1369769c8" ON "chats_topics" ("topic_id") `);
        await queryRunner.query(`CREATE TABLE "chats_subscribed_users" ("chat_id" integer NOT NULL, "user_id" integer NOT NULL, CONSTRAINT "PK_4ad64762674da38b93eb301d343" PRIMARY KEY ("chat_id", "user_id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_4ddb3a396c65573242ff224c2f" ON "chats_subscribed_users" ("chat_id") `);
        await queryRunner.query(`CREATE INDEX "IDX_9647fcb3c8b7ffaffb532af312" ON "chats_subscribed_users" ("user_id") `);
        await queryRunner.query(`ALTER TABLE "chat_experience" ADD CONSTRAINT "FK_a126df0258ec5c7c40ee496f671" FOREIGN KEY ("owner_user_id") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "chat_experience" ADD CONSTRAINT "FK_7060b6a3e82f0e15fa7f7300002" FOREIGN KEY ("nft_id") REFERENCES "spore_ds"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "chats_topics" ADD CONSTRAINT "FK_f5ba23305ab1a8d5e74fe9889ae" FOREIGN KEY ("chat_id") REFERENCES "chat_experience"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "chats_topics" ADD CONSTRAINT "FK_2a858ffd8738b8d9a1369769c87" FOREIGN KEY ("topic_id") REFERENCES "topic"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "chats_subscribed_users" ADD CONSTRAINT "FK_4ddb3a396c65573242ff224c2f7" FOREIGN KEY ("chat_id") REFERENCES "chat_experience"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "chats_subscribed_users" ADD CONSTRAINT "FK_9647fcb3c8b7ffaffb532af3120" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "chats_subscribed_users" DROP CONSTRAINT "FK_9647fcb3c8b7ffaffb532af3120"`);
        await queryRunner.query(`ALTER TABLE "chats_subscribed_users" DROP CONSTRAINT "FK_4ddb3a396c65573242ff224c2f7"`);
        await queryRunner.query(`ALTER TABLE "chats_topics" DROP CONSTRAINT "FK_2a858ffd8738b8d9a1369769c87"`);
        await queryRunner.query(`ALTER TABLE "chats_topics" DROP CONSTRAINT "FK_f5ba23305ab1a8d5e74fe9889ae"`);
        await queryRunner.query(`ALTER TABLE "chat_experience" DROP CONSTRAINT "FK_7060b6a3e82f0e15fa7f7300002"`);
        await queryRunner.query(`ALTER TABLE "chat_experience" DROP CONSTRAINT "FK_a126df0258ec5c7c40ee496f671"`);
        await queryRunner.query(`DROP INDEX "IDX_9647fcb3c8b7ffaffb532af312"`);
        await queryRunner.query(`DROP INDEX "IDX_4ddb3a396c65573242ff224c2f"`);
        await queryRunner.query(`DROP TABLE "chats_subscribed_users"`);
        await queryRunner.query(`DROP INDEX "IDX_2a858ffd8738b8d9a1369769c8"`);
        await queryRunner.query(`DROP INDEX "IDX_f5ba23305ab1a8d5e74fe9889a"`);
        await queryRunner.query(`DROP TABLE "chats_topics"`);
        await queryRunner.query(`DROP TABLE "chat_experience"`);
        await queryRunner.query(`DROP TYPE "chat_experience_type_enum"`);
    }

}
