import {MigrationInterface, QueryRunner} from "typeorm";

export class sporeDs1648155869823 implements MigrationInterface {
    name = 'sporeDs1648155869823'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "spore_ds" ("id" SERIAL NOT NULL, "wallet_adress" character varying NOT NULL, "contract_adress" character varying NOT NULL, "serial_number" integer, "uri" character varying, "url_image" character varying, "created_at" TIMESTAMP DEFAULT now(), "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "user_id" integer, CONSTRAINT "PK_0fb10f9aacc3c0f1520bac098e0" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "spore_ds" ADD CONSTRAINT "FK_ba851a6ad2304a6d34d5b1c130f" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "spore_ds" DROP CONSTRAINT "FK_ba851a6ad2304a6d34d5b1c130f"`);
        await queryRunner.query(`DROP TABLE "spore_ds"`);
    }

}
