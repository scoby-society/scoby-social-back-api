import {MigrationInterface, QueryRunner} from "typeorm";

export class sporeDsTrx1649270941210 implements MigrationInterface {
    name = 'sporeDsTrx1649270941210'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "spore_ds_trx" ("id" SERIAL NOT NULL, "event" character varying, "price" double precision, "from_wallet" character varying, "to_wallet" character varying, "signature" character varying NOT NULL, "instructions" character varying, "date_trx" TIMESTAMP DEFAULT now(), "spore_id" integer, CONSTRAINT "PK_cc783bfe1df20c835d7da37eabd" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "spore_ds_trx" ADD CONSTRAINT "FK_af7de28f159372bb116cc4de503" FOREIGN KEY ("spore_id") REFERENCES "spore_ds"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "spore_ds_trx" DROP CONSTRAINT "FK_af7de28f159372bb116cc4de503"`);
        await queryRunner.query(`DROP TABLE "spore_ds_trx"`);
    }

}
