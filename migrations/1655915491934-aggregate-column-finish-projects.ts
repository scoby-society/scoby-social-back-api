import {MigrationInterface, QueryRunner} from "typeorm";

export class aggregateColumnFinishProjects1655915491934 implements MigrationInterface {
    name = 'aggregateColumnFinishProjects1655915491934'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "projects" ADD "finished_at" TIMESTAMP`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "projects" DROP COLUMN "finished_at"`);
    }

}
