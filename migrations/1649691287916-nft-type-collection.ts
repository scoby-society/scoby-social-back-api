import {MigrationInterface, QueryRunner} from "typeorm";

export class nftTypeCollection1649691287916 implements MigrationInterface {
    name = 'nftTypeCollection1649691287916'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "spore_ds" ADD "type_collections" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "spore_ds" DROP COLUMN "type_collections"`);
    }

}
