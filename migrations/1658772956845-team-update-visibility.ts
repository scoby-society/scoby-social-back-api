import {MigrationInterface, QueryRunner} from "typeorm";

export class teamUpdateVisibility1658772956845 implements MigrationInterface {
    name = 'teamUpdateVisibility1658772956845'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TYPE "team_teamtype_enum" AS ENUM('Open', 'Closed', 'Exclusive')`);
        await queryRunner.query(`ALTER TABLE "team" ADD "teamType" "team_teamtype_enum" NOT NULL DEFAULT 'Open'`);
        await queryRunner.query(`CREATE TYPE "team_visibility_enum" AS ENUM('Listed', 'Unlisted')`);
        await queryRunner.query(`ALTER TABLE "team" ADD "visibility" "team_visibility_enum" DEFAULT 'Listed'`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "team" DROP COLUMN "visibility"`);
        await queryRunner.query(`DROP TYPE "team_visibility_enum"`);
        await queryRunner.query(`ALTER TABLE "team" DROP COLUMN "teamType"`);
        await queryRunner.query(`DROP TYPE "team_teamtype_enum"`);
    }

}
