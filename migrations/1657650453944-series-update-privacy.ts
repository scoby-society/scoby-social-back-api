import {MigrationInterface, QueryRunner} from "typeorm";

export class seriesUpdatePrivacy1657650453944 implements MigrationInterface {
    name = 'seriesUpdatePrivacy1657650453944'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "series" DROP COLUMN "type"`);
        await queryRunner.query(`DROP TYPE "public"."series_type_enum"`);
        await queryRunner.query(`CREATE TYPE "series_types_enum" AS ENUM('Open', 'Closed', 'Exclusive')`);
        await queryRunner.query(`ALTER TABLE "series" ADD "types" "series_types_enum" DEFAULT 'Open'`);
        await queryRunner.query(`CREATE TYPE "series_visibility_enum" AS ENUM('Listed', 'Unlisted')`);
        await queryRunner.query(`ALTER TABLE "series" ADD "visibility" "series_visibility_enum" DEFAULT 'Listed'`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "series" DROP COLUMN "visibility"`);
        await queryRunner.query(`DROP TYPE "series_visibility_enum"`);
        await queryRunner.query(`ALTER TABLE "series" DROP COLUMN "types"`);
        await queryRunner.query(`DROP TYPE "series_types_enum"`);
        await queryRunner.query(`CREATE TYPE "public"."series_type_enum" AS ENUM('Private', 'Public', 'Secret')`);
        await queryRunner.query(`ALTER TABLE "series" ADD "type" "series_type_enum" NOT NULL DEFAULT 'Public'`);
    }

}
