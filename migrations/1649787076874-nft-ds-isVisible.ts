import {MigrationInterface, QueryRunner} from "typeorm";

export class nftDsIsVisible1649787076874 implements MigrationInterface {
    name = 'nftDsIsVisible1649787076874'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "spore_ds" ADD "is_visible" boolean NOT NULL DEFAULT true`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "spore_ds" DROP COLUMN "is_visible"`);
    }

}
