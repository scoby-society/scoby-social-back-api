import {MigrationInterface, QueryRunner} from "typeorm";

export class seriesRelationsProject1655751513463 implements MigrationInterface {
    name = 'seriesRelationsProject1655751513463'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "series" ADD "project_id" integer`);
        await queryRunner.query(`ALTER TABLE "series" ADD CONSTRAINT "FK_8a870ad92351f10709f19eac19d" FOREIGN KEY ("project_id") REFERENCES "projects"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "series" DROP CONSTRAINT "FK_8a870ad92351f10709f19eac19d"`);
        await queryRunner.query(`ALTER TABLE "series" DROP COLUMN "project_id"`);
    }

}
