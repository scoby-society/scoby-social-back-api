import {MigrationInterface, QueryRunner} from "typeorm";

export class memberNetwork1652801974846 implements MigrationInterface {
    name = 'memberNetwork1652801974846'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "member_network" ("id" SERIAL NOT NULL, "total_contribution" double precision, "as_creator" double precision, "as_collector" double precision, "as_connector" double precision, "invitations_request" integer, "invitations_send" integer, "user_contribudor" integer NOT NULL, "source_user" integer NOT NULL, CONSTRAINT "PK_0f1f575c562e76a7a3d6fe897b4" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "member_network" ADD CONSTRAINT "FK_e0165b59c21f44e78035c4e8ea0" FOREIGN KEY ("user_contribudor") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "member_network" ADD CONSTRAINT "FK_8c0e5af34bcc8f168a5df328178" FOREIGN KEY ("source_user") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "member_network" DROP CONSTRAINT "FK_8c0e5af34bcc8f168a5df328178"`);
        await queryRunner.query(`ALTER TABLE "member_network" DROP CONSTRAINT "FK_e0165b59c21f44e78035c4e8ea0"`);
        await queryRunner.query(`DROP TABLE "member_network"`);
    }

}
