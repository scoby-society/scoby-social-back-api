import {MigrationInterface, QueryRunner} from "typeorm";

export class sporeNft1644529849992 implements MigrationInterface {
    name = 'sporeNft1644529849992'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "spore_nft" ("id" SERIAL NOT NULL, "wallet_adress" character varying NOT NULL, "contract_adress" character varying NOT NULL, "serial_number" integer NOT NULL, "user_id" integer NOT NULL, CONSTRAINT "PK_fe38c9578a900b416cfee956236" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "spore_nft" ADD CONSTRAINT "FK_adfe124860aacb6088e38d4a489" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "spore_nft" DROP CONSTRAINT "FK_adfe124860aacb6088e38d4a489"`);
        await queryRunner.query(`DROP TABLE "spore_nft"`);
    }

}
