import {MigrationInterface, QueryRunner} from "typeorm";

export class pool1651614730407 implements MigrationInterface {
    name = 'pool1651614730407'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "pool" ("id" SERIAL NOT NULL, "title" character varying NOT NULL, "card_project_description" character varying NOT NULL, "Project_subtitle" character varying, "view_website_link" character varying NOT NULL, "view_creator_profile" character varying NOT NULL, "project_description" character varying NOT NULL, "external_link" character varying NOT NULL, "pool" character varying NOT NULL, "symbol" character varying NOT NULL, "program_id" character varying NOT NULL, "card_project_avatar" character varying, "card_project_header" character varying, "project_image" character varying, "minting_image" character varying, "avatar_creator" character varying, CONSTRAINT "PK_db1bfe411e1516c01120b85f8fe" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "pool"`);
    }

}
