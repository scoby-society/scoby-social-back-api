import {MigrationInterface, QueryRunner} from "typeorm";

export class socialNetworkConnector1652566656537 implements MigrationInterface {
    name = 'socialNetworkConnector1652566656537'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "social_network" ADD "connector_royalties" double precision`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "social_network" DROP COLUMN "connector_royalties"`);
    }

}
