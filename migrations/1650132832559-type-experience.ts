import {MigrationInterface, QueryRunner} from "typeorm";

export class typeExperience1650132832559 implements MigrationInterface {
    name = 'typeExperience1650132832559'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TYPE "session_type_enum" AS ENUM('Private', 'Public', 'Secret')`);
        await queryRunner.query(`ALTER TABLE "session" ADD "type" "session_type_enum" NOT NULL DEFAULT 'Public'`);
        await queryRunner.query(`CREATE TYPE "event_type_enum" AS ENUM('Private', 'Public', 'Secret')`);
        await queryRunner.query(`ALTER TABLE "event" ADD "type" "event_type_enum" NOT NULL DEFAULT 'Public'`);
        await queryRunner.query(`CREATE TYPE "series_type_enum" AS ENUM('Private', 'Public', 'Secret')`);
        await queryRunner.query(`ALTER TABLE "series" ADD "type" "series_type_enum" NOT NULL DEFAULT 'Public'`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "series" DROP COLUMN "type"`);
        await queryRunner.query(`DROP TYPE "series_type_enum"`);
        await queryRunner.query(`ALTER TABLE "event" DROP COLUMN "type"`);
        await queryRunner.query(`DROP TYPE "event_type_enum"`);
        await queryRunner.query(`ALTER TABLE "session" DROP COLUMN "type"`);
        await queryRunner.query(`DROP TYPE "session_type_enum"`);
    }

}
