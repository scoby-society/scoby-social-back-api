import {MigrationInterface, QueryRunner} from "typeorm";

export class poolProjectImage1652888544920 implements MigrationInterface {
    name = 'poolProjectImage1652888544920'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "pool" ADD "project_image_left" character varying`);
        await queryRunner.query(`ALTER TABLE "pool" ADD "project_image_right" character varying`);
        await queryRunner.query(`ALTER TABLE "pool" ADD "project_image_top" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "pool" DROP COLUMN "project_image_top"`);
        await queryRunner.query(`ALTER TABLE "pool" DROP COLUMN "project_image_right"`);
        await queryRunner.query(`ALTER TABLE "pool" DROP COLUMN "project_image_left"`);
    }

}
