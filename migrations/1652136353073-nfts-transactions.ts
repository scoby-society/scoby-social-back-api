import {MigrationInterface, QueryRunner} from "typeorm";

export class nftsTransactions1652136353073 implements MigrationInterface {
    name = 'nftsTransactions1652136353073'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "nfts_transactions" ("id" SERIAL NOT NULL, "nft_address" character varying, "signature" character varying, "date_trx" TIMESTAMP, "price" double precision, "from_wallet" character varying, "toWallet" character varying, "instructions" character varying, "event" character varying, "scoby" double precision, "wallet_scoby" character varying, "creator_scout" double precision, "wallet_creator_scout" character varying, "collector_scout" double precision, "wallet_collector_scout" character varying, "connector_scout" double precision, "wallet_connector_scout" character varying, "type_action" character varying NOT NULL, CONSTRAINT "PK_f839c7d827d021df40a84a1dc64" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "nfts_transactions"`);
    }

}
