import {MigrationInterface, QueryRunner} from "typeorm";

export class eventRelationsProject1655751865286 implements MigrationInterface {
    name = 'eventRelationsProject1655751865286'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "event" ADD "project_id" integer`);
        await queryRunner.query(`ALTER TABLE "event" ADD CONSTRAINT "FK_5ddf1fde7a9479ca096bb6a1424" FOREIGN KEY ("project_id") REFERENCES "projects"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "event" DROP CONSTRAINT "FK_5ddf1fde7a9479ca096bb6a1424"`);
        await queryRunner.query(`ALTER TABLE "event" DROP COLUMN "project_id"`);
    }

}
