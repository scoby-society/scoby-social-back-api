import {MigrationInterface, QueryRunner} from "typeorm";

export class ntfsTransactionsRemovingField1652201364779 implements MigrationInterface {
    name = 'ntfsTransactionsRemovingField1652201364779'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "nfts_transactions" DROP COLUMN "type_action"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "nfts_transactions" ADD "type_action" character varying NOT NULL`);
    }

}
