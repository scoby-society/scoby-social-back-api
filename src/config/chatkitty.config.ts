import { registerAs } from '@nestjs/config';

const { env } = process;

const chatkitty = () => ({
  username: env.CHATKITTY_CLIENT_ID as string,
  password: env.CHATKITTY_CLIENT_SECRET as string,
  idToken: Number(env.CHATKITTY_APPLICATION_ID),
  apiUrl: env.CHATKITTY_URL as string,
});

export type ChatkittyConfig = ReturnType<typeof chatkitty>;

export default registerAs('chatkitty', chatkitty);
