import { registerAs } from '@nestjs/config';

const { env } = process;

const sendinblue = () => ({
  apiKey: env.SENDINBLUE_API_KEY as string,
});

export type sendinblueConfig = ReturnType<typeof sendinblue>;

export default registerAs('sendinblue', sendinblue);
