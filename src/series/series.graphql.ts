import { ObjectType, Int, Field, InputType } from '@nestjs/graphql';
import { Topic } from '../topics/topics.graphql';
import { UserProfile } from '../users/users.graphql';
import { SessionLiveToSerie } from 'src/sessions/sessions.graphql';
import { ScheduleObject } from './schedule.graphql';
import { PagingObject } from '../lib/common/common.graphql';
import { SporeUsersDs } from 'src/spore-nft/spore.graphql';
import { ProjectView } from 'src/projects/projects.graphql';
import { Projects } from 'src/projects/projects.entity';
@ObjectType()
export class SeriesObject {
  @Field(() => Int)
  id: number;

  @Field(() => String, { nullable: true })
  calendarName?: string | null;

  @Field(() => String, { nullable: true })
  className?: string | null;

  @Field(() => String)
  seriesName: string;

  @Field(() => String)
  description: string;

  @Field()
  ownerUser: UserProfile;

  @Field(() => String, { nullable: true })
  avatar?: string | null;

  @Field(() => String, { nullable: true })
  backgroundImage?: string | null;

  @Field(() => [UserProfile])
  suscribeUsers: UserProfile[];

  @Field(() => [Topic], { nullable: true })
  topics: Topic[];

  @Field()
  createdAt: Date;

  @Field()
  updatedAt: Date;

  @Field(() => Date, { nullable: true })
  finishedAt?: Date | null;

  @Field(() => SessionLiveToSerie, { nullable: true })
  session?: SessionLiveToSerie | null;

  @Field(() => [ScheduleObject], { nullable: true })
  schedule?: ScheduleObject[] | null;

  @Field(() => Boolean)
  subscribed?: boolean | null;

  @Field(() => String)
  serieType: string;

  @Field(() => SporeUsersDs, { nullable: true })
  nft?: SporeUsersDs | null;

  @Field(() => ProjectView, { nullable: true })
  projects?: Projects | null;

  @Field(() => Boolean, { nullable: true })
  isAvailable?: boolean | true;

  @Field(() => [Number], { nullable: true })
  nftsRequired?: number[] | null;
}

@InputType()
export class SerieCreation {
  @Field(() => String, { nullable: true })
  calendarName?: string | null;

  @Field(() => String, { nullable: true })
  className?: string | null;

  @Field(() => String)
  seriesName: string;

  @Field(() => String)
  description: string;

  @Field(() => [Int])
  topics: number[];

  @Field(() => [Int], { nullable: true })
  invitedUsers: [number];

  @Field(() => Boolean, { nullable: true })
  isTeamsMembersInvited: boolean;

  @Field(() => [Int], { nullable: true })
  teamInvitation: [number];

  @Field(() => String,{nullable:true})
  serieType: string | null;

  @Field(() => Int, { nullable: true })
  nft?: number | null;

  @Field(() => [Int], { nullable: true })
  nftsInviteUser?: number[] | null;

  @Field(() => [Int], { nullable: true })
  nftsRequired?: number[] | null;

  @Field(() => Int, { nullable: true })
  projectId: number | null;

  @Field(() => String, {nullable:true})
  visibility: string | null;
}

@InputType()
export class ScheduleCreation {
  @Field(() => String)
  day: string;

  @Field(() => String)
  start: string;

  @Field(() => String)
  end: string;

  @Field(() => Int, { nullable: true })
  idSerie: number;
}

@InputType()
export class serieEdit {
  @Field(() => String, { nullable: true })
  calendarName: string;

  @Field(() => String, { nullable: true })
  className: string;

  @Field(() => String)
  seriesName: string;

  @Field(() => String)
  description: string;

  @Field(() => [Int])
  topics: number[];

  @Field(() => Int, { nullable: true })
  projectId?: number | null;
}

@ObjectType()
export class SeriesViewers extends SeriesObject {
  @Field()
  viewers: number;

  @Field(() => Boolean)
  subscribed?: boolean | null;

  @Field()
  paging: PagingObject;
}
