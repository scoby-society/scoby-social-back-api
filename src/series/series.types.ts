export enum ExperienceSerieType {
  OPEN = 'Open',
  CLOSED = 'Closed',
  EXCLUSIVE = 'Exclusive',
}
