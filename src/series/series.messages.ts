export const SERIES_ERRORS = {
  SERIE_NOT_FOUND: {
    CODE: 'ERR_SERIE_NOT_FOUND',
    MESSAGE: 'Serie not found',
  },
  SERIE_FINISHED: {
    CODE: 'ERR_SERIE_FINISHED',
    MESSAGE: 'Serie has been finished',
  },
  NOT_SERIE_OWNER: {
    CODE: 'ERR_NOT_SERIE_OWNER',
    MESSAGE: 'User is not owner of the serie',
  },
  SERIE_IS_LIVE: {
    CODE: 'ERR_SERIE_IS_LIVE',
    MESSAGE: 'The serie is now live ',
  },
  SERIE_IS_PRIVATE: {
    CODE: 'ERR_SERIE_IS_PRIVATE',
    MESSAGE: 'The user does not have an invitation to this serie',
  },
  SERIE_REQUIRED_NFT: {
    CODE: 'ERR_REQUIRED_NFT',
    MESSAGE: 'The user does not have the required nft',
  },
  SERIE_NOT_FOUND_PROJECT: {
    CODE: 'ERR_SERIE_NOT_FOUND_PROJECT',
    MESSAGE: 'the Project not Found in the serie'
  }
};
