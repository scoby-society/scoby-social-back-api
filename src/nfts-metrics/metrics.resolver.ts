import { Resolver, Query, Args } from '@nestjs/graphql';
import { MetricsServices } from './metrics.service';
import { JwtGuard } from '../lib/jwt/jwt.guard';
import { MemberNetworkObject, SocialNetworkObject } from './metrics.graphl';
import { UseGuards } from '@nestjs/common';
import { BasePayload } from '../lib/jwt/jwt.service';
import { CurrentUser } from '../lib/common/current-user.decorator';

@Resolver(() => 'ntfsTransactions')
export class MetricsResolver {
  constructor(private metricsServices: MetricsServices) {}

  @Query(() => [SocialNetworkObject])
  @UseGuards(JwtGuard)
  async getSocialNetworks(): Promise<SocialNetworkObject[]> {
    return this.metricsServices.getSocialNetworks();
  }

  @Query(() => [MemberNetworkObject])
  @UseGuards(JwtGuard)
  async getMemberNetworkById(
    @Args('id') id: number,
  ): Promise<MemberNetworkObject[]> {
    return this.metricsServices.getMemberNetworkById(id);
  }

  @Query(() => [MemberNetworkObject])
  @UseGuards(JwtGuard)
  async getAllMemberNetwork(): Promise<MemberNetworkObject[]> {
    return this.metricsServices.getAllMemberNetwork();
  }

  @Query(() => [SocialNetworkObject])
  @UseGuards(JwtGuard)
  async getSocialNetworkUser(
    @CurrentUser() currentUser: BasePayload,
    @Args('idUser', { nullable: true }) idUser?: number,
  ): Promise<SocialNetworkObject[]> {
    return this.metricsServices.getSocialNetworkUser(idUser ?? currentUser.id);
  }
}
