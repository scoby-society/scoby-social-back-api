import { Module } from '@nestjs/common';
import { JwtModule } from '../lib/jwt/jwt.module';
import { MetricsServices } from './metrics.service';
import { MetricsResolver } from './metrics.resolver';

@Module({
  imports: [JwtModule],
  providers: [MetricsServices, MetricsResolver],
  exports: [MetricsServices],
})
export class MetricsModule {}
