import { Injectable } from '@nestjs/common';
import { getRepository } from 'typeorm';
import { memberNetwork } from './memberNetwork.entity';
import { MemberNetworkObject, SocialNetworkObject } from './metrics.graphl';
import { socialNetwork } from './socialNetwork.entity';

@Injectable()
export class MetricsServices {
  async getSocialNetworks(): Promise<SocialNetworkObject[]> {
    return await getRepository(socialNetwork)
      .createQueryBuilder('social')
      .leftJoinAndSelect('social.ownerUser', 'user')
      .orderBy('social.totalRoyalties', 'DESC')
      .getMany();
  }

  async getMemberNetworkById(id: number): Promise<MemberNetworkObject[]> {
    return await getRepository(memberNetwork)
      .createQueryBuilder('member')
      .leftJoinAndSelect('member.sourceUser', 'user')
      .leftJoinAndSelect('member.userContribudor', 'userContribudor')
      .where('user.id = :id', { id })
      .orderBy('member.totalContribution', 'DESC')
      .getMany();
  }

  async getAllMemberNetwork(): Promise<MemberNetworkObject[]> {
    return await getRepository(memberNetwork)
      .createQueryBuilder('member')
      .leftJoinAndSelect('member.sourceUser', 'user')
      .leftJoinAndSelect('member.userContribudor', 'userContribudor')
      .orderBy('member.totalContribution', 'DESC')
      .getMany();
  }

  async getSocialNetworkUser(idUser: number): Promise<SocialNetworkObject[]> {
    return await getRepository(socialNetwork)
      .createQueryBuilder('social')
      .leftJoinAndSelect('social.ownerUser', 'user')
      .where('user.id = :id', { id: idUser })
      .orderBy('social.totalRoyalties', 'DESC')
      .getMany();
  }
}
