import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { User } from '../users/user.entity';

@Entity()
export class memberNetwork {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'float', name: 'total_contribution', nullable: true })
  totalContribution: number | null;

  @Column({ type: 'float', name: 'as_creator', nullable: true })
  asCreator: number | null;

  @Column({ type: 'float', name: 'as_collector', nullable: true })
  asCollector: number | null;

  @Column({ type: 'float', name: 'as_connector', nullable: true })
  asConnector: number | null;

  @Column('int', { name: 'invitations_request', nullable: true })
  invitationsRequest: number | null;

  @Column('int', { name: 'invitations_send', nullable: true })
  invitationsSend: number | null;

  @ManyToOne(() => User, { nullable: false })
  @JoinColumn({ name: 'user_contribudor' })
  userContribudor: User;

  @ManyToOne(() => User, { nullable: false })
  @JoinColumn({ name: 'source_user' })
  sourceUser: User;
}
