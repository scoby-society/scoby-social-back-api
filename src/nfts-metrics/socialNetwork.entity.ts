import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { User } from '../users/user.entity';

@Entity()
export class socialNetwork {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User, { nullable: false })
  @JoinColumn({ name: 'user_id' })
  ownerUser: User;

  @Column({ type: 'int', name: 'invitations_requested', nullable: true })
  invitationsRequested: number | null;

  @Column({ type: 'int', name: 'invitation_sent', nullable: true })
  invitationSent: number | null;

  @Column({ type: 'float', name: 'creator_royalties', nullable: true })
  creatorRoyalties: number | null;

  @Column({ type: 'float', name: 'connector_royalties', nullable: true })
  connectorRoyalties: number | null;

  @Column({ type: 'float', name: 'total_royalties', nullable: true })
  totalRoyalties: number | null;
}
