import { ObjectType, Int, Field } from '@nestjs/graphql';
import { UserProfile } from '../users/users.graphql';

@ObjectType()
export class SocialNetworkObject {
  @Field(() => Int)
  id: number;

  @Field()
  ownerUser: UserProfile;

  @Field(() => Number, { nullable: true })
  invitationsRequested?: number | null;

  @Field(() => Number, { nullable: true })
  invitationSent?: number | null;

  @Field(() => Number, { nullable: true })
  creatorRoyalties?: number | null;

  @Field(() => Number, { nullable: true })
  connectorRoyalties?: number | null;

  @Field(() => Number, { nullable: true })
  totalRoyalties?: number | null;
}

@ObjectType()
export class MemberNetworkObject {
  @Field(() => Int)
  id: number;

  @Field(() => Number, { nullable: true })
  totalContribution?: number | null;

  @Field(() => Number, { nullable: true })
  asCreator?: number | null;

  @Field(() => Number, { nullable: true })
  asCollector?: number | null;

  @Field(() => Number, { nullable: true })
  asConnector?: number | null;

  @Field(() => Number, { nullable: true })
  invitationsRequest?: number | null;

  @Field(() => Number, { nullable: true })
  invitationsSend?: number | null;

  @Field(() => UserProfile, { nullable: false })
  userContribudor?: UserProfile | null;
  
  @Field(() => UserProfile, { nullable: false })
  sourceUser?: UserProfile | null;
}
