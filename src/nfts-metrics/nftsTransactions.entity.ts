import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class nftsTransactions {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', name: 'nft_address', nullable: true })
  nftAdress: string | null;

  @Column({ type: 'varchar', name: 'signature', nullable: true })
  signature: string | null;

  @Column({ type: 'timestamp', nullable: true, name: 'date_trx' })
  dateTrx?: Date | null;

  @Column({ type: 'float', name: 'price', nullable: true })
  price: number | null;

  @Column({ type: 'varchar', name: 'from_wallet', nullable: true })
  fromWallet: string | null;

  @Column({ type: 'varchar', name: 'toWallet', nullable: true })
  toWallet: string | null;

  @Column({ type: 'varchar', name: 'instructions', nullable: true })
  instructions: string | null;

  @Column({ type: 'varchar', name: 'event', nullable: true })
  event: string | null;

  @Column({ type: 'float', name: 'scoby', nullable: true })
  scoby: number | null;

  @Column({ type: 'varchar', name: 'wallet_scoby', nullable: true })
  walletScoby: string | null;

  @Column({ type: 'float', name: 'creator_scout', nullable: true })
  creatorScout: number | null;

  @Column({ type: 'varchar', name: 'wallet_creator_scout', nullable: true })
  walletCreatorScout: string | null;

  @Column({ type: 'float', name: 'collector_scout', nullable: true })
  collectorScout: number | null;

  @Column({ type: 'varchar', name: 'wallet_collector_scout', nullable: true })
  walletCollectorScout: string | null;

  @Column({ type: 'float', name: 'connector_scout', nullable: true })
  connectorScout: number | null;

  @Column({ type: 'varchar', name: 'wallet_connector_scout', nullable: true })
  walletConnectorScout: string | null;

  @Column({ type: 'varchar', name: 'source_collection', nullable: true })
  sourceCollection: string | null;

  @Column({ type: 'varchar', name: 'wallet_scoby_source', nullable: true })
  walletScobySource: string | null;

  @Column({
    type: 'varchar',
    name: 'wallet_creator_scout_source',
    nullable: true,
  })
  walletCreatorScoutSource: string | null;

  @Column({
    type: 'varchar',
    name: 'wallet_collector_scout_source',
    nullable: true,
  })
  walletCollectorScoutSource: string | null;

  @Column({
    type: 'varchar',
    name: 'wallet_connector_scout_source',
    nullable: true,
  })
  walletConnectorScoutSource: string | null;

  @Column({ type: 'varchar', name: 'creator', nullable: true })
  creator: string | null;

  @Column({ type: 'varchar', name: 'wallet_creator', nullable: true })
  walletCreator: string | null;

  @Column({ type: 'varchar', name: 'wallet_creator_source', nullable: true })
  walletCreatorSource: string | null;
}
