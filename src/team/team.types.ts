import { Team } from './team.entity';

export interface ITeamMembersIds {
  ids: number[];
  team: Team;
}

export enum TeamType {
  OPEN = 'Open',
  CLOSED = 'Closed',
  EXCLUSIVE = 'Exclusive',
}
