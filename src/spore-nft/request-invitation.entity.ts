import {
  CreateDateColumn,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  Column,
} from 'typeorm';

import { User } from '../users/user.entity';

@Entity()
export class RequestInvitation {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User, {
    nullable: false,
    onDelete: 'CASCADE',
    primary: true,
  })
  @Index()
  @JoinColumn({ name: 'source_user_id' })
  sourceUser: User;

  @ManyToOne(() => User, {
    nullable: false,
    onDelete: 'CASCADE',
    primary: true,
  })
  @Index()
  @JoinColumn({ name: 'target_user_id' })
  targetUser: User;

  @Column({ type: 'boolean', name: 'invitation_status', default: false })
  invitationStatus: boolean;

  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @Column({ type: 'timestamp', nullable: true, name: 'finished_at' })
  finishedAt?: Date | null;
}
