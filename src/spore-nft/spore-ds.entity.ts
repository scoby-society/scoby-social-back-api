import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Projects } from '../projects/projects.entity';
import { User } from '../users/user.entity';

@Entity()
export class SporeDs {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User, { nullable: true })
  @JoinColumn({ name: 'user_id' })
  ownerUser?: User | null;

  @Column({ type: 'varchar', name: 'wallet_adress' })
  walletAdress: string;

  @Column({ type: 'varchar', name: 'contract_adress' })
  contractAdress: string;

  @Column({ type: 'int', name: 'serial_number', nullable: true })
  serialNumber: number | null;

  @Column({ type: 'varchar', name: 'uri', nullable: true })
  uri: string | null;

  @Column({ type: 'varchar', name: 'url_image', nullable: true })
  urlImage: string | null;

  @CreateDateColumn({ name: 'created_at', nullable: true })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;

  @Column({ type: 'varchar', name: 'symbol', nullable: true })
  symbol: string | null;

  @Column({ type: 'varchar', name: 'name', nullable: true })
  name: string | null;

  @Column({ type: 'varchar', name: 'description', nullable: true })
  description: string | null;

  @Column({ type: 'varchar', name: 'web_site', nullable: true })
  webSite: string | null;

  @Column({ type: 'varchar', name: 'background', nullable: true })
  background: string | null;

  @Column({ type: 'varchar', name: 'meta_data', nullable: true })
  metaData: string | null;

  @Column({ type: 'varchar', name: 'token_id', nullable: true })
  tokenId: string | null;

  @Column({ type: 'varchar', name: 'block_chain', nullable: true })
  blockChain: string | null;

  @Column({ type: 'varchar', name: 'effect', nullable: true })
  effect: string | null;

  @Column({ type: 'varchar', name: 'creator', nullable: true })
  creator: string | null;

  @Column({ type: 'varchar', name: 'type_collections', nullable: true })
  typeCollections: string | null;

  @Column({ type: 'boolean', name: 'is_visible', default: true })
  isVisible: boolean;

  @ManyToMany(() => Projects, project => project.nft)
  projects: Projects[];
}
