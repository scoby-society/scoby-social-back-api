import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

import { SporeDs } from './spore-ds.entity';
@Entity()
export class SporeDsTrx {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', name: 'event', nullable: true })
  event: string | null;

  @Column({ type: 'float', name: 'price', nullable: true })
  price: number | null;

  @Column({ type: 'varchar', name: 'from_wallet', nullable: true })
  fromWallet: string | null;

  @Column({ type: 'varchar', name: 'to_wallet', nullable: true })
  toWallet: string | null;

  @Column({ type: 'varchar', name: 'signature' })
  signature: string;

  @Column({ type: 'varchar', name: 'instructions', nullable: true })
  instructions: string;

  @CreateDateColumn({ name: 'date_trx', nullable: true })
  dateTrx: Date;

  @ManyToOne(() => SporeDs, { nullable: true })
  @JoinColumn({ name: 'spore_id' })
  sporeId?: SporeDs | null;
}
