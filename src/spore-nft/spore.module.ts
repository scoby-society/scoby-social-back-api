import { Module } from '@nestjs/common';
import { SporeService } from './spore.service';
import { SporeResolver } from './spore.resolver';
import { JwtModule } from '../lib/jwt/jwt.module';

@Module({
  imports: [JwtModule],
  providers: [SporeService,SporeResolver],
  exports: [SporeService],
})
export class SporeModule {}