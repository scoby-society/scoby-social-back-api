import { UseGuards } from '@nestjs/common';
import { Resolver, Query, Mutation, Args, Int } from '@nestjs/graphql';
import { SporeService } from './spore.service';
import {
  SporeObject,
  SporeUsers,
  SporeUsersDs,
  SporeDsTransaction,
  metaDataDs,
  createNft,
  RequestInvitationObject,
} from './spore.graphql';
import { JwtGuard } from 'src/lib/jwt/jwt.guard';
import { CurrentUser } from '../lib/common/current-user.decorator';
import { BasePayload } from '../lib/jwt/jwt.service';
import { PagingInput } from '../lib/common/common.graphql';
import { Paging } from '../lib/common/common.interfaces';

@Resolver('Spore')
export class SporeResolver {
  constructor(private sporeService: SporeService) {}

  @Mutation(() => SporeObject)
  @UseGuards(JwtGuard)
  async createNFT(
    @Args('walletAdress') walletAdress: string,
    @Args('contractAdress') contractAdress: string,
    @Args('uri', { nullable: true }) uriNft: string,
    @Args('urlImage', { nullable: true }) urlImage: string,
    @Args('serialNFt', { nullable: true }) serialNft: number,
    @Args('visible', { nullable: true }) isVisible: boolean,
    @Args('metaData') meta: metaDataDs,
    @Args('idUser', { nullable: true, type: () => Number })
    idUser?: number | null,
  ): Promise<SporeObject> {
    return this.sporeService.createNft(
      walletAdress,
      contractAdress,
      serialNft,
      uriNft,
      urlImage,
      isVisible,
      meta,
      idUser,
    );
  }

  @Query(() => [SporeObject])
  @UseGuards(JwtGuard)
  async getSpores(): Promise<SporeObject[]> {
    return this.sporeService.getSpores();
  }

  @Mutation(() => SporeObject)
  @UseGuards(JwtGuard)
  async editSpore(
    @Args('id') id: number,
    @Args('walletAdress') walletAdress: string,
    @Args('contractAdress') contractAdress: string,
    @Args('uri', { nullable: true }) uriNft: string,
    @Args('serialNFt', { nullable: true }) serialNft: number,
    @Args('urlImage', { nullable: true }) urlImage: string,
    @Args('idUser', { nullable: true, type: () => Number })
    idUser?: number | null,
  ): Promise<SporeObject> {
    return this.sporeService.editSpore(
      id,
      walletAdress,
      contractAdress,
      serialNft,
      uriNft,
      urlImage,
      idUser,
    );
  }

  @Mutation(() => SporeObject)
  @UseGuards(JwtGuard)
  async createSporePublic(
    @Args('walletAdress') walletAdress: string,
    @Args('contractAdress') contractAdress: string,
    @Args('uri') uriNft: string,
    @Args('serialNFt') serialNft: number,
  ): Promise<SporeObject> {
    return this.sporeService.createSporePublic(
      walletAdress,
      contractAdress,
      serialNft,
      uriNft,
    );
  }

  @Query(() => [SporeObject])
  async getSporesPublic(): Promise<SporeObject[]> {
    return this.sporeService.getSporesPublic();
  }

  @Query(() => [SporeUsers])
  async getAllSpores(): Promise<SporeUsers[]> {
    return this.sporeService.getAllSpores();
  }

  @Mutation(() => Boolean)
  @UseGuards(JwtGuard)
  async cleanSpores(): Promise<boolean> {
    return this.sporeService.cleanSpores();
  }

  @Mutation(() => Boolean)
  @UseGuards(JwtGuard)
  async deleteSporeById(@Args('idSpore') idSpore: number): Promise<boolean> {
    return this.sporeService.deleteSporeById(idSpore);
  }

  @Query(() => [SporeUsersDs])
  async getNftByUser(
    @Args('id') userId: number,
    @Args('collection', { nullable: true }) symbol?: string,
  ): Promise<SporeUsersDs[]> {
    return this.sporeService.getNftByUser(userId, symbol);
  }

  @Query(() => [SporeUsersDs])
  async getNftByUserPaging(
    @Args('id') userId: number,
    @Args('paging', { type: () => PagingInput, nullable: true })
    paging: Paging,
    @Args('collection', { nullable: true }) symbol?: string,
  ): Promise<SporeUsersDs[]> {
    return this.sporeService.getNftByUserPaging(userId, paging, symbol);
  }

  @Query(() => [SporeUsersDs])
  async getAllSporesDataScience(): Promise<SporeUsersDs[]> {
    return this.sporeService.getAllSporesDs();
  }

  @Query(() => SporeUsersDs)
  async getSporeDsById(@Args('id') idSpore: number): Promise<SporeUsersDs> {
    return this.sporeService.getSporeDsById(idSpore);
  }

  @Mutation(() => SporeUsers)
  @UseGuards(JwtGuard)
  async updateVisibleSpore(
    @CurrentUser() currentUser: BasePayload,
    @Args('id', { nullable: true }) sporeId: number,
  ): Promise<SporeUsers> {
    return this.sporeService.updateVisibleSpore(sporeId, currentUser.id);
  }

  @Mutation(() => Boolean)
  @UseGuards(JwtGuard)
  async showSpores(
    @Args('idSpores', { type: () => [Int] })
    sporeIds: [number],
  ): Promise<boolean> {
    return this.sporeService.showSpores(sporeIds);
  }

  @Mutation(() => Boolean)
  @UseGuards(JwtGuard)
  async hideSpores(
    @Args('idSpores', { type: () => [Int] })
    sporeIds: [number],
  ): Promise<boolean> {
    return this.sporeService.hideSpores(sporeIds);
  }

  @Query(() => SporeUsersDs)
  async getSporeDsBySerial(
    @Args('serial') serialSpore: number,
  ): Promise<SporeUsersDs> {
    return this.sporeService.getSporeDsBySerial(serialSpore);
  }

  @Query(() => [SporeDsTransaction])
  async getAllSporesTransactions(): Promise<SporeDsTransaction[]> {
    return this.sporeService.getAllSporesTransaction();
  }

  @Query(() => [SporeUsersDs])
  async getCollectionOfNft(
    @Args('collection') name: string,
  ): Promise<SporeUsersDs[]> {
    return this.sporeService.getCollectionNft(name);
  }

  @Mutation(() => [SporeUsersDs])
  async updateProfileNft(
    @Args('data', { nullable: true, type: () => [createNft] })
    data: [createNft],
  ): Promise<SporeUsersDs[]> {
    return this.sporeService.updateProfileNft(data);
  }

  @Query(() => [RequestInvitationObject])
  @UseGuards(JwtGuard)
  async getInvitationTargetUser(
    @CurrentUser() currentUser: BasePayload,
    @Args('userId', { nullable: true }) userId: number,
  ): Promise<RequestInvitationObject[]> {
    return this.sporeService.getInvitationTargetUser(userId ?? currentUser.id);
  }

  @Query(() => [RequestInvitationObject])
  @UseGuards(JwtGuard)
  async getInvitationSourceUser(
    @CurrentUser() currentUser: BasePayload,
    @Args('userId', { nullable: true }) userId: number,
  ): Promise<RequestInvitationObject[]> {
    return this.sporeService.getInvitationSourceUser(userId ?? currentUser.id);
  }

  @Mutation(() => RequestInvitationObject)
  @UseGuards(JwtGuard)
  async updateInvitationStateById(
    @Args('id') id: number,
  ): Promise<RequestInvitationObject> {
    return this.sporeService.updateInvitationState(id);
  }

  @Mutation(() => RequestInvitationObject)
  @UseGuards(JwtGuard)
  async deleteInvitationById(
    @Args('id') id: number,
  ): Promise<RequestInvitationObject> {
    return this.sporeService.deleteRequestById(id);
  }

  @Query(() => [SporeUsersDs])
  async getNftByWallet(
    @Args('walletAdress') wallet: string,
    @Args('collection', { nullable: true }) symbol?: string,
  ): Promise<SporeUsersDs[]> {
    return this.sporeService.getNftByWallet(wallet, symbol);
  }
}
