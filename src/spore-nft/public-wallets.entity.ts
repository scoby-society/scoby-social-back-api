import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class PublicWallet {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', name: 'wallet_adress' })
  walletAdress: string;

  @Column({ type: 'varchar', name: 'contract_adress' })
  contractAdress: string;

  @Column({ type: 'int', name: 'serial_number' })
  serialNumber: number;

  @Column({ type: 'varchar', name: 'uri', nullable:true})
  uri: string | null;

  @Column({ type: 'varchar', name: 'url_image', nullable:true})
  urlImage: string | null;
}
