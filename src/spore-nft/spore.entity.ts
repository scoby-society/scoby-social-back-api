import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { User } from '../users/user.entity';

@Entity()
export class SporeNft {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User, { nullable: true })
  @JoinColumn({ name: 'user_id' })
  ownerUser?: User | null;

  @Column({ type: 'varchar', name: 'wallet_adress' })
  walletAdress: string;

  @Column({ type: 'varchar', name: 'contract_adress' })
  contractAdress: string;

  @Column({ type: 'int', name: 'serial_number', nullable: true })
  serialNumber: number | null;

  @Column({ type: 'varchar', name: 'uri', nullable: true })
  uri: string | null;

  @Column({ type: 'varchar', name: 'url_image', nullable: true })
  urlImage: string | null;

  @CreateDateColumn({ name: 'created_at', nullable: true })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;

  @Column({ type: 'boolean', name: 'is_visible', default: true })
  isVisible: boolean;
}
