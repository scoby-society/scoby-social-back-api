import { Field, Int, ObjectType, InputType } from '@nestjs/graphql';
import { UserProfile } from '../users/users.graphql';

@ObjectType()
export class SporeObjectProject {
  @Field(() => Int)
  id: number;

  @Field(() => String, { nullable: true })
  name?: string | null;

  @Field(() => String, { nullable: true })
  symbol?: string | null;

  @Field(() => String, { nullable: true })
  description?: string | null;

  @Field(() => String)
  walletAdress: string;

  @Field(() => String)
  contractAdress: string;

  @Field(() => Number, { nullable: true })
  serialNumber: number | null;

  @Field(() => String, { nullable: true })
  uri: string | null;

  @Field(() => String, { nullable: true })
  urlImage: string | null;

  @Field(() => Boolean, { nullable: true })
  isVisible?: boolean | null;
}
@ObjectType()
export class SporeObject {
  @Field(() => Int)
  id: number;

  @Field(() => String)
  walletAdress: string;

  @Field(() => String)
  contractAdress: string;

  @Field(() => Number, { nullable: true })
  serialNumber: number | null;

  @Field(() => String, { nullable: true })
  uri: string | null;

  @Field(() => String, { nullable: true })
  urlImage: string | null;

  @Field(() => Boolean, { nullable: true })
  isVisible?: boolean | null;
}

@ObjectType()
export class SporeUsers extends SporeObject {
  @Field(() => UserProfile, { nullable: true })
  user?: UserProfile | null;
}

@ObjectType()
export class WalletPublicObject {
  @Field(() => Int)
  id: number;

  @Field(() => String)
  walletAdress: string;

  @Field(() => String)
  contractAdress: string;

  @Field(() => Number, { nullable: true })
  serialNumber: number | null;

  @Field(() => String, { nullable: true })
  uri: string | null;
}

@ObjectType()
export class SporeUsersDs extends SporeObject {
  @Field(() => String, { nullable: true })
  creator: string | null;

  @Field(() => String, { nullable: true })
  symbol: string | null;

  @Field(() => String, { nullable: true })
  name: string | null;

  @Field(() => String, { nullable: true })
  description: string | null;

  @Field(() => String, { nullable: true })
  webSite: string | null;

  @Field(() => String, { nullable: true })
  background: string | null;

  @Field(() => String, { nullable: true })
  metaData: string | null;

  @Field(() => String, { nullable: true })
  tokenId: string | null;

  @Field(() => String, { nullable: true })
  blockChain: string | null;

  @Field(() => String, { nullable: true })
  effect: string | null;

  @Field(() => UserProfile, { nullable: true })
  user?: UserProfile | null;

  @Field(() => String, { nullable: true })
  typeCollections: string | null;
}

@ObjectType()
export class SporeDsTransaction {
  @Field(() => Int)
  id: number;

  @Field(() => String, { nullable: true })
  event: string | null;

  @Field(() => Number, { nullable: true })
  price: number | null;

  @Field(() => String, { nullable: true })
  fromWallet: string | null;

  @Field(() => String, { nullable: true })
  toWallet: string | null;

  @Field(() => String, { nullable: true })
  signature: string | null;

  @Field(() => String, { nullable: true })
  instructions: string | null;

  @Field()
  dateTrx: Date;

  @Field(() => SporeObject, { nullable: true })
  sporeId?: SporeObject | null;
}

@InputType()
export class metaDataDs {
  @Field(() => String, { nullable: true })
  symbol?: string | null;

  @Field(() => String, { nullable: true })
  name?: string | null;

  @Field(() => String, { nullable: true })
  description?: string | null;

  @Field(() => String, { nullable: true })
  webSite?: string | null;

  @Field(() => String, { nullable: true })
  background?: string | null;

  @Field(() => String, { nullable: true })
  metaData?: string | null;

  @Field(() => String, { nullable: true })
  tokenId?: string | null;

  @Field(() => String, { nullable: true })
  blockChain?: string | null;

  @Field(() => String, { nullable: true })
  effect?: string | null;

  @Field(() => String, { nullable: true })
  creator?: string | null;

  @Field(() => String, { nullable: true })
  typeCollections?: string | null;
}
@InputType()
export class createNft {
  @Field(() => String)
  walletAdress: string;

  @Field(() => String)
  contractAdress: string;

  @Field(() => String)
  uri: string;

  @Field(() => String)
  urlImage: string;

  @Field(() => Int)
  serialNft: number;

  @Field(() => Boolean, { nullable: true })
  isVisible: boolean | true;

  @Field(() => String, { nullable: true })
  symbol?: string | null;

  @Field(() => String, { nullable: true })
  name?: string | null;

  @Field(() => String, { nullable: true })
  description?: string | null;

  @Field(() => String, { nullable: true })
  webSite?: string | null;

  @Field(() => String, { nullable: true })
  background?: string | null;

  @Field(() => String, { nullable: true })
  metaData?: string | null;

  @Field(() => String)
  tokenId: string;

  @Field(() => String, { nullable: true })
  blockChain?: string | null;

  @Field(() => String, { nullable: true })
  effect?: string | null;

  @Field(() => String, { nullable: true })
  creator?: string | null;
}

@ObjectType()
export class RequestInvitationObject {
  @Field(() => Int)
  id: number;

  @Field()
  createdAt: Date;

  @Field(() => UserProfile)
  sourceUser: UserProfile;

  @Field(() => UserProfile)
  targetUser: UserProfile;

  @Field(() => Boolean, { nullable: true })
  invitationStatus?: boolean;
}
