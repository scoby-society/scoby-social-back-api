export const SPORE_ERRORS = {
  WALLET_NOT_FOUND: {
    CODE: 'ERR_WALLET_NOT_FOUND',
    MESSAGE: 'Wallet not found',
  },
  USER_NOT_OWNER_WALLET: {
    CODE: 'ERR_USER_NOT_OWNER_WALLET',
    MESSAGE: 'User not owner of the wallet',
  },
  SPORE_NOT_FOUND: {
    CODE: 'ERR_SPORE_NOT_FOUND',
    MESSAGE: 'Spore not found',
  },
  NOT_NFT_OWNER: {
    CODE: 'ERR_NOT_NFT_OWNER',
    MESSAGE: 'User is not owner of the NFT',
  },
  REQUEST_NOT_FOUND: {
    CODE: 'ERR_REQUEST_NOT_FOUND',
    MESSAGE: 'Request not found',
  },
};
