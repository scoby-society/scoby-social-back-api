import { Injectable } from '@nestjs/common';
import { getConnection, getRepository, Brackets } from 'typeorm';
import { SporeNft } from './spore.entity';
import { SporeDs } from './spore-ds.entity';
import { User } from 'src/users/user.entity';
import { SporeDsTrx } from './spore-ds-trx.entity';
import {
  SporeObject,
  SporeUsers,
  SporeUsersDs,
  SporeDsTransaction,
  metaDataDs,
  createNft,
  RequestInvitationObject,
} from './spore.graphql';
import { ApolloError } from 'apollo-server-express';
import { USERS_ERRORS } from 'src/users/users.messages';
import { SPORE_ERRORS } from './spore.messages';
import { PublicWallet } from './public-wallets.entity';
import { RequestInvitation } from './request-invitation.entity';
import { Paging } from 'src/lib/common/common.interfaces';

@Injectable()
export class SporeService {
  async createNft(
    walletAdress: string,
    contractAdress: string,
    serialNft: number,
    uriNft: string,
    urlImage: string,
    isVisible: boolean,
    meta: metaDataDs,
    idUser?: number | null,
  ): Promise<SporeObject> {
    let user = null;
    const {
      symbol,
      name,
      description,
      webSite,
      background,
      metaData,
      tokenId,
      blockChain,
      effect,
      creator,
      typeCollections,
    } = meta;
    if (idUser) {
      user = await getRepository(User).findOne(idUser);
      if (!user) {
        throw new ApolloError(
          USERS_ERRORS.USER_NOT_FOUND.MESSAGE,
          USERS_ERRORS.USER_NOT_FOUND.CODE,
        );
      }
    }

    const repository = getRepository(SporeDs);

    return await repository.save({
      ownerUser: user,
      walletAdress,
      contractAdress,
      serialNumber: serialNft,
      uri: uriNft,
      urlImage,
      isVisible,
      symbol,
      name,
      description,
      webSite,
      background,
      tokenId,
      metaData,
      blockChain,
      effect,
      creator,
      typeCollections,
    });
  }

  async getSpores(): Promise<SporeObject[]> {
    await this.getRoleUser(1);
    return await getRepository(SporeDs)
      .createQueryBuilder('spore')
      .leftJoinAndSelect('spore.ownerUser', 'user')
      .getMany();
  }

  async getSporeUser(id: number): Promise<SporeObject[]> {
    return await getRepository(SporeDs)
      .createQueryBuilder('spore')
      .where('spore.ownerUser = :id', { id })
      .getMany();
  }

  async editSpore(
    id: number,
    walletAdress: string,
    contractAdress: string,
    serialNft: number,
    uriNft: string,
    urlImage: string,
    idUser?: number | null,
  ): Promise<SporeObject> {
    let user = null;
    if (idUser) {
      user = await getRepository(User).findOne(idUser);
      if (!user) {
        throw new ApolloError(
          USERS_ERRORS.USER_NOT_FOUND.MESSAGE,
          USERS_ERRORS.USER_NOT_FOUND.CODE,
        );
      }
    }
    const repository = getRepository(SporeNft);
    const spore = await repository.findOne(id);

    if (!spore) {
      throw new ApolloError(
        SPORE_ERRORS.SPORE_NOT_FOUND.MESSAGE,
        SPORE_ERRORS.SPORE_NOT_FOUND.CODE,
      );
    }

    return await repository.save({
      id: id,
      ownerUser: user || spore.ownerUser,
      walletAdress,
      contractAdress,
      serialNumber: serialNft,
      uri: uriNft,
      urlImage,
    });
  }

  async createSporePublic(
    walletAdress: string,
    contractAdress: string,
    serialNft: number,
    uriNft: string,
  ): Promise<SporeObject> {
    const repository = getRepository(PublicWallet);
    return await repository.save({
      walletAdress,
      contractAdress,
      serialNumber: serialNft,
      uri: uriNft,
    });
  }

  async getSporesPublic(): Promise<SporeObject[]> {
    return await getRepository(PublicWallet)
      .createQueryBuilder('spore')
      .getMany();
  }

  async getAllSpores(): Promise<SporeUsers[]> {
    const sporas = await getRepository(SporeNft)
      .createQueryBuilder('spore')
      .leftJoinAndSelect('spore.ownerUser', 'user')
      .orderBy('spore.id', 'DESC')
      .getMany();

    const users = await getRepository(User)
      .createQueryBuilder('user')
      .leftJoinAndSelect('user.topics', 'topics')
      .where({ isPublicKeyVerified: true })
      .getMany();

    const data = [];
    for await (const spora of sporas) {
      const user = users.find(({ id }) => spora.ownerUser?.id === id);
      const userData = { ...spora, user };
      data.push(userData);
    }
    return data;
  }

  async cleanSpores(): Promise<boolean> {
    await getConnection()
      .createQueryBuilder()
      .delete()
      .from(SporeNft)
      .execute();

    return true;
  }

  async deleteSporeById(id: number): Promise<boolean> {
    await getConnection()
      .createQueryBuilder()
      .delete()
      .from(SporeNft)
      .where('id = :id', { id })
      .execute();

    return true;
  }

  async getNftByUser(idUser: number, symbol?: string): Promise<SporeUsersDs[]> {
    const repository = getRepository(SporeDs);
    const queryBuilder = repository
      .createQueryBuilder()
      .where({
        ownerUser: idUser,
      })
      .orderBy({
        created_at: 'DESC',
      });
    if (symbol) {
      queryBuilder.andWhere(
        new Brackets((qb) => qb.andWhere('symbol LIKE :symbol', { symbol })),
      );
    }
    const sporas = await queryBuilder.getMany();

    const users = await getRepository(User)
      .createQueryBuilder('user')
      .leftJoinAndSelect('user.topics', 'topics')
      .where('user.id =:idUser', { idUser })
      .getOne();

    const data = [];
    for await (const spora of sporas) {
      const userData = { ...spora, user: users };
      data.push(userData);
    }

    return data;
  }

  async getNftByUserPaging(
    idUser: number,
    paging: Paging,
    symbol?: string,
  ): Promise<SporeUsersDs[]> {
    const repository = getRepository(SporeDs);
    const queryBuilder = repository
      .createQueryBuilder()
      .where({
        ownerUser: idUser,
      })
      .orderBy({
        created_at: 'DESC',
      });
    if (symbol) {
      queryBuilder.andWhere(
        new Brackets((qb) => qb.andWhere('symbol LIKE :symbol', { symbol })),
      );
    }

    const sporas = await queryBuilder
      .skip(paging.limit * (paging.page - 1))
      .take(paging.limit)
      .getMany();

    const users = await getRepository(User)
      .createQueryBuilder('user')
      .leftJoinAndSelect('user.topics', 'topics')
      .where('user.id =:idUser', { idUser })
      .getOne();

    const data = [];
    for await (const spora of sporas) {
      const userData = { ...spora, user: users };
      data.push(userData);
    }

    return data;
  }

  async getAllSporesDs(): Promise<SporeUsersDs[]> {
    const sporas = await getRepository(SporeDs)
      .createQueryBuilder('spore')
      .leftJoinAndSelect('spore.ownerUser', 'user')
      .orderBy('spore.serialNumber', 'DESC')
      .getMany();

    const users = await getRepository(User)
      .createQueryBuilder('user')
      .leftJoinAndSelect('user.topics', 'topics')
      .where({ isPublicKeyVerified: true })
      .getMany();

    const data = [];
    for await (const spora of sporas) {
      const user = users.find(({ id }) => spora.ownerUser?.id === id);
      const userData = { ...spora, user };
      data.push(userData);
    }
    return data;
  }

  async getSporeDsById(id: number): Promise<SporeUsersDs> {
    const spora = await getRepository(SporeDs)
      .createQueryBuilder('spore')
      .leftJoinAndSelect('spore.ownerUser', 'user')
      .where('spore.id=:id', { id })
      .getOne();

    if (!spora) {
      throw new ApolloError(
        SPORE_ERRORS.SPORE_NOT_FOUND.MESSAGE,
        SPORE_ERRORS.SPORE_NOT_FOUND.CODE,
      );
    }

    return { ...spora, user: spora.ownerUser };
  }

  async getSporeDsBySymbol(symbol: string[]): Promise<SporeDs[]> {
    const spora = await getRepository(SporeDs)
      .createQueryBuilder('spore')
      .leftJoinAndSelect('spore.ownerUser', 'user')
      .where('spore.symbol=:symbol', { symbol })
      .getMany();

    if (!spora) {
      throw new ApolloError(
        SPORE_ERRORS.SPORE_NOT_FOUND.MESSAGE,
        SPORE_ERRORS.SPORE_NOT_FOUND.CODE,
      );
    }

    return spora;
  }

  async verifyCollectionNfts(nfts: number[]): Promise<void> {
    for (const id of nfts) {
      await this.getSporeDsById(id);
    }
  }

  async verifyCollectionNftsSymbol(nfts: string[]): Promise<void> {
    await this.getSporeDsBySymbol(nfts);
  }

  async getNameCollectionNfts(ids: number[]): Promise<string[]> {
    const collections: string[] = [];
    for (const idNft of ids) {
      const nftObject = await this.getSporeDsById(idNft);
      collections.push(nftObject.symbol || '');
    }
    return collections.filter((element) => element !== '');
  }

  async updateVisibleSpore(
    id: number,
    currentUserId: number,
  ): Promise<SporeUsers> {
    const repository = getRepository(SporeDs);
    const serieRepository = await getRepository(SporeDs)
      .createQueryBuilder('spore')
      .leftJoinAndSelect('spore.ownerUser', 'user')
      .where('spore.id =:id', { id })
      .getOne();

    if (!serieRepository) {
      throw new ApolloError(
        SPORE_ERRORS.SPORE_NOT_FOUND.MESSAGE,
        SPORE_ERRORS.SPORE_NOT_FOUND.CODE,
      );
    }

    if (serieRepository.ownerUser?.id !== currentUserId) {
      throw new ApolloError(
        SPORE_ERRORS.NOT_NFT_OWNER.MESSAGE,
        SPORE_ERRORS.NOT_NFT_OWNER.CODE,
      );
    }

    const spore = await repository.save({
      id: id,
      isVisible: serieRepository.isVisible ? false : true,
    });

    return spore;
  }

  async showSpores(id: number[]): Promise<boolean> {
    const repository = getRepository(SporeDs);
    const serieRepository = await getRepository(SporeDs)
      .createQueryBuilder('spore')
      .leftJoinAndSelect('spore.ownerUser', 'user')
      .where('spore.id IN (:...id)', { id })
      .getMany();

    for await (const spore of serieRepository) {
      await repository.save({
        id: spore.id,
        isVisible: true,
      });
    }
    return true;
  }

  async hideSpores(id: number[]): Promise<boolean> {
    const repository = getRepository(SporeDs);
    const serieRepository = await getRepository(SporeDs)
      .createQueryBuilder('spore')
      .leftJoinAndSelect('spore.ownerUser', 'user')
      .where('spore.id IN (:...id)', { id })
      .getMany();

    for await (const spore of serieRepository) {
      await repository.save({
        id: spore.id,
        isVisible: false,
      });
    }
    return true;
  }

  async getSporeDsBySerial(id: number): Promise<SporeUsersDs> {
    const spora = await getRepository(SporeDs)
      .createQueryBuilder('spore')
      .leftJoinAndSelect('spore.ownerUser', 'user')
      .where('spore.serialNumber=:id', { id })
      .getOne();

    if (!spora) {
      throw new ApolloError(
        SPORE_ERRORS.SPORE_NOT_FOUND.MESSAGE,
        SPORE_ERRORS.SPORE_NOT_FOUND.CODE,
      );
    }

    return { ...spora, user: spora.ownerUser };
  }

  async getAllSporesTransaction(): Promise<SporeDsTransaction[]> {
    return await getRepository(SporeDsTrx)
      .createQueryBuilder('transaction')
      .leftJoinAndSelect('transaction.sporeId', 'user')
      .getMany();
  }

  async getCollectionNft(name: string): Promise<SporeUsersDs[]> {
    const sporas = await getRepository(SporeDs)
      .createQueryBuilder('spore')
      .leftJoinAndSelect('spore.ownerUser', 'owner')
      .where('spore.symbol=:name', { name })
      .orderBy('spore.serialNumber', 'DESC')
      .getMany();

    const users = await getRepository(User)
      .createQueryBuilder('user')
      .leftJoinAndSelect('user.topics', 'topics')
      .where({ isPublicKeyVerified: true })
      .getMany();

    const data = [];
    for await (const spora of sporas) {
      const user = users.find(({ id }) => spora.ownerUser?.id === id);
      const userData = { ...spora, user };
      data.push(userData);
    }
    return data;
  }

  async idUserCollectionNft(name: string): Promise<number[]> {
    const nft = await this.getCollectionNft(name);
    const ids = nft.map((nft) => nft.user?.id || 0);
    ids.filter((element) => element !== 0);
    return [...new Set(ids)];
  }

  async idUserCollectionNftByIds(ids: number[]): Promise<number[]> {
    const nfts = [];
    for (const idNft of ids) {
      nfts.push(await this.getSporeDsById(idNft));
    }

    const idUsers = [];
    for (const nameCollection of nfts) {
      idUsers.push(
        ...(await this.idUserCollectionNft(nameCollection.symbol || '')),
      );
    }
    return [...new Set(idUsers)];
  }

  async updateProfileNft(data: createNft[]): Promise<SporeUsersDs[]> {
    const walletUser = data[0].walletAdress;
    const repository = getRepository(SporeDs);
    const nftUsers = await repository
      .createQueryBuilder('nft')
      .where('nft.walletAdress=:contract', { contract: walletUser })
      .getMany();

    const nftInDataBase: any[] = [];
    for (const nftUser of nftUsers) {
      if (data.find((nft) => nft.contractAdress === nftUser.contractAdress)) {
        nftInDataBase.push(nftUser);
      }
    }
    const deleteNfts = nftUsers.filter(
      (elem) =>
        !nftInDataBase.find(
          (nftDB) => elem.contractAdress === nftDB.contractAdress,
        ),
    );

    for (const nft of data) {
      const nftDb = await repository
        .createQueryBuilder('nft')
        .where('nft.contractAdress=:contract', { contract: nft.contractAdress })
        .getOne();

      const user = await getRepository(User).findOne({
        publicKey: nft.walletAdress,
      });

      if (nftDb && user) {
        await repository.save({
          id: nftDb.id,
          ownerUser: user,
          walletAdress: nft.walletAdress,
          serialNumber: nft.serialNft,
          contractAdress: nft.contractAdress || '',
          uri: nft.uri,
          urlImage: nft.urlImage,
          isVisible: nft.isVisible,
          symbol: nft.symbol,
          name: nft.name,
          description: nft.description,
          webSite: nft.webSite,
          background: nft.background,
          tokenId: nft.tokenId,
          metaData: nft.metaData,
          blockChain: nft.blockChain,
          effect: nft.blockChain,
          creator: nft.creator,
        });
      } else if (!nftDb && user) {
        await repository.save({
          ownerUser: user,
          walletAdress: nft.walletAdress,
          serialNumber: nft.serialNft,
          contractAdress: nft.contractAdress || '',
          uri: nft.uri,
          urlImage: nft.urlImage,
          isVisible: nft.isVisible,
          symbol: nft.symbol,
          name: nft.name,
          description: nft.description,
          webSite: nft.webSite,
          background: nft.background,
          tokenId: nft.tokenId,
          metaData: nft.metaData,
          blockChain: nft.blockChain,
          effect: nft.blockChain,
          creator: nft.creator,
        });
      }
    }

    for (const nft of deleteNfts) {
      repository.save({
        id: nft.id,
        ownerUser: null,
        walletAdress: '',
        serialNumber: nft.serialNumber,
        contractAdress: nft.contractAdress || '',
        uri: nft.uri,
        urlImage: nft.urlImage,
        isVisible: false,
        symbol: nft.symbol,
        name: nft.name,
        description: nft.description,
        webSite: nft.webSite,
        background: nft.background,
        tokenId: nft.tokenId,
        metaData: nft.metaData,
        blockChain: nft.blockChain,
        effect: nft.blockChain,
        creator: nft.creator,
      });
    }

    return await getRepository(SporeDs)
      .createQueryBuilder('nft')
      .leftJoinAndSelect('nft.ownerUser', 'owner')
      .where('nft.walletAdress=:contract', { contract: walletUser })
      .orderBy('nft.id', 'DESC')
      .getMany();
  }

  async getInvitationTargetUser(
    userId: number,
  ): Promise<RequestInvitationObject[]> {
    return await getRepository(RequestInvitation)
      .createQueryBuilder('invitation')
      .leftJoinAndSelect('invitation.targetUser', 'sourceUser')
      .leftJoinAndSelect('invitation.sourceUser', 'targetUser')
      .where('targetUser.id = :userId', { userId })
      .andWhere('invitation.finishedAt is null')
      .getMany();
  }

  async getInvitationSourceUser(
    userId: number,
  ): Promise<RequestInvitationObject[]> {
    return await getRepository(RequestInvitation)
      .createQueryBuilder('invitation')
      .leftJoinAndSelect('invitation.targetUser', 'sourceUser')
      .leftJoinAndSelect('invitation.sourceUser', 'targetUser')
      .where('sourceUser.id = :userId', { userId })
      .andWhere('invitation.finishedAt is null')
      .getMany();
  }

  async updateInvitationState(id: number): Promise<RequestInvitationObject> {
    const repository = getRepository(RequestInvitation);
    const request = await repository
      .createQueryBuilder('invitation')
      .leftJoinAndSelect('invitation.targetUser', 'sourceUser')
      .leftJoinAndSelect('invitation.sourceUser', 'targetUser')
      .where('invitation.id = :id', { id })
      .andWhere('invitation.finishedAt is null')
      .getOne();

    if (!request) {
      throw new ApolloError(
        SPORE_ERRORS.REQUEST_NOT_FOUND.MESSAGE,
        SPORE_ERRORS.REQUEST_NOT_FOUND.CODE,
      );
    }

    await repository.save({
      id: request.id,
      sourceUser: request.sourceUser,
      targetUser: request.targetUser,
      createdAt: request.createdAt,
      invitationStatus: true,
    });

    return { ...request, invitationStatus: true };
  }

  async deleteRequestById(id: number): Promise<RequestInvitationObject> {
    const repository = getRepository(RequestInvitation);
    const request = await repository
      .createQueryBuilder('invitation')
      .leftJoinAndSelect('invitation.targetUser', 'sourceUser')
      .leftJoinAndSelect('invitation.sourceUser', 'targetUser')
      .where('invitation.id = :id', { id })
      .andWhere('invitation.finishedAt is null')
      .getOne();

    if (!request) {
      throw new ApolloError(
        SPORE_ERRORS.REQUEST_NOT_FOUND.MESSAGE,
        SPORE_ERRORS.REQUEST_NOT_FOUND.CODE,
      );
    }
    const finishedAt = new Date(Date.now());

    await repository.save({
      id: request.id,
      sourceUser: request.sourceUser,
      targetUser: request.targetUser,
      createdAt: request.createdAt,
      invitationStatus: request.invitationStatus,
      finishedAt,
    });
    return request;
  }

  async getRoleUser(idUser: number): Promise<string> {
    const userRepository = await getRepository(SporeDs)
      .createQueryBuilder('spore')
      .leftJoinAndSelect('spore.ownerUser', 'user')
      .where(
        'user.id = :id AND spore.serialNumber = 0 OR user.id = :id AND spore.symbol =:royaltie ',
        { id: idUser, royaltie: 'RLTY' },
      )
      .getOne();

    const user = await getRepository(User).findOne(idUser);
    const nftUser = await this.getNftByUser(idUser);

    if (userRepository?.id === 0) {
      return 'Creador';
    } else if (user?.role === 'Guide') {
      return 'Guide';
    } else if (userRepository?.symbol === 'RLTY') {
      return 'Royalty';
    } else if (nftUser.length > 0) {
      return 'Member';
    } else {
      return 'Guest';
    }
  }

  async getNftByWallet(
    wallet: string,
    symbol?: string,
  ): Promise<SporeUsersDs[]> {
    const repository = getRepository(SporeDs);
    const queryBuilder = repository
      .createQueryBuilder()
      .where({
        walletAdress: wallet,
      })
      .orderBy({
        created_at: 'DESC',
      });
    if (symbol) {
      queryBuilder.andWhere(
        new Brackets((qb) => qb.andWhere('symbol LIKE :symbol', { symbol })),
      );
    }
    const sporas = await queryBuilder.getMany();

    const users = await getRepository(User)
      .createQueryBuilder('user')
      .leftJoinAndSelect('user.topics', 'topics')
      .where('user.publicKey =:wallet', { wallet })
      .getOne();

    const data = [];
    for await (const spora of sporas) {
      const userData = { ...spora, user: users };
      data.push(userData);
    }

    return data;
  }
}
