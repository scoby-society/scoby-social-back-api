import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { JwtGuard } from '../lib/jwt/jwt.guard';
import { CurrentUser } from '../lib/common/current-user.decorator';
import { BasePayload } from '../lib/jwt/jwt.service';
import { UseGuards } from '@nestjs/common';
import { ReportService } from './report.service';
import { UserProfile } from 'src/users/users.graphql';

@Resolver('Report')
export class ReportResolver {
  constructor(private reportService: ReportService) {}

  @Mutation(() => UserProfile)
  @UseGuards(JwtGuard)
  async createReport(
    @CurrentUser() currentUser: BasePayload,
    @Args('id') id: number,
  ): Promise<UserProfile> {
    return await this.reportService.createReport(currentUser.id, id);
  }
}
