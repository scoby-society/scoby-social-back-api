import { Injectable } from '@nestjs/common';
import { ApolloError } from 'apollo-server-express';
import { EmailService } from 'src/lib/email/email.service';
import { UserProfile } from 'src/users/users.graphql';
import { UsersService } from 'src/users/users.service';
import { getConnection, getRepository } from 'typeorm';
import { ReportUser } from './report.entity';
import { REPORT_ERRORS } from './report.messages';

@Injectable()
export class ReportService {
  constructor(
    private userService: UsersService,
    private emailService: EmailService,
  ) {}

  async createReport(
    currentUser: number,
    userId: number,
  ): Promise<UserProfile> {
    const createdAt = new Date(Date.now());
    const sourceUser = await this.userService.getUserProfile(currentUser);
    const targetUser = await this.userService.getUserProfile(userId);

    await this.checkShippingLimit(sourceUser.id, targetUser.id);

    await getConnection()
      .createQueryBuilder()
      .insert()
      .into(ReportUser)
      .values({
        sourceUser: {
          id: currentUser,
        },
        targetUser: {
          id: userId,
        },
        createdAt: createdAt,
      })
      .orIgnore()
      .execute();

    await this.emailService.sendReport(sourceUser, targetUser);
    return targetUser;
  }

  async checkShippingLimit(
    sourceUser: number,
    targetUser: number,
  ): Promise<void> {
    const report = await getRepository(ReportUser)
      .createQueryBuilder('user')
      .leftJoinAndSelect('user.sourceUser', 'sourceUser')
      .leftJoinAndSelect('user.targetUser', 'targetUser')
      .where('sourceUser.id = :source  AND targetUser.id = :target', {
        source: sourceUser,
        target: targetUser,
      })
      .orderBy('user.id', 'DESC')
      .getOne();

    const date = new Date(Date.now());

    if (report) {
      const expirationDate = new Date(report.createdAt);
      expirationDate.setDate(expirationDate.getDate() + 1);

      if (expirationDate > date) {
        throw new ApolloError(
          REPORT_ERRORS.LIMIT_OF_REPORTS.MESSAGE,
          REPORT_ERRORS.LIMIT_OF_REPORTS.CODE,
        );
      }
    }
  }
}
