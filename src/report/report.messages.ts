export const REPORT_ERRORS = {
  LIMIT_OF_REPORTS: {
    CODE: 'ERR_LIMIT_OF_REPORTS',
    MESSAGE: 'The user already has a report',
  },
};
