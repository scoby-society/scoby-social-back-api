import { Field, Int, ObjectType } from '@nestjs/graphql';
import { UserProfile } from '../users/users.graphql';

@ObjectType()
export class ReportObject {
  @Field(() => Int)
  id: number;

  @Field()
  createdAt: Date;

  @Field(() => UserProfile)
  sourceUser: UserProfile;

  @Field(() => UserProfile)
  targetUser: UserProfile;
}
