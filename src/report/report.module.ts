import { Module } from '@nestjs/common';
import { UsersModule } from 'src/users/users.module';
import { ReportService } from './report.service';
import { ReportResolver } from './report.resolver';
import { JwtModule } from 'src/lib/jwt/jwt.module';
import { EmailModule } from 'src/lib/email/email.module';
@Module({
  imports: [UsersModule,JwtModule,EmailModule],
  providers: [ReportService, ReportResolver],
  exports: [ReportService],
})
export class ReportModule {}
