import { Injectable } from '@nestjs/common';
import { Activity } from 'src/activity/activity.entity';
import { ChatExperience } from 'src/chatExperience/chatExperience.entity';
import { Event } from 'src/events/events.entity';
import { EventsObject } from 'src/events/events.graphql';
import { EventsService } from 'src/events/events.service';
import { Projects } from 'src/projects/projects.entity';
import { ProjectsService } from 'src/projects/projects.service';
import { Series } from 'src/series/series.entity';
import { SeriesObject } from 'src/series/series.graphql';
import { SeriesServices } from 'src/series/series.service';
import { Session } from 'src/sessions/session.entity';
import { SessionsService } from 'src/sessions/sessions.service';
import { SporeDs } from 'src/spore-nft/spore-ds.entity';
import { Team } from 'src/team/team.entity';
import { User } from 'src/users/user.entity';
import { UserProfile } from 'src/users/users.graphql';
import { EntityManager, getRepository, IsNull, Not, getConnectionManager  } from 'typeorm';
import { GlobalHomeFeed } from './global_search.graphql';
import { RankSearchAPI } from './rank-seach.service';

@Injectable()
export class GlobalSearchService {
  constructor(
    private rankSearchAPI: RankSearchAPI,
    private seriesServices: SeriesServices,
    private eventsService: EventsService,
    private sessionsService: SessionsService,
    private projectsService: ProjectsService
  ) {}

  async getSerieRanking(
    idUser: number,
    query: string,
  ): Promise<SeriesObject[]> {
    const repository = getRepository(Series);
    const experienceSecrets = await this.seriesServices.getSecretExperiences(
      idUser,
    );
    const series = await repository
      .createQueryBuilder('serie')
      .leftJoinAndSelect('serie.topics', 'topics')
      .leftJoinAndSelect('serie.schedule', 'schedule')
      .leftJoinAndSelect('serie.suscribeUsers', 'suscribeUsers')
      .leftJoinAndSelect('serie.projects', 'projects')
      .leftJoinAndSelect('serie.session', 'session')
      .leftJoinAndSelect('serie.ownerUser', 'ownerUser')
      .andWhere('serie.seriesName ILIKE :name  AND serie.finishedAt is null', {
        name: `%${query ? query : ''}%`,
      })
      .andWhere('serie.projects is not null')

      .getMany();

    const idSeries: number[] = series.map((serie) => {
      return serie.id;
    });

    const { experiencies } = await this.rankSearchAPI.getRankMember({
      source_user: idUser,
      experiencies: idSeries,
    });

    const arraySort: any[] = [];
    if (experiencies === undefined) {
      return series;
    }
    for (const idSerie of experiencies) {
      arraySort.push(
        series.find((serie) => {
          return serie.id === idSerie;
        }),
      );
    }
    const serieArray: any[] = [];
    Promise.all(
      arraySort.map(async (serie) => {
        const isAvailable = await this.seriesServices.isAvailable(
          experienceSecrets,
          serie,
          idUser,
        );
        const { suscribeUsers } = serie;
        const isSuscribed = [...suscribeUsers].some(
          (User) => User.id === idUser,
        );
        if (serie.serieType === 'Secret') {
          if (experienceSecrets.find((element) => element === serie.id)) {
            serieArray.push({ ...serie, subscribed: isSuscribed, isAvailable });
          }
        } else {
          serieArray.push({ ...serie, subscribed: isSuscribed, isAvailable });
        }
      }),
    );
    return serieArray;
  }

  async getTeamRanking(
    idUser: number,
    query: string,
    limit: number,
    page: number,
    isAll:boolean,
  ): Promise<GlobalHomeFeed[]> {
    const TeamArray: any[] = [];
    const teams = await getRepository(Team)
      .createQueryBuilder('team')
      .leftJoinAndSelect('team.topics', 'topics')
      .leftJoinAndSelect('team.ownerUser', 'ownerUser')
      .leftJoinAndSelect('team.members', 'members')
      .leftJoinAndSelect('team.projects', 'projects')
      .leftJoinAndSelect('team.pendingUsers', 'pendingUsers')
      .where('team.name ILIKE :name', { name: `%${query}%` })
      .andWhere('team.projects is not null')
      .getMany();

    const idTeams: number[] = teams.map((team) => {
      return team.id;
    });

    const { communities } = await this.rankSearchAPI.getRankMember({
      source_user: idUser,
      communities: idTeams,
    });

    if (communities === undefined) {
      return teams;
    }

    const arraySort: any[] = [];
    for (const idTeam of communities) {
      arraySort.push(
        teams.find((team) => {
          return team.id === idTeam;
        }),
      );
    }

    arraySort.map((team) => {
      TeamArray.push({ ...team, typeData: 'comunity' });
    });

    if(isAll){
      return  TeamArray;
    }
    return TeamArray.slice(page * limit - limit, page * limit);
  }

  async getEventRanking(
    idUser: number,
    query: string,
  ): Promise<EventsObject[]> {
    const experienceSecrets = await this.eventsService.getSecretExperiences(
      idUser,
    );
    const repository = getRepository(Event);
    const event = await repository
      .createQueryBuilder('event')
      .leftJoinAndSelect('event.topics', 'topics')
      .leftJoinAndSelect('event.suscribeUsers', 'suscribeUsers')
      .leftJoinAndSelect('event.session', 'session')
      .leftJoinAndSelect('event.projects', 'projects')
      .leftJoinAndSelect('event.ownerUser', 'ownerUser')
      .andWhere('event.title ILIKE :name  AND event.finishedAt is null ', {
        name: `%${query ? query : ''}%`,
      })
      .andWhere('event.projects is not null')
      .getMany();

    const idEvents: number[] = event.map((event) => {
      return event.id;
    });

    const { events } = await this.rankSearchAPI.getRankMember({
      source_user: idUser,
      events: idEvents,
    });

    if (events === undefined) {
      return event;
    }

    const arraySort: any[] = [];
    for (const idEvent of events) {
      arraySort.push(
        event.find((event) => {
          return event.id === idEvent;
        }),
      );
    }
    const eventArray: any[] = [];
    Promise.all(
      arraySort.map(async (event) => {
        const isAvailable = await this.eventsService.isPrivateAvailable(
          experienceSecrets,
          event,
          idUser,
        );
        const { suscribeUsers } = event;
        const isSuscribed = [...suscribeUsers].some(
          (User) => User.id === idUser,
        );
        if (event.serieType === 'Secret') {
          if (experienceSecrets.find((element) => element === event.id)) {
            eventArray.push({ ...event, subscribed: isSuscribed, isAvailable });
          }
        } else {
          eventArray.push({ ...event, subscribed: isSuscribed, isAvailable });
        }
      }),
    );
    return eventArray;
  }

  async getCollectionGlobal(
    query: string,
    limit: number,
    page: number,
  ): Promise<GlobalHomeFeed[]> {
    const collectionArray: any[] = [];
    const repository = getRepository(Projects);
    const projects = await repository
      .createQueryBuilder('project')
      .leftJoinAndSelect('project.nft', 'nft')
      .where('nft.symbol ILIKE :parsedQuery  AND project.finishedAt is null', {
        parsedQuery: `%${query ? query : ''}%`,
      })
      .getMany();

    projects.map((project) => {
      collectionArray.push({ ...project, typeData: 'collections' });
    });

    return collectionArray.slice(
      page * limit - limit,
      page * limit,
    ) as GlobalHomeFeed[];
  }

  async getMembersGlobal(
    idCurrentUser: number,
    query: string,
  ): Promise<UserProfile[]> {
    const repository = getRepository(User);
    const user = await repository
      .createQueryBuilder('user')
      .where('user.username ILIKE :parsedQuery', {
        parsedQuery: `%${query ? query : ''}%`,
      })
      .orWhere('user.fullName ILIKE :parsedQuery', {
        parsedQuery: `%${query ? query : ''}%`,
      })
      .getMany();

    const idUsers: number[] = user.map((user) => {
      return user.id;
    });

    const { members } = await this.rankSearchAPI.getRankMember({
      source_user: idCurrentUser,
      members: idUsers,
    });

    if (members === undefined) {
      return user;
    }

    const arraySort: any[] = [];
    for (const idUser of members) {
      arraySort.push(
        user.find((user) => {
          return user.id === idUser;
        }),
      );
    }
    return arraySort;
  }

  async getSearchSession(
    idCurrentUser: number,
    query: string,
  ): Promise<Session[]> {
    const sessionArray: any = [];
    const repository = getRepository(Session);
    const sessionLive = await this.sessionsService.getLiveSessions(
      idCurrentUser,
    );
    const sessions = await repository
      .createQueryBuilder('session')
      .leftJoinAndSelect('session.projects', 'projects')
      .leftJoinAndSelect('session.projects', 'viewerUsers')
      .leftJoinAndSelect('session.ownerUser', 'ownerUser')
      .where(
        'session.title ILIKE :parsedQuery  AND session.finishedAt is null',
        {
          parsedQuery: `%${query ? query : ''}%`,
        },
      )
      .getMany();

    sessionLive.map((session) => {
      
      if (sessions.find((element) => element.id === session.id)) {
        sessionArray.push(session);
      }
    });

    return sessionArray;
  }

  async searchInviteUserProject(
    target: number,
    procedure: number,
  ): Promise<boolean> {
    const activity = await getRepository(Activity).find({
      where: {
        type_action: 'project',
        targetUser: target,
        procedure_action: procedure,
      },
      relations: ['targetUser', 'sourceUser'],
    });
    if (activity.length > 0) {
      return true;
    }
    return false;
  }

  async getHomeProjectFeed(
    limit: number,
    page: number,
    category: string,
    currentUserId: number,
  ): Promise<GlobalHomeFeed[] | undefined> {
    const seriesArray: any = [];
    const eventArray: any = [];
    const sessionArray: any = [];
    const comunityArray: any = [];
    const collectionsArray: any = [];
    const channelArray: any = [];
    const seriesArrayUn: any = [];
    const eventArrayUn: any = [];
    const sessionArrayUn: any = [];
    const comunityArrayUn: any = [];
    const collectionsArrayUn: any = [];
    const channelArrayUn: any = [];
    const series = await getRepository(Series).find({
      where: { finishedAt: null, projects: Not(IsNull()) },
      relations: ['projects', 'ownerUser','suscribeUsers','schedule'],
    });
    await Promise.all(
      series.map(async (serie) => {
        if (serie?.projects?.project === 'Listed' && serie?.projects?.finishedAt === null) {
          const seri = (await getRepository(Series).findOne({where:{id:serie.id}})) as Series;
          const experienceSecret = await this.seriesServices.getSecretExperiences(currentUserId || 1);
          const isAvailable = currentUserId
          ? await this.seriesServices.isAvailable(
              experienceSecret,
              seri,
              currentUserId,
            )
          : false;
          const subscribed = await this.seriesServices.subscribed(serie?.id,currentUserId)
          seriesArray.push({
            ...serie,
            subscribed,
            isAvailable,
            typeExperience: 'series',
            typeData: 'experience',
          });
        }
        const resultActivityUser = await this.searchInviteUserProject(
          currentUserId,
          serie?.projects?.id || 0,
        );
        if (
          serie?.projects?.project === 'Unlisted' &&
          serie?.projects?.finishedAt === null &&
          (serie?.projects?.suscribeUsers?.some(
            (sus: any) => sus.id === currentUserId,
          ) ||
            resultActivityUser)
        ) {
          const seri = (await getRepository(Series).findOne({where:{id:serie.id}})) as Series;
          const experienceSecret = await this.seriesServices.getSecretExperiences(currentUserId || 1);
          const isAvailable = currentUserId
          ? await this.seriesServices.isAvailable(
              experienceSecret,
              seri,
              currentUserId,
            )
          : false;
          const subscribed = await this.seriesServices.subscribed(serie?.id,currentUserId)
          seriesArrayUn.push({
            ...serie,
            subscribed,
            isAvailable,
            typeExperience: 'series',
            typeData: 'experience',
          });
        }
      }),
    );
    const channels = await getRepository(ChatExperience).find({
      where:{finishedAt:null,projects: Not(IsNull())},
      relations:['projects','ownerUser','suscribeUsers']
    });

    await Promise.all(
      channels.map(async (channel) => {
        if(channel?.projects?.project === 'Listed' && channel?.projects?.finishedAt === null){
          channelArray.push({
            ...channel,
            typeExperience: 'channels',
            typeData: 'experience'
          })
        }
        const resultActivityUser = await this.searchInviteUserProject(
          currentUserId,
          channel?.projects?.id || 0,
        );
        if (
          channel.projects?.project === 'Unlisted' &&
          channel?.projects?.finishedAt === null &&
          (channel?.projects?.suscribeUsers?.some(
            (sus: any) => sus.id === currentUserId,
          ) ||
            resultActivityUser)
        ) {
          channelArrayUn.push({
            ...channel,
            typeExperience: 'channels',
            typeData: 'experience',
          });
        }
      })
    );

    const events = await getRepository(Event).find({
      where: { finishedAt: null, projects: Not(IsNull()) },
      relations: ['projects', 'ownerUser','suscribeUsers'],
    });
    await Promise.all(
      events.map(async (event) => {
        if (event?.projects?.project === 'Listed' && event?.projects?.finishedAt === null) {
          const even = (await getRepository(Event).findOne({where:{id:event.id}})) as Event;
          const experienceSecret = await this.eventsService.getSecretExperiences(currentUserId || 1);
          const isAvailable = currentUserId
          ? await this.eventsService.isPrivateAvailable(
              experienceSecret,
              even,
              currentUserId,
            )
          : false;
          const subscribed = await this.eventsService.subscribed(event?.id,currentUserId)
          eventArray.push({
            ...event,
            isAvailable,
            subscribed,
            typeExperience: 'events',
            typeData: 'experience',
          });
        }
        const resultActivityUser = await this.searchInviteUserProject(
          currentUserId,
          event?.projects?.id || 0,
        );
        if (
          event.projects?.project === 'Unlisted' &&
          event?.projects?.finishedAt === null &&
          (event?.projects?.suscribeUsers?.some(
            (sus: any) => sus.id === currentUserId,
          ) ||
            resultActivityUser)
        ) {
          const even = (await getRepository(Event).findOne({where:{id:event.id}})) as Event;
          const experienceSecret = await this.eventsService.getSecretExperiences(currentUserId || 1);
          const isAvailable = currentUserId
          ? await this.eventsService.isPrivateAvailable(
              experienceSecret,
              even,
              currentUserId,
            )
          : false;
          const subscribed = await this.eventsService.subscribed(event?.id,currentUserId)
          eventArrayUn.push({
            ...event,
            isAvailable,
            subscribed,
            typeExperience: 'series',
            typeData: 'experience',
          });
        }
      }),
    );
    const sessions = await getRepository(Session).find({
      where: { finishedAt: null, projects: Not(IsNull()) },
      relations: ['projects', 'ownerUser', 'viewerUsers'],
    });
    await Promise.all(
      sessions.map(async (session) => {
        if (session.projects?.project === 'Listed' && session?.projects?.finishedAt === null) {
          const sess = (await getRepository(Session).findOne({where:{id:session.id}})) as Session;
          const viewers = this.sessionsService.countViewers(sess);
          const experienceSecret = await this.sessionsService.getSecretExperiences(currentUserId || 1);
          const isAvailable = currentUserId
          ? await this.sessionsService.isPrivateAvailable(
              experienceSecret,
              sess,
              currentUserId,
            )
          : false;
          sessionArray.push({
            ...session,
            isAvailable,
            viewers,
            typeExperience: 'sessions',
            typeData: 'experience',
          });
        }
        const resultActivityUser = await this.searchInviteUserProject(
          currentUserId,
          session?.projects?.id || 0,
        );
        if (
          session.projects?.project === 'Unlisted' &&
          session.projects?.finishedAt === null &&
          (session?.projects?.suscribeUsers?.some(
            (sus: any) => sus.id === currentUserId,
          ) ||
            resultActivityUser)
        ) {
          const sess = (await getRepository(Session).findOne({where:{id:session.id}})) as Session;
          const viewers = this.sessionsService.countViewers(sess);
          const experienceSecret = await this.sessionsService.getSecretExperiences(currentUserId || 1);
          const isAvailable = currentUserId
          ? await this.sessionsService.isPrivateAvailable(
              experienceSecret,
              sess,
              currentUserId,
            )
          : false;
          sessionArrayUn.push({
            ...session,
            isAvailable,
            viewers,
            typeExperience: 'series',
            typeData: 'experience',
          });
        }
      }),
    );
    const comunitys = await getRepository(Team).find({
      where: { finishedAt: null, projects: Not(IsNull()) },
      relations: ['projects', 'ownerUser','members'],
    });
    await Promise.all(
      comunitys.map(async (comunity) => {
        if (comunity.projects?.project === 'Listed' && comunity.projects?.finishedAt === null) {
          comunityArray.push({ ...comunity, typeData: 'comunity' });
        }
        const resultActivityUser = await this.searchInviteUserProject(
          currentUserId,
          comunity?.projects?.id || 0,
        );
        if (
          comunity.projects?.project === 'Unlisted' &&
          comunity.projects?.finishedAt === null &&
          (comunity?.projects?.suscribeUsers?.some(
            (sus: any) => sus.id === currentUserId,
          ) ||
            resultActivityUser)
        ) {
          comunityArrayUn.push({ ...comunity, typeData: 'comunity' });
        }
      }),
    );

    const projects = await getRepository(Projects).find({
      where: { finishedAt: null},
      relations: ['nft', 'suscribeUsers', 'user'],
    });
    await Promise.all(
      projects.map(async (projects) => {
        if (projects?.project === 'Listed') {
          const projectsStatus = await this.projectsService.searchProjectStatus(projects?.id);
          if(projects?.nft?.length > 0){
            collectionsArray.push({ ...projects, typeData: 'collections' });
          }
          if(projects?.nft?.length <= 0 && projectsStatus === false){
            collectionsArray.push({ ...projects, typeData: 'collections' });
          }
        }
        const resultActivityUser = await this.searchInviteUserProject(
          currentUserId,
          projects?.id || 0,
        );
        if (
          projects?.project === 'Unlisted' &&
          (projects?.suscribeUsers?.some(
            (sus: any) => sus.id === currentUserId,
          ) ||
            resultActivityUser)
        ) {
          const projectsStatus = await this.projectsService.searchProjectStatus(projects?.id);
          if(projects?.nft?.length > 0){
            collectionsArrayUn.push({ ...projects, typeData: 'collections' });
          }
          if(projects?.nft?.length <= 0 && projectsStatus === false){
            collectionsArrayUn.push({ ...projects, typeData: 'collections' });
          }
        }
      }),
    );

    if (category === 'all') {
      return [
        ...seriesArray,
        ...seriesArrayUn,
        ...eventArray,
        ...eventArrayUn,
        ...sessionArray,
        ...sessionArrayUn,
        ...comunityArray,
        ...comunityArrayUn,
        ...channelArray,
        ...channelArrayUn,
        ...collectionsArray,
        ...collectionsArrayUn,
      ].slice(page * limit - limit, page * limit);
    }

    if (category === 'experience') {
      return [
        ...seriesArray,
        ...seriesArrayUn,
        ...eventArray,
        ...eventArrayUn,
        ...sessionArray,
        ...sessionArrayUn,
        ...channelArray,
        ...channelArrayUn
      ].slice(page * limit - limit, page * limit);
    }

    if (category === 'comunity') {
      return [...comunityArray, ...comunityArrayUn].slice(
        page * limit - limit,
        page * limit,
      );
    }

    if (category === 'collection') {
      return [...collectionsArray, ...collectionsArrayUn].slice(
        page * limit - limit,
        page * limit,
      );
    }

    if (category === 'channel') {
      return [...channelArray, ...channelArrayUn].slice(
        page * limit - limit,
        page * limit,
      );
    }
  }

  async searchAllExperiences(
    idUser: number,
    search: string,
    limit: number,
    page: number,
    returnAll:boolean
  ): Promise<GlobalHomeFeed[]> {
    const seriesArray: any = [];
    const eventArray: any = [];
    const sessionArray: any = [];
    const series = await this.getSerieRanking(idUser, search);
    const events = await this.getEventRanking(idUser, search);
    const sessions = await this.getSearchSession(idUser, search);

    await Promise.all(
      series.map(async (serie) => {
        const serieObject = await getRepository(Series).findOne({
          where: { id:serie.id },
          relations: ['projects', 'ownerUser','suscribeUsers','schedule'],
        });
        const subscribed = await this.seriesServices.subscribed(serie?.id,idUser)
        eventArray.push({ ...serieObject,
          typeExperience: 'series',
          isAvailable:serie.isAvailable,
          subscribed,
          typeData: 'experience', });
      })
    )

   await Promise.all(
    events.map(async (event) => {
      const subscribed = await this.eventsService.subscribed(event.id,idUser)
      const eventObject = await getRepository(Event).findOne({
        where: { id:event.id },
        relations: ['projects', 'ownerUser','suscribeUsers'],
      });
      eventArray.push({ ...eventObject,
        typeExperience: 'event',
        isAvailable:event.isAvailable,
        subscribed,
        typeData: 'experience', });
    })
   )
   const experienceSecret = await this.sessionsService.getSecretExperiences(idUser);

   await Promise.all(
    sessions.map(async (session) => {
      
      const isAvailable = idUser
      ? await this.sessionsService.isPrivateAvailable(
          experienceSecret,
          session,
          idUser,
        )
      : false;

      const viewers = this.sessionsService.countViewers(session);
      
      const sessionObject = await getRepository(Session).findOne({
        where: { id:session.id },
        relations: ['projects', 'ownerUser'],
      });
      eventArray.push({ ...sessionObject,
        typeExperience: 'session',
        isAvailable,
        viewers,
        typeData: 'experience', });
    })
   )

   const channels = await getRepository(ChatExperience)
   .createQueryBuilder('channel')
   .leftJoinAndSelect('channel.ownerUser', 'ownerUser')
   .leftJoinAndSelect('channel.suscribeUsers', 'suscribeUsers')
   .leftJoinAndSelect('channel.projects', 'projects')
   .where(
     `channel.title ILIKE :name `,
     { name: `%${search}%` },
   )
   .andWhere('channel.projects is not null')
   .getMany();

   const channelArray:any[] = []
   const channelArrayUn:any[] = []

   await Promise.all(
     channels.map(async (channel) => {
       if(channel?.projects?.project === 'Listed' && channel?.projects?.finishedAt === null){
         channelArray.push({
           ...channel,
           typeExperience: 'channels',
           typeData: 'experience'
         })
       }
       const resultActivityUser = await this.searchInviteUserProject(
         idUser,
         channel?.projects?.id || 0,
       );
       if (
         channel.projects?.project === 'Unlisted' &&
         channel?.projects?.finishedAt === null &&
         (channel?.projects?.suscribeUsers?.some(
           (sus: any) => sus.id === idUser,
         ) ||
           resultActivityUser)
       ) {
         channelArrayUn.push({
           ...channel,
           typeExperience: 'channels',
           typeData: 'experience',
         });
       }
     })
   );
    if(returnAll){
      return  [...seriesArray, ...eventArray, ...sessionArray,...channelArray,...channelArrayUn];
    }
    return [...seriesArray, ...eventArray, ...sessionArray,...channelArray,...channelArrayUn].slice(
      page * limit - limit,
      page * limit,
    );
  }

  async searchAll(
    idUser: number,
    search: string,
    limit: number,
    page: number,
    category:string,
  ): Promise<GlobalHomeFeed[]> {
    const experience= await this.searchAllExperiences(idUser,search,limit,page,true)
    const teams = await this.getTeamRanking(idUser,search,1,1,true)
    const collectionsArray:any[] = []
    const collectionsArrayUn:any[] = []

    const projects = await getRepository(Projects)
      .createQueryBuilder('project')
      .leftJoinAndSelect('project.user', 'ownerUser')
      .leftJoinAndSelect('project.suscribeUsers', 'suscribeUsers')
      .leftJoinAndSelect('project.nft', 'nft')
      .where(
        `nft.symbol ILIKE :name `,
        { name: `%${search}%` },
      )
      .getMany();

    await Promise.all(
      projects.map(async (projects) => {
        if (projects?.project === 'Listed') {
          const projectsStatus = await this.projectsService.searchProjectStatus(projects?.id);
          if(projects?.nft?.length > 0){
            collectionsArray.push({ ...projects, typeData: 'collections' });
          }
          if(projects?.nft?.length <= 0 && projectsStatus === false){
            collectionsArray.push({ ...projects, typeData: 'collections' });
          }
        }
        const resultActivityUser = await this.searchInviteUserProject(
          idUser,
          projects?.id || 0,
        );
        if (
          projects?.project === 'Unlisted' &&
          (projects?.suscribeUsers?.some(
            (sus: any) => sus.id === idUser,
          ) ||
            resultActivityUser)
        ) {
          const projectsStatus = await this.projectsService.searchProjectStatus(projects?.id);
          if(projects?.nft?.length > 0){
            collectionsArrayUn.push({ ...projects, typeData: 'collections' });
          }
          if(projects?.nft?.length <= 0 && projectsStatus === false){
            collectionsArrayUn.push({ ...projects, typeData: 'collections' });
          }
        }
      }),
    );

   
    
    if(category==='experiences'){
      return [...experience]
    }else if(category==='collections'){
      return [...collectionsArray,...collectionsArrayUn]
    }else if(category==='community'){
      return [...teams]
    }
    return [...experience,...teams,...collectionsArray,...collectionsArrayUn].slice(page * limit - limit,
      page * limit,)
  }

  
}
