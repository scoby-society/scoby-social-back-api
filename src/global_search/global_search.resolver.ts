import { UseGuards } from '@nestjs/common';
import { Resolver, Query, Args } from '@nestjs/graphql';
import { GlobalSearchService } from './global_search.service';
import { JwtGuard } from 'src/lib/jwt/jwt.guard';
import { SeriesObject } from 'src/series/series.graphql';
import { EventsObject } from 'src/events/events.graphql';
import { UserProfile } from 'src/users/users.graphql';
import { CurrentUser } from '../lib/common/current-user.decorator';
import { BasePayload } from '../lib/jwt/jwt.service';
import { SessionObject } from 'src/sessions/sessions.graphql';
import { Session } from 'src/sessions/session.entity';
import { GlobalHomeFeed } from './global_search.graphql';
import { PagingInput } from '../lib/common/common.graphql';
import { Paging } from '../lib/common/common.interfaces';

@Resolver('GlobalSearch')
export class GlobalSearchResolver {
  constructor(private globalSearchService: GlobalSearchService) {}

  @Query(() => [SeriesObject])
  @UseGuards(JwtGuard)
  async getSearchSerie(
    @CurrentUser() currentUser: BasePayload,
    @Args('query') query: string,
  ): Promise<SeriesObject[]> {
    return this.globalSearchService.getSerieRanking(currentUser.id, query);
  }

  @Query(() => [GlobalHomeFeed])
  @UseGuards(JwtGuard)
  async getSearchTeam(
    @CurrentUser() currentUser: BasePayload,
    @Args('query') query: string,
    @Args('paging', { type: () => PagingInput, nullable: true }) paging: Paging,
  ): Promise<GlobalHomeFeed[]> {
    return this.globalSearchService.getTeamRanking(
      currentUser.id,
      query,
      paging.limit,
      paging.page,
      false
    );
  }

  @Query(() => [EventsObject])
  @UseGuards(JwtGuard)
  async getSearchEvent(
    @CurrentUser() currentUser: BasePayload,
    @Args('query') query: string,
  ): Promise<EventsObject[]> {
    return this.globalSearchService.getEventRanking(currentUser.id, query);
  }

  @Query(() => [GlobalHomeFeed])
  @UseGuards(JwtGuard)
  async getSearchCollection(
    @Args('query') query: string,
    @Args('paging', { type: () => PagingInput, nullable: true }) paging: Paging,
  ): Promise<GlobalHomeFeed[]> {
    return this.globalSearchService.getCollectionGlobal(
      query,
      paging.limit,
      paging.page,
    );
  }

  @Query(() => [UserProfile])
  @UseGuards(JwtGuard)
  async getSearchMembers(
    @CurrentUser() currentUser: BasePayload,
    @Args('query') query: string,
  ): Promise<UserProfile[]> {
    return this.globalSearchService.getMembersGlobal(currentUser.id, query);
  }

  @Query(() => [SessionObject])
  @UseGuards(JwtGuard)
  async getSearchSession(
    @CurrentUser() currentUser: BasePayload,
    @Args('query') query: string,
  ): Promise<Session[]> {
    return this.globalSearchService.getSearchSession(currentUser.id, query);
  }

  @UseGuards(JwtGuard)
  @Query(() => [GlobalHomeFeed])
  async getHomeProjectFeed(
    @CurrentUser() currentUser: BasePayload,
    @Args('paging', { type: () => PagingInput, nullable: true }) paging: Paging,
    @Args('category', { type: () => String, nullable: true }) category: string,
  ): Promise<GlobalHomeFeed[] | undefined> {
    return this.globalSearchService.getHomeProjectFeed(
      paging.limit,
      paging.page,
      category,
      currentUser.id
    );
  }

  @Query(() => [GlobalHomeFeed])
  @UseGuards(JwtGuard)
  async getSearchAllExperiences(
    @CurrentUser() currentUser: BasePayload,
    @Args('query') query: string,
    @Args('paging', { type: () => PagingInput, nullable: true }) paging: Paging,
  ): Promise<GlobalHomeFeed[]> {
    return this.globalSearchService.searchAllExperiences(
      currentUser.id,
      query,
      paging.limit,
      paging.page,
      false,
    );
  }

  @Query(() => [GlobalHomeFeed])
  @UseGuards(JwtGuard)
  async searchAllData(
    @CurrentUser() currentUser: BasePayload,
    @Args('query') query: string,
    @Args('category') category: string,
    @Args('paging', { type: () => PagingInput, nullable: true }) paging: Paging,

  ): Promise<GlobalHomeFeed[]> {
    return this.globalSearchService.searchAll(
      currentUser.id,
      query,
      paging.limit,
      paging.page,
      category
    );
  }
}
