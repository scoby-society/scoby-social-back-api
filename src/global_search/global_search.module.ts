import { Module } from '@nestjs/common';
import { JwtModule } from '../lib/jwt/jwt.module';
import { GlobalSearchService } from './global_search.service';
import { GlobalSearchResolver } from './global_search.resolver';
import { UsersModule } from 'src/users/users.module';
import { RankSearchAPI } from './rank-seach.service';
import { SessionsModule } from 'src/sessions/sessions.module';
import { SeriesModule } from 'src/series/series.module';
import { EventsModule } from 'src/events/events.module';
import { ProjectsModule } from 'src/projects/projects.module';

@Module({
  imports: [
    UsersModule,
    JwtModule,
    RankSearchAPI,
    SessionsModule,
    SeriesModule,
    EventsModule,
    ProjectsModule
  ],
  providers: [GlobalSearchResolver, GlobalSearchService, RankSearchAPI],
  exports: [GlobalSearchService],
})
export class GlobalSearchModule {}
