import { Injectable } from '@nestjs/common';
//import { ApolloError } from 'apollo-server-express';
//import { USERS_ERRORS } from './users.messages';
import axios from 'axios';

export interface NotificationMessage {
  source_user: number;
  members?: number[];
  collections?: number[];
  events?: number[];
  experiencies?: number[];
  communities?: number[];
  projects?: number[];
}

@Injectable()
export class RankSearchAPI {
  async getRankMember(searchObject: NotificationMessage): Promise<any> {
    searchObject;
    try {
      const { data } = await axios.post(
        `https://recommendation-api-prod-scoby.scoby.dev/rank_search`,
        {
          source_user: searchObject.source_user,
          members: searchObject.members || [],
          collections: searchObject.collections || [],
          events: searchObject.events || [],
          experiencies: searchObject.experiencies || [],
          communities: searchObject.communities || [],
          projects: searchObject.projects || [],
        },
      );
      return data[0] || [];
    } catch (e) {
      console.error('Recommendation request failed', e);
      return [14];
    }
  }
}
