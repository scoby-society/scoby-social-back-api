import { Int, Field, ObjectType } from '@nestjs/graphql';
import { UserProfile } from 'src/users/users.graphql';
import { SessionLiveToSerie } from 'src/sessions/sessions.graphql';
import { Topic } from '../topics/topics.graphql';
import {  SporeUsersDs } from 'src/spore-nft/spore.graphql';
import { Projects } from 'src/projects/projects.entity';
import { ProjectView } from 'src/projects/projects.graphql';
import { ScheduleObject } from 'src/series/schedule.graphql';
import { TeamMemberObject } from 'src/team/team.graphql';
@ObjectType()
export class GlobalMembers {
  @Field(() => [UserProfile], { nullable: true })
  members?: UserProfile[] | null;
}

@ObjectType()
export class GlobalHomeFeed {
  @Field(() => Int)
  id: number;

  @Field(() => String, { nullable: true })
  name?: string | null;

  @Field(() => String, { nullable: true })
  coverImage?: string | null;

  @Field(() => String, { nullable: true })
  tile?: string | null;

  @Field(() => UserProfile, {nullable:true})
  user?: UserProfile | null;

  @Field(() => String, { nullable: true })
  calendarName?: string | null;

  @Field(() => String, { nullable: true })
  className?: string | null;

  @Field(() => String, { nullable: true })
  seriesName?: string | null;

  @Field(() => String, { nullable: true })
  title?: string | null;

  @Field(() => String, { nullable: true })
  typeExperience?: string | null;

  @Field(() => String, { nullable: true })
  description?: string | null;

  @Field(() => UserProfile, { nullable: true })
  ownerUser?: UserProfile | null;

  @Field(() => String, { nullable: true })
  avatar?: string | null;

  @Field(() => String, { nullable: true })
  backgroundImage?: string | null;

  @Field(() => [UserProfile], { nullable: true })
  suscribeUsers?: UserProfile[] | null;

  @Field(() => String, { nullable: true })
  linkWebsite?: string | null;

  @Field(() => String, { nullable: true })
  join?: string | null;

  @Field(() => String, { nullable: true })
  project?: string | null;

  @Field(() => [TeamMemberObject], { nullable: true })
  members?: TeamMemberObject[] | null;

  @Field(() => String, { nullable: true })
  teamType?: string | null;

  @Field(() => Boolean, { nullable: true })
  membersAllowedToHost?: boolean | null;

  @Field(() => Boolean, { nullable: true })
  membersAllowedToInvite?: boolean | null;

  @Field(() => [UserProfile], { nullable: true })
  pendingUsers?: UserProfile[] | null;

  @Field(() => SessionLiveToSerie, { nullable: true })
  session?: SessionLiveToSerie | null;

  @Field(() => [Topic], { nullable: true })
  topics?: Topic[] | null;

  @Field(() => Date, { nullable: true })
  createdAt?: Date | null;

  @Field(() => Date, { nullable: true })
  updatedAt?: Date | null;

  @Field(() => String, { nullable: true })
  start?: Date | null;

  @Field(() => String, { nullable: true })
  end?: Date | null;

  @Field(() => String, { nullable: true })
  day?: Date | null;

  @Field(() => Date, { nullable: true })
  finishedAt?: Date | null;

  @Field(() => Boolean, { nullable: true })
  subscribed?: boolean | null;

  @Field(() => [ScheduleObject], { nullable: true })
  schedule?: ScheduleObject[] | null;

  @Field(() => String, { nullable: true })
  serieType?: string | null;

  @Field(() => String, {nullable:true})
  chatType?: string | null;

  @Field(() => Number, { nullable: true })
  viewers?: number | null;

  @Field(() => String, { nullable: true })
  eventType?: string | null;

  @Field(() => [SporeUsersDs], { nullable: true })
  nft?: SporeUsersDs[] | null;

  @Field(() => Boolean, { nullable: true })
  isAvailable?: boolean | true;

  @Field(() => [Number], { nullable: true })
  nftsRequired?: number[] | null;

  @Field(() => [UserProfile], { nullable: true })
  participantUsers?: UserProfile[] | null;

  @Field(() => [UserProfile], { nullable: true })
  greenRoomUsers?: UserProfile[] | null;

  @Field(() => [UserProfile], { nullable: true })
  viewerUsers?: UserProfile[] | null;

  @Field(() => Boolean, { nullable: true })
  isPrivate?: boolean | null;

  @Field(() => String, { nullable: true })
  sessionType?: string | null;

  @Field(() => String, {nullable:true})
  typeData?: string | null;

  @Field(() => ProjectView, { nullable:true })
  projects?: Projects | null;
}

@ObjectType()
export class GlobalExperiences {
  @Field(() => Int)
  id: number;

  @Field(() => String, { nullable: true })
  calendarName?: string | null;

  @Field(() => String, { nullable: true })
  className?: string | null;

  @Field(() => String, { nullable: true })
  seriesName?: string | null;

  @Field(() => String, { nullable: true })
  title?: string | null;

  @Field(() => String, { nullable: true })
  typeExperience?: string | null;

  @Field(() => String, { nullable: true })
  description?: string | null;

  @Field(() => UserProfile, { nullable: true })
  ownerUser?: UserProfile | null;

  @Field(() => String, { nullable: true })
  avatar?: string | null;

  @Field(() => String, { nullable: true })
  backgroundImage?: string | null;

  @Field(() => [UserProfile],{nullable:true})
  suscribeUsers?: UserProfile[];

  @Field(() => SessionLiveToSerie, { nullable: true })
  session?: SessionLiveToSerie | null;

  @Field(() => [Topic], { nullable: true })
  topics?: Topic[] | null;

  @Field(() => Date, { nullable: true })
  createdAt?: Date | null;

  @Field(() => Date, { nullable: true })
  updatedAt?: Date | null;

  @Field(() => String, { nullable: true })
  start?: Date | null;

  @Field(() => String, { nullable: true })
  end?: Date | null;

  @Field(() => String, { nullable: true })
  day?: Date | null;

  @Field(() => Date, { nullable: true })
  finishedAt?: Date | null;

  @Field(() => Boolean, { nullable: true })
  subscribed?: boolean | null;

  @Field(() => [ScheduleObject], { nullable: true })
  schedule?: ScheduleObject[] | null;

  @Field(() => String, { nullable: true })
  serieType?: string | null;

  @Field(() => Number, { nullable: true })
  viewers?: number | null;

  @Field(() => String, { nullable: true })
  eventType?: string | null;

  @Field(() => SporeUsersDs, { nullable: true })
  nft?: SporeUsersDs | null;

  @Field(() => Boolean, { nullable: true })
  isAvailable?: boolean | true;

  @Field(() => [Number], { nullable: true })
  nftsRequired?: number[] | null;

  @Field(() => [UserProfile], { nullable: true })
  participantUsers?: UserProfile[] | null;

  @Field(() => [UserProfile], { nullable: true })
  greenRoomUsers?: UserProfile[] | null;

  @Field(() => [UserProfile], { nullable: true })
  viewerUsers?: UserProfile[] | null;

  @Field(() => Boolean, { nullable: true })
  isPrivate?: boolean | null;

  @Field(() => String, { nullable: true })
  sessionType?: string | null;

  @Field(() => ProjectView, { nullable: true })
  projects?: Projects | null;
}
