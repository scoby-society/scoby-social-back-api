import { Field, InputType, Int, ObjectType } from '@nestjs/graphql';
import { ChatExperienceObject } from 'src/chatExperience/chatExperience.graphql';
import { EventsObject } from 'src/events/events.graphql';
import { ScheduleObject } from 'src/series/schedule.graphql';
import { SeriesObject } from 'src/series/series.graphql';
import { SessionLiveToSerie, SessionObject } from 'src/sessions/sessions.graphql';
import { SporeObjectProject, SporeUsersDs } from 'src/spore-nft/spore.graphql';
import { TeamObject } from 'src/team/team.graphql';
import { Topic } from 'src/topics/topics.graphql';
import { UserProfile } from 'src/users/users.graphql';
import { Projects } from './projects.entity';
@ObjectType()
export class ProjectView {
  @Field(() => Int)
  id?: number;
  @Field(() => String, { nullable: true })
  name?: string;
  @Field(() => String, { nullable: true })
  description?: string;
  @Field(() => String, { nullable: true })
  coverImage?: string;
  @Field(() => String, { nullable: true })
  tile?: string;
  @Field(() => String, { nullable: true })
  join?: string;
  @Field(() => String, { nullable: true })
  project?: string;
  @Field(() => Boolean, { nullable: true })
  ownerProject?: boolean;
  @Field(() => Boolean, { nullable: true })
  suscribeProject?: boolean;
  @Field()
  user?: UserProfile;
  @Field(() => [UserProfile],{nullable:true})
  suscribeUsers?: UserProfile[];
  @Field(() => [SporeObjectProject])
  nft?: SporeObjectProject[];
  @Field(() => [SeriesObject],{ nullable: true })
  series?: SeriesObject[];
  @Field(() => [EventsObject],{ nullable: true })
  event?: EventsObject[];
  @Field(() => [TeamObject],{ nullable: true })
  team?: TeamObject[];
  @Field(() => [ChatExperienceObject],{ nullable: true })
  chatExperience?: ChatExperienceObject[];
  @Field(() => [SessionObject],{ nullable: true })
  session?: SessionObject[]
  @Field(() => Date, { nullable: true })
  finishedAt?: Date | null;
}

@ObjectType()
export class ProjectViewById {
  @Field(() => Int)
  id?: number;
  @Field(() => String, { nullable: true })
  name?: string;
  @Field(() => String, { nullable: true })
  description?: string;
  @Field(() => String, { nullable: true })
  coverImage?: string;
  @Field(() => String, { nullable: true })
  tile?: string;
  @Field(() => String, { nullable: true })
  join?: string;
  @Field(() => String, { nullable: true })
  project?: string;
  @Field(() => Boolean, { nullable: true })
  ownerProject?: boolean;
  @Field(() => Boolean, { nullable: true })
  suscribeProject?: boolean;
  @Field()
  user?: UserProfile;
  @Field(() => [UserProfile],{nullable:true})
  suscribeUsers?: UserProfile[];
  @Field(() => [SporeObjectProject])
  nft?: SporeObjectProject[];
  @Field(() => [ExperienceObject],{ nullable: true })
  experience?: ExperienceObject[];
  @Field(() => [TeamObject],{ nullable: true })
  team?: TeamObject[];
  @Field(() => Date, { nullable: true })
  finishedAt?: Date | null;
}

@ObjectType()
export class ExperienceObject {
  @Field(() => Int)
  id: number;

  @Field(() => String, { nullable: true })
  name?: string | null;

  @Field(() => UserProfile, {nullable:true})
  user?: UserProfile | null;

  @Field(() => String, { nullable: true })
  calendarName?: string | null;

  @Field(() => String, { nullable: true })
  className?: string | null;

  @Field(() => String, { nullable: true })
  seriesName?: string | null;

  @Field(() => String, { nullable: true })
  title?: string | null;

  @Field(() => String, { nullable: true })
  typeExperience?: string | null;

  @Field(() => String, { nullable: true })
  description?: string | null;

  @Field(() => UserProfile, { nullable: true })
  ownerUser?: UserProfile | null;

  @Field(() => String, { nullable: true })
  avatar?: string | null;

  @Field(() => String, { nullable: true })
  backgroundImage?: string | null;

  @Field(() => [UserProfile], { nullable: true })
  suscribeUsers?: UserProfile[] | null;

  @Field(() => SessionLiveToSerie, { nullable: true })
  session?: SessionLiveToSerie | null;

  @Field(() => [Topic], { nullable: true })
  topics?: Topic[] | null;

  @Field(() => Date, { nullable: true })
  createdAt?: Date | null;

  @Field(() => Date, { nullable: true })
  updatedAt?: Date | null;

  @Field(() => String, { nullable: true })
  start?: Date | null;

  @Field(() => String, { nullable: true })
  end?: Date | null;

  @Field(() => String, { nullable: true })
  day?: Date | null;

  @Field(() => Date, { nullable: true })
  finishedAt?: Date | null;

  @Field(() => Boolean, { nullable: true })
  subscribed?: boolean | null;

  @Field(() => String, { nullable: true })
  serieType?: string | null;

  @Field(() => Number, { nullable: true })
  viewers?: number | null;

  @Field(() => String, { nullable: true })
  eventType?: string | null;

  @Field(() => String, { nullable: true })
  chatType?: string | null;

  @Field(() => Boolean, { nullable: true })
  isAvailable?: boolean | true;

  @Field(() => [Number], { nullable: true })
  nftsRequired?: number[] | null;

  @Field(() => [UserProfile], { nullable: true })
  participantUsers?: UserProfile[] | null;

  @Field(() => [UserProfile], { nullable: true })
  greenRoomUsers?: UserProfile[] | null;

  @Field(() => [UserProfile], { nullable: true })
  viewerUsers?: UserProfile[] | null;

  @Field(() => Boolean, { nullable: true })
  isPrivate?: boolean | null;

  @Field(() => String, { nullable: true })
  sessionType?: string | null;

  @Field(() => ProjectView, { nullable:true })
  projects?: Projects | null;
}

@InputType()
export class projectEdit {
  @Field(() => String, { nullable: true })
  name?: string;
  @Field(() => String, { nullable: true })
  description?: string;
  @Field(() => String, { nullable: true })
  join?: string;
  @Field(() => String, { nullable: true })
  project?: string;
  @Field(() => [Int],{ nullable: true })
  nft: [number];
  @Field(() => [Int], { nullable: true })
  invitedUsers: [number];
  @Field(() => [Int], { nullable: true })
  teamInvitation: [number]; 
  @Field(() => Boolean, { nullable: true })
  isTeamsMembersInvited: boolean;
}

@InputType()
export class CreateProjectInput {
  @Field()
  name?: string;
  @Field()
  description?: string;
  @Field()
  join?: string;
  @Field()
  project?: string;
  @Field(() => [Int])
  nft: [number];
  @Field(() => [Int], { nullable: true })
  invitedUsers: [number];
  @Field(() => [Int], { nullable: true })
  teamInvitation: [number];
  @Field(() => Boolean, { nullable: true })
  isTeamsMembersInvited: boolean;
}
