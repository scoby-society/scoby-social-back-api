import { Resolver, Mutation, Args, Query, Int, Float } from '@nestjs/graphql';
import { Projects } from './projects.entity';
import { JwtGuard } from '../lib/jwt/jwt.guard';
import { UseGuards } from '@nestjs/common';
import {
  ProjectView,
  CreateProjectInput,
  projectEdit,
  ProjectViewById,
} from './projects.graphql';
import { CurrentUser } from '../lib/common/current-user.decorator';
import { BasePayload } from '../lib/jwt/jwt.service';
import { GraphQLUpload } from 'apollo-server-express';
import { FileUpload, Paging } from '../lib/common/common.interfaces';
import { ProjectsService } from './projects.service';
import { GlobalHomeFeed } from 'src/global_search/global_search.graphql';
import { PagingInput } from 'src/lib/common/common.graphql';
@Resolver('Projects')
export class ProjectsResolver {
  constructor(private projectsServices: ProjectsService) {}

  @Mutation(() => ProjectView)
  @UseGuards(JwtGuard)
  async createProject(
    @CurrentUser() currentUser: BasePayload,
    @Args('projectObj') projectObj: CreateProjectInput,
    @Args('coverImage', { nullable: true, type: () => GraphQLUpload })
    coverImage?: FileUpload,
    @Args('tile', { nullable: true, type: () => GraphQLUpload })
    tile?: FileUpload,
  ): Promise<ProjectView> {
    return this.projectsServices.createProject(
      currentUser.id,
      projectObj,
      coverImage,
      tile,
    );
  }

  @Mutation(() => ProjectView)
  @UseGuards(JwtGuard)
  async editProject(
    @CurrentUser() currentUser: BasePayload,
    @Args('projectId') projectId: number,
    @Args('projectEditObj') projectEditObj: projectEdit,
    @Args('coverImage', { nullable: true, type: () => GraphQLUpload })
    coverImage?: FileUpload,
    @Args('tile', { nullable: true, type: () => GraphQLUpload })
    tile?: FileUpload,
  ): Promise<ProjectView> {
    return await this.projectsServices.editProject(
      currentUser.id,
      projectId,
      projectEditObj,
      coverImage,
      tile,
    );
  }

  @Query(() => [ProjectView])
  @UseGuards(JwtGuard)
  async getAllProjects(): Promise<ProjectView[]> {
    return this.projectsServices.getAllProject();
  }

  @Query(() => [GlobalHomeFeed])
  @UseGuards(JwtGuard)
  async getProjectsForUser(
    @CurrentUser() currentUser: BasePayload,
    @Args('paging', { type: () => PagingInput, nullable: true }) paging: Paging,
    @Args('category', { type: () => String, nullable: true }) category: string,
    @Args('type', { type: () => String, nullable: true }) type: string,
    @Args('profileId', { type: () => Int, nullable: true }) profileId: number,
  ): Promise<GlobalHomeFeed[] | undefined> {
    return this.projectsServices.getProfileOfUser(
      paging.limit,
      paging.page,
      category,
      currentUser.id,
      type,
      profileId
    );
  }

  @Query(() => ProjectViewById)
  @UseGuards(JwtGuard)
  async getProjectById(
    @CurrentUser() currentUser: BasePayload,
    @Args('projectId') projectId: number,
  ): Promise<ProjectViewById> {
    return this.projectsServices.getProjectById(projectId, currentUser.id);
  }

  @Mutation(() => ProjectView)
  @UseGuards(JwtGuard)
  async endProject(
    @CurrentUser() currentUser: BasePayload,
    @Args('projectId') projectId: number,
  ): Promise<ProjectView> {
    return this.projectsServices.deleteProjectById(currentUser.id, projectId);
  }

  @Mutation(() => ProjectView)
  @UseGuards(JwtGuard)
  async joinProject(
    @CurrentUser() currentUser: BasePayload,
    @Args('projectId') projectId: number,
  ): Promise<ProjectView> {
    return this.projectsServices.joinProject(currentUser.id, projectId);
  }

  @Mutation(() => ProjectView)
  @UseGuards(JwtGuard)
  async invitedUsersProjects(
    @CurrentUser() currentUser: BasePayload,
    @Args('invitedUsers', { type: () => [Int] })
    invitedUsers: [number],
    @Args('idProject')
    idProject: number,
    @Args('teamInvitation', { nullable: true, type: () => [Int] })
    teamInvitation?: null | [number],
    @Args('isTeamsMembersInvited', { nullable: true, type: () => Boolean })
    isTeamsMembersInvited?: boolean,
  ): Promise<ProjectView> {
    return this.projectsServices.invitedUsersProjects(
      currentUser.id,
      invitedUsers,
      idProject,
      teamInvitation,
      isTeamsMembersInvited,
    );
  }

  @Mutation(() => ProjectView)
  @UseGuards(JwtGuard)
  async leaveProject(
    @CurrentUser() CurrentUser: BasePayload,
    @Args('projectId') projectId: number,
  ): Promise<ProjectView> {
    return this.projectsServices.leaveProject(CurrentUser.id, projectId);
  }
}
