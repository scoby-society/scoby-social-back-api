import { Module } from '@nestjs/common';
import { S3Module } from '../lib/s3/s3.module';
import { JwtModule } from '../lib/jwt/jwt.module';
import { ImageProcessorModule } from '../lib/image-processor/image-processor.module';
import {ProjectsService} from './projects.service';
import {ProjectsResolver} from './projects.resolver';
import { SessionsModule } from 'src/sessions/sessions.module';
import { SporeModule } from 'src/spore-nft/spore.module';
import { NotificationsModule } from 'src/notifications/notifications.module';
import { ActivityModule } from 'src/activity/activity.module';
import { SeriesModule } from 'src/series/series.module';
import { EventsModule } from 'src/events/events.module';
@Module({
  imports: [
    JwtModule,
    ImageProcessorModule,
    S3Module,
    SporeModule,
    SessionsModule,
    SeriesModule,
    EventsModule,
    NotificationsModule,
    ActivityModule
  ],
  providers: [ProjectsService,ProjectsResolver],
  exports: [ProjectsService],
})
export class ProjectsModule {}