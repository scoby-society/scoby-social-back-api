import { Series } from '../series/series.entity';
import {Event} from '../events/events.entity';
import {Team} from '../team/team.entity';
import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  JoinColumn,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
  ManyToMany,
  JoinTable,
} from 'typeorm';
import { SporeDs } from '../spore-nft/spore-ds.entity';
import { User } from '../users/user.entity';
import { Session } from '../sessions/session.entity';
import { ChatExperience } from '../chatExperience/chatExperience.entity';
function ImageLinkTransformerFrom(value: string | null): string | null {
  if (value == null) return value;
  const bucketName = process.env.AWS_S3_USER_PROFILE_ASSETS_BUCKET;
  const baseUrl = process.env.AWS_S3_BASE_URL;
  return `https://${bucketName}.${baseUrl}/${value}`;
}

function ImageLinkTransformerTo(value: string | null): string | null {
  if (value == null) return value;

  try {
    const url = new URL(value);
    const pathSplitted = url.pathname.split('/');
    return pathSplitted[pathSplitted.length - 1];
  } catch (e) {
    return value;
  }
}

@Entity()
export class Projects {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', nullable: true })
  name?: string | null;

  @Column({ type: 'varchar', nullable: true })
  description?: string | null;

  @Column({
    type: 'varchar',
    nullable: true,
    name: 'cover_image',
    transformer: {
      from: ImageLinkTransformerFrom,
      to: ImageLinkTransformerTo,
    },
  })
  coverImage?: string | null;

  @ManyToMany(() => User, { nullable: true, cascade: true,eager:true })
  @JoinTable({
    name: 'projects_subscribed_users',
    joinColumn: { name: 'project_id' },
    inverseJoinColumn: { name: 'user_id' },
  })
  suscribeUsers?: User[];

  @Column({
    type: 'varchar',
    nullable: true,
    transformer: {
      from: ImageLinkTransformerFrom,
      to: ImageLinkTransformerTo,
    },
  })
  tile?: string | null;

  @Column({ type: 'varchar', nullable: true })
  join?: string | null;

  @Column({ type: 'varchar', nullable: true })
  project?: string | null;

  @CreateDateColumn({ name: 'created_at', nullable: true })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;

  @Column({ type: 'timestamp', nullable: true, name: 'finished_at' })
  finishedAt?: Date | null;

  @ManyToOne(() => User, { nullable: true, eager:true })
  @JoinColumn({ name: 'user_id' })
  user?: User | null;

  @ManyToMany(() => SporeDs, spore => spore.projects, { nullable: false, eager: true, cascade: true })
  @JoinTable({
    name: 'project_nft',
    joinColumn: { name: 'project_id' },
    inverseJoinColumn: { name: 'nft_id' },
  })
  nft: SporeDs[];

  @OneToMany(() => Series, (serie) => serie.projects)
  series?: Series[];

  @OneToMany(() => Event, (event) => event.projects)
  event?: Event[];

  @OneToMany(() => Team, (team) => team.projects)
  team?: Team[];

  @OneToMany(() => Session, (session) => session.projects)
  session?: Session[];

  @OneToMany(() => ChatExperience,(chat) => chat.projects)
  chatExperience?: ChatExperience[];
}
