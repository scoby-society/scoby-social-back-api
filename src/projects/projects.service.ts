import { Injectable } from '@nestjs/common';
import { getRepository, Not, IsNull, In } from 'typeorm';
import { Event } from 'src/events/events.entity';
import { PROJECT_ERRORS } from './projects.messages';
import {
  ProjectView,
  CreateProjectInput,
  projectEdit,
  ExperienceObject,
  ProjectViewById,
} from './projects.graphql';
import { FileUpload } from '../lib/common/common.interfaces';
import { Projects } from './projects.entity';
import { S3Service } from '../lib/s3/s3.service';
import {
  ImageProcessorService,
  ImageTargetType,
} from 'src/lib/image-processor/image-processor.service';
import { SporeService } from 'src/spore-nft/spore.service';
import { SporeDs } from 'src/spore-nft/spore-ds.entity';
import { ApolloError } from 'apollo-server-express';
import { SessionsService } from 'src/sessions/sessions.service';
import { NotificationsService } from 'src/notifications/notifications.service';
import { User } from 'src/users/user.entity';
import { USERS_ERRORS } from 'src/users/users.messages';
import { ActivityActionTypes } from 'src/activity/activity.types';
import { ActivityServices } from 'src/activity/activity.service';
import { Activity } from 'src/activity/activity.entity';
import { Series } from 'src/series/series.entity';
import { GlobalHomeFeed } from 'src/global_search/global_search.graphql';
import { Team } from 'src/team/team.entity';
import { Session } from 'src/sessions/session.entity';
import { ChatExperience } from 'src/chatExperience/chatExperience.entity';
import { SessionObject } from 'src/sessions/sessions.graphql';
import { SeriesServices } from 'src/series/series.service';
import { EventsService } from 'src/events/events.service';
@Injectable()
export class ProjectsService {
  constructor(
    private s3Service: S3Service,
    private imageProcessorService: ImageProcessorService,
    private sporeService: SporeService,
    private sessionsService: SessionsService,
    private notificationsService: NotificationsService,
    private activityServices: ActivityServices,
    private seriesServices: SeriesServices,
    private eventsServices: EventsService,
  ) {}
  async createProject(
    currentUserId: number,
    projectObj: CreateProjectInput,
    coverImage?: FileUpload,
    tile?: FileUpload,
  ): Promise<ProjectView> {
    const repository = getRepository(Projects);
    const {
      name,
      description,
      join,
      nft,
      project,
      invitedUsers,
      teamInvitation,
      isTeamsMembersInvited
    } = projectObj;
    await this.sporeService.verifyCollectionNfts(nft);
    const nftObject = await getRepository(SporeDs).find({
      where: { id: In(nft) },
    });
    const { userIds } = await this.sessionsService.getInvitedUsers(
      invitedUsers,
      currentUserId,
      isTeamsMembersInvited,
      teamInvitation,
    );
    const savedProject = await repository.save({
      name,
      description,
      join,
      project,
      user: {
        id: currentUserId,
      },
      nft: nftObject,
    });

    this.notificationsService.sendProjectCreatedNotifications(
      savedProject.id,
      currentUserId,
      userIds as [number],
    );

    this.activityServices.sendGroupNotificationActivity(
      userIds,
      savedProject.id,
      currentUserId,
      ActivityActionTypes.CREATE_PROJECT,
    );

    await this.uploadFileProjects(
      currentUserId,
      savedProject.id,
      coverImage,
      tile,
    );

    return (await repository.findOne(savedProject.id, {
      relations: ['user', 'nft', 'series', 'event', 'team', 'session'],
    })) as ProjectView;
  }

  async invitedUsersProjects(
    currentUserId: number,
    invitedUsers: [number],
    idProject: number,
    teamInvitation?: [number] | null,
    isTeamsMembersInvited?: boolean,
  ): Promise<ProjectView> {
    const repository = getRepository(Projects);
    const project = (await repository.findOne(idProject, {
      relations: ['user'],
    })) as ProjectView;

    if (!project) {
      throw new ApolloError(
        PROJECT_ERRORS.PROJECT_NOT_FOUND.MESSAGE,
        PROJECT_ERRORS.PROJECT_NOT_FOUND.CODE,
      );
    }
    const { userIds } = await this.sessionsService.getInvitedUsers(
      invitedUsers ?? [],
      currentUserId,
      isTeamsMembersInvited,
      teamInvitation,
    );

    this.notificationsService.sendProjectCreatedNotifications(
      idProject,
      currentUserId,
      userIds as [number],
    );

    this.activityServices.sendGroupNotificationActivity(
      userIds,
      project.id,
      currentUserId,
      ActivityActionTypes.CREATE_PROJECT,
    );

    return project;
  }

  async editProject(
    currentUserId: number,
    projectId: number,
    projectEditObj: projectEdit,
    coverImage?: FileUpload,
    tile?: FileUpload,
  ): Promise<ProjectView> {
    const {
      name,
      description,
      join,
      nft,
      project,
      invitedUsers,
      teamInvitation,
      isTeamsMembersInvited
    } = projectEditObj;
    await this.sporeService.verifyCollectionNfts(nft);
    const nftObject = await getRepository(SporeDs).find({
      where: { id: In(nft) },
    });
    const repository = getRepository(Projects);
    const projectRepository = await repository.findOne(projectId, {
      relations: ['user', 'nft', 'series', 'event', 'team', 'session'],
    });
    const { userIds } = await this.sessionsService.getInvitedUsers(
      invitedUsers,
      currentUserId,
      isTeamsMembersInvited,
      teamInvitation,
    );
    if (!projectRepository) {
      throw new ApolloError(
        PROJECT_ERRORS.PROJECTS_NOT_FOUND.MESSAGE,
        PROJECT_ERRORS.PROJECTS_NOT_FOUND.CODE,
      );
    }

    this.notificationsService.sendProjectCreatedNotifications(
      projectId,
      currentUserId,
      userIds as [number],
    );

    await this.uploadFileProjects(
      currentUserId,
      projectRepository.id,
      coverImage,
      tile,
    );

    await repository.save({
      id: projectId,
      name,
      description,
      join,
      project,
      user: {
        id: currentUserId,
      },
      nft: nftObject,
    });

    return (await repository.findOne(projectId, {
      relations: ['user', 'nft', 'series', 'event', 'team', 'session'],
    })) as ProjectView;
  }

  async uploadFileProjects(
    currentUserId: number,
    idProject?: number,
    coverImage?: FileUpload,
    tile?: FileUpload,
  ): Promise<ProjectView> {
    const repository = getRepository(Projects);
    const project = (await repository.findOne(idProject, {
      relations: ['user'],
    })) as ProjectView;

    if (!project) {
      throw new ApolloError(
        PROJECT_ERRORS.PROJECT_NOT_FOUND.MESSAGE,
        PROJECT_ERRORS.PROJECT_NOT_FOUND.CODE,
      );
    }

    if (project?.user?.id !== currentUserId) {
      throw new ApolloError(
        PROJECT_ERRORS.NOT_PROJECT_OWNER.MESSAGE,
        PROJECT_ERRORS.NOT_PROJECT_OWNER.CODE,
      );
    }

    const removeAssets = [];

    if (tile && project.tile) {
      removeAssets.push(project.tile);
    }

    if (coverImage && project.coverImage) {
      removeAssets.push(project.coverImage);
    }

    if (removeAssets.length) {
      await this.s3Service.removeFiles(removeAssets);
    }

    let tileUpload;
    let coverImageUpload;

    if (tile) {
      tileUpload = this.imageProcessorService.optimizeImage(
        tile.createReadStream(),
        ImageTargetType.AVATAR,
      );
    }

    if (coverImage) {
      coverImageUpload = this.imageProcessorService.optimizeImage(
        coverImage.createReadStream(),
        ImageTargetType.BACKGROUND_IMAGE,
      );
    }

    const [tileResult, coverImageResult] = await Promise.all([
      tileUpload
        ? this.s3Service.uploadFile({
            extension: tileUpload.extension,
            mime: tileUpload.mime,
            stream: tileUpload.stream,
          })
        : undefined,
      coverImageUpload
        ? this.s3Service.uploadFile({
            extension: coverImageUpload.extension,
            mime: coverImageUpload.mime,
            stream: coverImageUpload.stream,
          })
        : undefined,
    ]);

    await repository.save({
      id: idProject,
      tile: tileResult?.Key,
      coverImage: coverImageResult?.Key,
    });

    return (await repository.findOne(idProject)) as ProjectView;
  }

  async getAllProject(): Promise<ProjectView[]> {
    const respository = getRepository(Projects);
    const getAllProject = (await respository.find({
      where: { finishedAt: null },
      relations: [
        'user',
        'nft',
        'series',
        'event',
        'team',
        'session',
        'suscribeUsers',
      ],
    })) as ProjectView[];
    if (!getAllProject) {
      throw new ApolloError(
        PROJECT_ERRORS.PROJECTS_NOT_FOUND.MESSAGE,
        PROJECT_ERRORS.PROJECTS_NOT_FOUND.CODE,
      );
    }
    return getAllProject;
  }

  async searchInviteUserProject(
    target: number,
    procedure: number,
  ): Promise<boolean> {
    const activity = await getRepository(Activity).find({
      where: {
        type_action: 'project',
        targetUser: target,
        procedure_action: procedure,
      },
      relations: ['targetUser', 'sourceUser'],
    });
    if (activity.length > 0) {
      return true;
    }
    return false;
  }

  async getProfileOfUser(
    limit: number,
    page: number,
    category: string,
    currentUserId: number,
    type: string,
    profileId: number | null,
  ): Promise<GlobalHomeFeed[] | undefined> {
    const seriesArray: any = [];
    const eventArray: any = [];
    const sessionArray: any = [];
    const comunityArray: any = [];
    const channelArray: any = [];
    const collectionsArray: any = [];
    const seriesArraySuscribe: any = [];
    const eventArraySuscribe: any = [];
    const sessionArraySuscribe: any = [];
    const comunityArraySuscribe: any = [];
    const collectionsArraySuscribe: any = [];
    const channelArraySuscribe: any = [];
    const seriesArrayUnJoined: any = [];
    const eventArrayUnJoined: any = [];
    const sessionArrayUnJoined: any = [];
    const comunityArrayUnJoined: any = [];
    const collectionsArrayUnJoined: any = [];
    const channelArrayUnJoined: any = [];
    const seriesArrayUnFounder: any = [];
    const eventArrayUnFounder: any = [];
    const sessionArrayUnFounder: any = [];
    const comunityArrayUnFounder: any = [];
    const collectionsArrayUnFounder: any = [];
    const channelArrayUnFounder: any = [];
    if (profileId) {
      /**
       * series objects
       *
       */
      const serie = await getRepository(Series).find({
        where: { finishedAt: null, projects: Not(IsNull()) },
        relations: ['projects', 'ownerUser','suscribeUsers','schedule'],
      });
      const series = serie.filter(
        (se) =>
          se?.projects?.user?.id === profileId &&
          se?.projects?.finishedAt === null,
      );
      await Promise.all(
        serie.map(async (series: Series) => {
          if (
            series?.projects?.suscribeUsers?.some(
              (sus: any) => sus.id === profileId,
            ) &&
            series?.projects?.finishedAt === null
          ) {
            const seri = (await getRepository(Series).findOne({
              where: { id: series.id },
            })) as Series;
            const experienceSecret = await this.seriesServices.getSecretExperiences(
              currentUserId || 1,
            );
            const isAvailable = currentUserId
              ? await this.seriesServices.isAvailable(
                  experienceSecret,
                  seri,
                  currentUserId,
                )
              : false;
            const subscribed = await this.seriesServices.subscribed(
              series?.id,
              currentUserId,
            );
            seriesArraySuscribe.push({
              ...series,
              subscribed,
              isAvailable,
              typeExperience: 'series',
              typeData: 'experience',
            });
          }
          const resultActivityUser = await this.searchInviteUserProject(
            currentUserId,
            series?.projects?.id || 0,
          );
          if (
            series?.projects?.project === 'Unlisted' &&
            series?.projects?.finishedAt === null &&
            (series?.projects?.suscribeUsers?.some(
              (sus: any) => sus.id === currentUserId,
            ) ||
              resultActivityUser)
          ) {
            const seri = (await getRepository(Series).findOne({
              where: { id: series.id },
            })) as Series;
            const experienceSecret = await this.seriesServices.getSecretExperiences(
              currentUserId || 1,
            );
            const isAvailable = currentUserId
              ? await this.seriesServices.isAvailable(
                  experienceSecret,
                  seri,
                  currentUserId,
                )
              : false;
            const subscribed = await this.seriesServices.subscribed(
              series?.id,
              currentUserId,
            );
            seriesArrayUnJoined.push({
              ...series,
              subscribed,
              isAvailable,
              typeExperience: 'series',
              typeData: 'experience',
            });
          }
        }),
      );
      await Promise.all(
        series.map(async (serie: Series) => {
          const seri = (await getRepository(Series).findOne({
            where: { id: serie.id },
          })) as Series;
          const experienceSecret = await this.seriesServices.getSecretExperiences(
            currentUserId || 1,
          );
          const isAvailable = currentUserId
            ? await this.seriesServices.isAvailable(
                experienceSecret,
                seri,
                currentUserId,
              )
            : false;
          const subscribed = await this.seriesServices.subscribed(
            serie?.id,
            currentUserId,
          );
          seriesArray.push({
            ...serie,
            subscribed,
            isAvailable,
            typeExperience: 'series',
            typeData: 'experience',
          });
          const resultActivityUser = await this.searchInviteUserProject(
            currentUserId,
            serie?.projects?.id || 0,
          );
          if (
            serie?.projects?.project === 'Unlisted' &&
            serie?.projects?.finishedAt === null &&
            (serie?.projects?.suscribeUsers?.some(
              (sus: any) => sus.id === currentUserId,
            ) ||
              resultActivityUser)
          ) {
            const seri = (await getRepository(Series).findOne({
              where: { id: serie.id },
            })) as Series;
            const experienceSecret = await this.seriesServices.getSecretExperiences(
              currentUserId || 1,
            );
            const isAvailable = currentUserId
              ? await this.seriesServices.isAvailable(
                  experienceSecret,
                  seri,
                  currentUserId,
                )
              : false;
            const subscribed = await this.seriesServices.subscribed(
              serie?.id,
              currentUserId,
            );
            seriesArrayUnFounder.push({
              ...serie,
              subscribed,
              isAvailable,
              typeExperience: 'series',
              typeData: 'experience',
            });
          }
        }),
      );
      /**
       * events objects
       *
       */
      const event = await getRepository(Event).find({
        where: { finishedAt: null, projects: Not(IsNull()) },
        relations: ['projects','suscribeUsers','ownerUser'],
      });
      const events = event.filter(
        (eve) =>
          eve?.projects?.user?.id === profileId &&
          eve?.projects?.finishedAt === null,
      );
      await Promise.all(
        event.map(async (events: Event) => {
          if (
            events?.projects?.suscribeUsers?.some(
              (sus: any) => sus.id === profileId,
            ) &&
            events?.projects?.finishedAt === null
          ) {
            const even = (await getRepository(Event).findOne({where:{id:events.id}})) as Event;
            const experienceSecret = await this.eventsServices.getSecretExperiences(currentUserId || 1);
            const isAvailable = currentUserId
            ? await this.eventsServices.isPrivateAvailable(
                experienceSecret,
                even,
                currentUserId,
              )
            : false;
            const subscribed = await this.eventsServices.subscribed(events?.id,currentUserId)
            eventArraySuscribe.push({
              ...events,
              isAvailable,
              subscribed,
              typeExperience: 'events',
              typeData: 'experience',
            });
          }
          const resultActivityUser = await this.searchInviteUserProject(
            currentUserId,
            events?.projects?.id || 0,
          );
          if (
            events.projects?.project === 'Unlisted' &&
            (events?.projects?.suscribeUsers?.some(
              (sus: any) => sus.id === currentUserId,
            ) ||
              resultActivityUser)
          ) {
            const even = (await getRepository(Event).findOne({where:{id:events.id}})) as Event;
            const experienceSecret = await this.eventsServices.getSecretExperiences(currentUserId || 1);
            const isAvailable = currentUserId
            ? await this.eventsServices.isPrivateAvailable(
                experienceSecret,
                even,
                currentUserId,
              )
            : false;
            const subscribed = await this.eventsServices.subscribed(events?.id,currentUserId)
            eventArrayUnJoined.push({
              ...events,
              isAvailable,
              subscribed,
              typeExperience: 'series',
              typeData: 'experience',
            });
          }
        }),
      );
      await Promise.all(
        events.map(async (event: Event) => {
          const even = (await getRepository(Event).findOne({where:{id:event.id}})) as Event;
          const experienceSecret = await this.eventsServices.getSecretExperiences(currentUserId || 1);
          const isAvailable = currentUserId
          ? await this.eventsServices.isPrivateAvailable(
              experienceSecret,
              even,
              currentUserId,
            )
          : false;
          const subscribed = await this.eventsServices.subscribed(event?.id,currentUserId)
          eventArray.push({
            ...event,
            isAvailable,
            subscribed,
            typeExperience: 'events',
            typeData: 'experience',
          });
          const resultActivityUser = await this.searchInviteUserProject(
            currentUserId,
            event?.projects?.id || 0,
          );
          if (
            event.projects?.project === 'Unlisted' &&
            event.projects?.finishedAt === null &&
            (event?.projects?.suscribeUsers?.some(
              (sus: any) => sus.id === currentUserId,
            ) ||
              resultActivityUser)
          ) {
            const even = (await getRepository(Event).findOne({where:{id:event.id}})) as Event;
            const experienceSecret = await this.eventsServices.getSecretExperiences(currentUserId || 1);
            const isAvailable = currentUserId
            ? await this.eventsServices.isPrivateAvailable(
                experienceSecret,
                even,
                currentUserId,
              )
            : false;
            const subscribed = await this.eventsServices.subscribed(event?.id,currentUserId)
            eventArrayUnFounder.push({
              ...event,
              isAvailable,
              subscribed,
              typeExperience: 'series',
              typeData: 'experience',
            });
          }
        }),
      );
      /**
       * sessions objects
       *
       */
      const session = await getRepository(Session).find({
        where: { finishedAt: null, projects: Not(IsNull()) },
        relations: ['projects', 'ownerUser', 'viewerUsers'],
      });
      const sessions = session.filter(
        (sess) =>
          sess?.projects?.user?.id === profileId &&
          sess?.projects?.finishedAt === null,
      );
      await Promise.all(
        session.map(async (sessions: Session) => {
          if (
            sessions?.projects?.suscribeUsers?.some(
              (sus: any) => sus.id === profileId,
            ) &&
            sessions?.projects?.finishedAt === null
          ) {
            const sess = (await getRepository(Session).findOne({where:{id:sessions.id}})) as Session;
            const viewers = this.sessionsService.countViewers(sess);
            const experienceSecret = await this.sessionsService.getSecretExperiences(currentUserId || 1);
            const isAvailable = currentUserId
            ? await this.sessionsService.isPrivateAvailable(
                experienceSecret,
                sess,
                currentUserId,
              )
            : false;
            sessionArraySuscribe.push({
              ...sessions,
              isAvailable,
              viewers,
              typeExperience: 'sessions',
              typeData: 'experience',
            });
          }
          const resultActivityUser = await this.searchInviteUserProject(
            currentUserId,
            sessions?.projects?.id || 0,
          );
          if (
            sessions.projects?.project === 'Unlisted' &&
            sessions?.projects?.finishedAt === null &&
            (sessions?.projects?.suscribeUsers?.some(
              (sus: any) => sus.id === currentUserId,
            ) ||
              resultActivityUser)
          ) {
            const sess = (await getRepository(Session).findOne({where:{id:sessions.id}})) as Session;
            const viewers = this.sessionsService.countViewers(sess);
            const experienceSecret = await this.sessionsService.getSecretExperiences(currentUserId || 1);
            const isAvailable = currentUserId
            ? await this.sessionsService.isPrivateAvailable(
                experienceSecret,
                sess,
                currentUserId,
              )
            : false;
            sessionArrayUnJoined.push({
              ...sessions,
              isAvailable,
              viewers,
              typeExperience: 'series',
              typeData: 'experience',
            });
          }
        }),
      );
      await Promise.all(
        sessions.map(async (session) => {
          const sess = (await getRepository(Session).findOne({where:{id:session.id}})) as Session;
          const viewers = this.sessionsService.countViewers(sess);
          const experienceSecret = await this.sessionsService.getSecretExperiences(currentUserId || 1);
          const isAvailable = currentUserId
          ? await this.sessionsService.isPrivateAvailable(
              experienceSecret,
              sess,
              currentUserId,
            )
          : false;
          sessionArray.push({
            ...session,
            isAvailable,
            viewers,
            typeExperience: 'sessions',
            typeData: 'experience',
          });
          const resultActivityUser = await this.searchInviteUserProject(
            currentUserId,
            session?.projects?.id || 0,
          );
          if (
            session.projects?.project === 'Unlisted' &&
            (session?.projects?.suscribeUsers?.some(
              (sus: any) => sus.id === currentUserId,
            ) ||
              resultActivityUser)
          ) {
            const sess = (await getRepository(Session).findOne({where:{id:session.id}})) as Session;
            const viewers = this.sessionsService.countViewers(sess);
            const experienceSecret = await this.sessionsService.getSecretExperiences(currentUserId || 1);
            const isAvailable = currentUserId
            ? await this.sessionsService.isPrivateAvailable(
                experienceSecret,
                sess,
                currentUserId,
              )
            : false;
            sessionArrayUnFounder.push({
              ...session,
              isAvailable,
              viewers,
              typeExperience: 'series',
              typeData: 'experience',
            });
          }
        }),
      );
      /**
       * comunity objects
       *
       */
      const comunity = await getRepository(Team).find({
        where: { finishedAt: null, projects: Not(IsNull()) },
        relations: ['projects', 'ownerUser','members'],
      });
      const comunitys = comunity.filter(
        (com) =>
          com?.projects?.user?.id === profileId &&
          com?.projects?.finishedAt === null,
      );

      await Promise.all(
        comunity.map(async (comunitys: Team) => {
          if (
            comunitys?.projects?.suscribeUsers?.some(
              (sus: any) => sus.id === currentUserId,
            ) &&
            comunitys?.projects?.finishedAt === null
          ) {
            comunityArraySuscribe.push({
              ...comunitys,
              typeData: 'comunity',
            });
          }
          const resultActivityUser = await this.searchInviteUserProject(
            currentUserId,
            comunitys?.projects?.id || 0,
          );
          if (
            comunitys.projects?.project === 'Unlisted' &&
            comunitys?.projects?.finishedAt === null &&
            (comunitys?.projects?.suscribeUsers?.some(
              (sus: any) => sus.id === currentUserId,
            ) ||
              resultActivityUser)
          ) {
            comunityArrayUnJoined.push({ ...comunity, typeData: 'comunity' });
          }
        }),
      );

      await Promise.all(
        comunitys.map(async (comunity) => {
          comunityArray.push({ ...comunity, typeData: 'comunity' });
          const resultActivityUser = await this.searchInviteUserProject(
            currentUserId,
            comunity?.projects?.id || 0,
          );
          if (
            comunity.projects?.project === 'Unlisted' &&
            (comunity?.projects?.suscribeUsers?.some(
              (sus: any) => sus.id === currentUserId,
            ) ||
              resultActivityUser)
          ) {
            comunityArrayUnFounder.push({ ...comunity, typeData: 'comunity' });
          }
        }),
      );
      /**
       * channels objects
       *
       */
      const channel = await getRepository(ChatExperience).find({
        where: { finishedAt: null, projects: Not(IsNull()) },
        relations: ['projects', 'ownerUser','suscribeUsers'],
      });

      const channels = channel.filter(
        (cha) =>
          cha?.projects?.user?.id === profileId &&
          cha?.projects?.finishedAt === null,
      );

      await Promise.all(
        channel.map(async (channels: ChatExperience) => {
          if (
            channels?.projects?.suscribeUsers?.some(
              (sus: any) => sus.id === currentUserId,
            ) &&
            channels?.projects?.finishedAt === null
          ) {
            channelArraySuscribe.push({
              ...channels,
              typeExperience: 'channels',
              typeData: 'experience',
            });
          }
          const resultActivityUser = await this.searchInviteUserProject(
            currentUserId,
            channels?.projects?.id || 0,
          );
          if (
            channels.projects?.project === 'Unlisted' &&
            channels?.projects?.finishedAt === null &&
            (channels?.projects?.suscribeUsers?.some(
              (sus: any) => sus.id === currentUserId,
            ) ||
              resultActivityUser)
          ) {
            channelArrayUnJoined.push({
              ...channels,
              typeExperience: 'channels',
              typeData: 'experience',
            });
          }
        }),
      );

      await Promise.all(
        channels.map(async (channel) => {
          channelArray.push({
            ...channel,
            typeExperience: 'channels',
            typeData: 'experience',
          });
          const resultActivityUser = await this.searchInviteUserProject(
            currentUserId,
            channel?.projects?.id || 0,
          );
          if (
            channel.projects?.project === 'Unlisted' &&
            (channel?.projects?.suscribeUsers?.some(
              (sus: any) => sus.id === currentUserId,
            ) ||
              resultActivityUser)
          ) {
            channelArrayUnFounder.push({
              ...channel,
              typeExperience: 'channels',
              typeData: 'experience',
            });
          }
        }),
      );

      const project = await getRepository(Projects).find({
        where: { finishedAt: null},
        relations: ['nft', 'suscribeUsers', 'user'],
      });
      const projects = project.filter(
        (pro) => pro?.user?.id === profileId && pro?.finishedAt === null
      );
      await Promise.all(
        project.map(async (projects: Projects) => {
          if (
            projects?.suscribeUsers?.some((sus: any) => sus.id === profileId) &&
            projects?.finishedAt === null 
          ) {
            const projectsStatus = await this.searchProjectStatus(projects?.id);
            if(projects?.nft?.length > 0){
              collectionsArraySuscribe.push({
                ...projects,
                typeData: 'collections',
              });
            }
            if(projects?.nft?.length <= 0 && projectsStatus === false){
              collectionsArraySuscribe.push({
                ...projects,
                typeData: 'collections',
              });
            }
          }
          const resultActivityUser = await this.searchInviteUserProject(
            currentUserId,
            projects?.id || 0,
          );
          if (
            projects?.project === 'Unlisted' &&
            (projects?.suscribeUsers?.some(
              (sus: any) => sus.id === currentUserId,
            ) ||
              resultActivityUser)
          ) {
            const projectsStatus = await this.searchProjectStatus(projects?.id);
            if(projects?.nft?.length > 0){
              collectionsArrayUnJoined.push({
                ...projects,
                typeData: 'collections',
              });
            }
            if(projects?.nft?.length <= 0 && projectsStatus === false){
              collectionsArrayUnJoined.push({
                ...projects,
                typeData: 'collections',
              });
            }
          }
        }),
      );

      await Promise.all(
        projects.map(async (projects) => {
          const projectsStatus = await this.searchProjectStatus(projects?.id);
          if(projects?.nft?.length > 0){
            collectionsArray.push({ ...projects, typeData: 'collections' });
          }
          if(projects?.nft?.length <= 0 && projectsStatus === false){
            collectionsArray.push({ ...projects, typeData: 'collections' });
          }
          const resultActivityUser = await this.searchInviteUserProject(
            currentUserId,
            projects?.id || 0,
          );
          if (
            projects?.project === 'Unlisted' &&
            (projects?.suscribeUsers?.some(
              (sus: any) => sus.id === currentUserId,
            ) ||
              resultActivityUser)
          ) {
            if(projects?.nft?.length > 0){
              collectionsArrayUnFounder.push({
                ...projects,
                typeData: 'collections',
              });
            }
            if(projects?.nft?.length <= 0 && projectsStatus === false){
              collectionsArrayUnFounder.push({
                ...projects,
                typeData: 'collections',
              });
            }
          }
        }),
      );
      if (category === 'all') {
        if (type === 'all') {
          return [
            ...seriesArray,
            ...seriesArraySuscribe,
            ...seriesArrayUnJoined,
            ...seriesArrayUnFounder,
            ...eventArray,
            ...eventArraySuscribe,
            ...eventArrayUnJoined,
            ...eventArrayUnFounder,
            ...sessionArray,
            ...sessionArraySuscribe,
            ...sessionArrayUnJoined,
            ...sessionArrayUnFounder,
            ...channelArray,
            ...channelArraySuscribe,
            ...channelArrayUnJoined,
            ...channelArrayUnFounder,
            ...comunityArray,
            ...comunityArraySuscribe,
            ...comunityArrayUnJoined,
            ...comunityArrayUnFounder,
            ...collectionsArray,
            ...collectionsArraySuscribe,
            ...collectionsArrayUnJoined,
            ...collectionsArrayUnFounder,
          ].slice(page * limit - limit, page * limit);
        }
        if (type === 'collection') {
          return [
            ...collectionsArray,
            ...collectionsArraySuscribe,
            ...collectionsArrayUnJoined,
            ...collectionsArrayUnFounder,
          ].slice(page * limit - limit, page * limit);
        }
        if (type === 'experience') {
          return [
            ...seriesArray,
            ...seriesArraySuscribe,
            ...seriesArrayUnJoined,
            ...seriesArrayUnFounder,
            ...eventArray,
            ...eventArraySuscribe,
            ...eventArrayUnJoined,
            ...eventArrayUnFounder,
            ...sessionArray,
            ...sessionArraySuscribe,
            ...sessionArrayUnJoined,
            ...sessionArrayUnFounder,
            ...channelArray,
            ...channelArraySuscribe,
            ...channelArrayUnJoined,
            ...channelArrayUnFounder,
          ].slice(page * limit - limit, page * limit);
        }
        if (type === 'comunity') {
          return [
            ...comunityArray,
            ...comunityArraySuscribe,
            ...comunityArrayUnJoined,
            ...comunityArrayUnFounder,
          ].slice(page * limit - limit, page * limit);
        }
      }

      if (category === 'founder') {
        if (type === 'all') {
          return [
            ...seriesArray,
            ...seriesArrayUnFounder,
            ...eventArray,
            ...eventArrayUnFounder,
            ...sessionArray,
            ...sessionArrayUnFounder,
            ...channelArray,
            ...channelArrayUnFounder,
            ...comunityArray,
            ...comunityArrayUnFounder,
            ...collectionsArray,
            ...collectionsArrayUnFounder,
          ].slice(page * limit - limit, page * limit);
        }
        if (type === 'collection') {
          return [...collectionsArray, ...collectionsArrayUnFounder].slice(
            page * limit - limit,
            page * limit,
          );
        }
        if (type === 'experience') {
          return [
            ...seriesArray,
            ...seriesArrayUnFounder,
            ...eventArray,
            ...eventArrayUnFounder,
            ...sessionArray,
            ...sessionArrayUnFounder,
            ...channelArray,
            ...channelArrayUnFounder,
          ].slice(page * limit - limit, page * limit);
        }
        if (type === 'comunity') {
          return [...comunityArray, ...comunityArrayUnFounder].slice(
            page * limit - limit,
            page * limit,
          );
        }
      }

      if (category === 'joined') {
        if (type === 'all') {
          return [
            ...seriesArraySuscribe,
            ...seriesArrayUnJoined,
            ...eventArraySuscribe,
            ...eventArrayUnJoined,
            ...sessionArraySuscribe,
            ...sessionArrayUnJoined,
            ...channelArraySuscribe,
            ...channelArrayUnJoined,
            ...comunityArraySuscribe,
            ...comunityArrayUnJoined,
            ...collectionsArraySuscribe,
            ...collectionsArrayUnJoined,
          ].slice(page * limit - limit, page * limit);
        }
        if (type === 'collection') {
          return [
            ...collectionsArraySuscribe,
            ...collectionsArrayUnJoined,
          ].slice(page * limit - limit, page * limit);
        }
        if (type === 'experience') {
          return [
            ...seriesArraySuscribe,
            ...seriesArrayUnJoined,
            ...eventArraySuscribe,
            ...eventArrayUnJoined,
            ...sessionArraySuscribe,
            ...sessionArrayUnJoined,
            ...channelArraySuscribe,
            ...channelArrayUnJoined,
          ].slice(page * limit - limit, page * limit);
        }
        if (type === 'comunity') {
          return [...comunityArraySuscribe, ...collectionsArrayUnJoined].slice(
            page * limit - limit,
            page * limit,
          );
        }
      }
    }
    if (!profileId) {
      /**
       * series objects
       *
       */
      const serie = await getRepository(Series).find({
        where: { finishedAt: null, projects: Not(IsNull()) },
        relations: ['projects', 'ownerUser','suscribeUsers','schedule'],
      });
      const series = serie.filter(
        (se) =>
          se?.projects?.user?.id === currentUserId &&
          se?.projects?.finishedAt === null,
      );
      await Promise.all(
        serie.map(async (series: Series) => {
          if (
            series?.projects?.suscribeUsers?.some(
              (sus: any) => sus.id === currentUserId,
            ) &&
            series?.projects?.finishedAt === null
          ) {
            const seri = (await getRepository(Series).findOne({where:{id:series.id}})) as Series;
            const experienceSecret = await this.seriesServices.getSecretExperiences(currentUserId || 1);
            const isAvailable = currentUserId
            ? await this.seriesServices.isAvailable(
                experienceSecret,
                seri,
                currentUserId,
              )
            : false;
            const subscribed = await this.seriesServices.subscribed(series?.id,currentUserId)
            seriesArraySuscribe.push({
              ...series,
              subscribed,
              isAvailable,
              typeExperience: 'series',
              typeData: 'experience',
            });
          }
        }),
      );
      await Promise.all(
        series.map(async (serie: Series) => {
          const seri = (await getRepository(Series).findOne({where:{id:serie.id}})) as Series;
          const experienceSecret = await this.seriesServices.getSecretExperiences(currentUserId || 1);
          const isAvailable = currentUserId
          ? await this.seriesServices.isAvailable(
              experienceSecret,
              seri,
              currentUserId,
            )
          : false;
          const subscribed = await this.seriesServices.subscribed(serie?.id,currentUserId)
          seriesArray.push({
            ...serie,
            subscribed,
            isAvailable,
            typeExperience: 'series',
            typeData: 'experience',
          });
        }),
      );
      /**
       * events objects
       *
       */
      const event = await getRepository(Event).find({
        where: { finishedAt: null, projects: Not(IsNull()) },
        relations: ['projects', 'ownerUser','suscribeUsers'],
      });
      const events = event.filter(
        (eve) =>
          eve?.projects?.user?.id === currentUserId &&
          eve?.projects?.finishedAt === null,
      );
      await Promise.all(
        event.map(async (events: Event) => {
          if (
            events?.projects?.suscribeUsers?.some(
              (sus: any) => sus.id === currentUserId,
            ) &&
            events?.projects?.finishedAt === null
          ) {
          const even = (await getRepository(Event).findOne({where:{id:events.id}})) as Event;
          const experienceSecret = await this.eventsServices.getSecretExperiences(currentUserId || 1);
          const isAvailable = currentUserId
          ? await this.eventsServices.isPrivateAvailable(
              experienceSecret,
              even,
              currentUserId,
            )
          : false;
          const subscribed = await this.eventsServices.subscribed(events?.id,currentUserId)
            eventArraySuscribe.push({
              ...events,
              isAvailable,
              subscribed,
              typeExperience: 'events',
              typeData: 'experience',
            });
          }
        }),
      );
      await Promise.all(
        events.map(async (event: Event) => {
          const even = (await getRepository(Event).findOne({where:{id:event.id}})) as Event;
          const experienceSecret = await this.eventsServices.getSecretExperiences(currentUserId || 1);
          const isAvailable = currentUserId
          ? await this.eventsServices.isPrivateAvailable(
              experienceSecret,
              even,
              currentUserId,
            )
          : false;
          const subscribed = await this.eventsServices.subscribed(event?.id,currentUserId);
          eventArray.push({
            ...event,
            isAvailable,
            subscribed,
            typeExperience: 'events',
            typeData: 'experience',
          });
        }),
      );
      /**
       * sessions objects
       *
       */
      const session = await getRepository(Session).find({
        where: { finishedAt: null, projects: Not(IsNull()) },
        relations: ['projects', 'ownerUser', 'viewerUsers'],
      });
      const sessions = session.filter(
        (sess) =>
          sess?.projects?.user?.id === currentUserId &&
          sess?.projects?.finishedAt === null,
      );
      await Promise.all(
        session.map(async (sessions: Session) => {
          if (
            sessions?.projects?.suscribeUsers?.some(
              (sus: any) => sus.id === currentUserId,
            ) &&
            sessions?.projects?.finishedAt === null
          ) {
            const sess = (await getRepository(Session).findOne({where:{id:sessions.id}})) as Session;
            const viewers = this.sessionsService.countViewers(sess);
            const experienceSecret = await this.sessionsService.getSecretExperiences(currentUserId || 1);
            const isAvailable = currentUserId
            ? await this.sessionsService.isPrivateAvailable(
                experienceSecret,
                sess,
                currentUserId,
              )
            : false;
            sessionArraySuscribe.push({
              ...sessions,
              isAvailable,
              viewers,
              typeExperience: 'sessions',
              typeData: 'experience',
            });
          }
        }),
      );
      await Promise.all(
        sessions.map(async (session) => {
          const sess = (await getRepository(Session).findOne({where:{id:session.id}})) as Session;
            const viewers = this.sessionsService.countViewers(sess);
            const experienceSecret = await this.sessionsService.getSecretExperiences(currentUserId || 1);
            const isAvailable = currentUserId
            ? await this.sessionsService.isPrivateAvailable(
                experienceSecret,
                sess,
                currentUserId,
              )
            : false;
          sessionArray.push({
            ...session,
            isAvailable,
            viewers,
            typeExperience: 'sessions',
            typeData: 'experience',
          });
        }),
      );

      const comunity = await getRepository(Team).find({
        where: { finishedAt: null, projects: Not(IsNull()) },
        relations: ['projects', 'ownerUser','members'],
      });
      const comunitys = comunity.filter(
        (com) =>
          com?.projects?.user?.id === currentUserId &&
          com?.projects?.finishedAt === null,
      );
      await Promise.all(
        comunitys.map(async (comunity) => {
          comunityArray.push({ ...comunity, typeData: 'comunity' });
        }),
      );

      await Promise.all(
        comunity.map(async (comunitys: Team) => {
          if (
            comunitys?.projects?.suscribeUsers?.some(
              (sus: any) => sus.id === currentUserId,
            ) &&
            comunitys?.projects?.finishedAt === null
          ) {
            comunityArraySuscribe.push({
              ...comunitys,
              typeData: 'comunity',
            });
          }
        }),
      );

      /**
       * channels objects
       *
       */
      const channel = await getRepository(ChatExperience).find({
        where: { finishedAt: null, projects: Not(IsNull()) },
        relations: ['projects', 'ownerUser','suscribeUsers'],
      });

      const channels = channel.filter(
        (cha) =>
          cha?.projects?.user?.id === currentUserId &&
          cha?.projects?.finishedAt === null,
      );

      await Promise.all(
        channel.map(async (channels: ChatExperience) => {
          if (
            channels?.projects?.suscribeUsers?.some(
              (sus: any) => sus.id === currentUserId,
            ) &&
            channels?.projects?.finishedAt === null
          ) {
            channelArraySuscribe.push({
              ...channels,
              typeExperience: 'channels',
              typeData: 'experience',
            });
          }
        }),
      );

      await Promise.all(
        channels.map(async (channel) => {
          channelArray.push({
            ...channel,
            typeExperience: 'channels',
            typeData: 'experience',
          });
        }),
      );

      const project = await getRepository(Projects).find({
        where: { finishedAt: null},
        relations: ['nft', 'suscribeUsers', 'user'],
      });
      const projects = project.filter(
        (pro) => pro?.user?.id === currentUserId && pro?.finishedAt === null,
      );
      await Promise.all(
        projects.map(async (projects) => {
          const projectsStatus = await this.searchProjectStatus(projects?.id);
          if(projects?.nft?.length > 0){
            collectionsArray.push({ ...projects, typeData: 'collections' });
          }
          if(projects?.nft?.length <= 0 && projectsStatus === false){
            collectionsArray.push({ ...projects, typeData: 'collections' });
          }
        }),
      );
      await Promise.all(
        project.map(async (projects: Projects) => {
          if (
            projects?.suscribeUsers?.some(
              (sus: any) => sus.id === currentUserId,
            ) &&
            projects?.finishedAt === null
          ) {
            const projectsStatus = await this.searchProjectStatus(projects?.id);
            if(projects?.nft?.length > 0){
              collectionsArraySuscribe.push({
              ...projects,
              typeData: 'collections',
            });
            }
            if(projects?.nft?.length <= 0 && projectsStatus === false){
              collectionsArraySuscribe.push({
              ...projects,
              typeData: 'collections',
            });
            }
          }
        }),
      );
      if (category === 'all') {
        if (type === 'all') {
          return [
            ...seriesArray,
            ...seriesArraySuscribe,
            ...eventArray,
            ...eventArraySuscribe,
            ...sessionArray,
            ...sessionArraySuscribe,
            ...channelArray,
            ...channelArraySuscribe,
            ...comunityArray,
            ...comunityArraySuscribe,
            ...collectionsArray,
            ...collectionsArraySuscribe,
          ].slice(page * limit - limit, page * limit);
        }
        if (type === 'collection') {
          return [...collectionsArray, ...collectionsArraySuscribe].slice(
            page * limit - limit,
            page * limit,
          );
        }
        if (type === 'experience') {
          return [
            ...seriesArray,
            ...seriesArraySuscribe,
            ...eventArray,
            ...eventArraySuscribe,
            ...sessionArray,
            ...sessionArraySuscribe,
            ...channelArray,
            ...channelArraySuscribe,
          ].slice(page * limit - limit, page * limit);
        }
        if (type === 'comunity') {
          return [...comunityArray, ...comunityArraySuscribe].slice(
            page * limit - limit,
            page * limit,
          );
        }
      }

      if (category === 'founder') {
        if (type === 'all') {
          return [
            ...seriesArray,
            ...eventArray,
            ...sessionArray,
            ...channelArray,
            ...comunityArray,
            ...collectionsArray,
          ].slice(page * limit - limit, page * limit);
        }
        if (type === 'collection') {
          return [...collectionsArray].slice(
            page * limit - limit,
            page * limit,
          );
        }
        if (type === 'experience') {
          return [
            ...seriesArray,
            ...eventArray,
            ...sessionArray,
            ...channelArray,
          ].slice(page * limit - limit, page * limit);
        }
        if (type === 'comunity') {
          return [...comunityArray].slice(page * limit - limit, page * limit);
        }
      }

      if (category === 'joined') {
        if (type === 'all') {
          return [
            ...seriesArraySuscribe,
            ...eventArraySuscribe,
            ...sessionArraySuscribe,
            ...channelArraySuscribe,
            ...comunityArraySuscribe,
            ...collectionsArraySuscribe,
          ].slice(page * limit - limit, page * limit);
        }
        if (type === 'collection') {
          return [...collectionsArraySuscribe].slice(
            page * limit - limit,
            page * limit,
          );
        }
        if (type === 'experience') {
          return [
            ...seriesArraySuscribe,
            ...eventArraySuscribe,
            ...sessionArraySuscribe,
            ...channelArraySuscribe,
          ].slice(page * limit - limit, page * limit);
        }
        if (type === 'comunity') {
          return [...comunityArraySuscribe].slice(
            page * limit - limit,
            page * limit,
          );
        }
      }
    }
  }

  async searchProjectStatus(projectId:number): Promise<boolean>{
    let seriesBand,eventBand,sessionBand,comunityBand,channelBand;
    const respository = getRepository(Projects);
    const getProject = (await respository.findOne({
      where: { id: projectId, finishedAt: null },
      relations: [
        'user',
        'series',
        'nft',
        'suscribeUsers',
        'event',
        'team',
        'session',
        'chatExperience',
      ],
    })) as ProjectView;
    if(getProject?.series){
      if(getProject?.series?.length > 0){
        seriesBand = true;
      }else {
       seriesBand = false
      }
    }else{
      seriesBand = false;
    }
    if(getProject?.event){
      if(getProject?.event?.length > 0){
        eventBand = true;
      }else {
       eventBand = false
      }
    }else{
      eventBand = false;
    }
    if(getProject?.session){
      if(getProject?.session?.length > 0){
        sessionBand = true;
      }else {
       sessionBand = false
      }
    }else{
      sessionBand = false;
    }
    if(getProject?.chatExperience){
      if(getProject?.chatExperience?.length > 0){
        channelBand = true;
      }else {
       channelBand = false
      }
    }else{
      channelBand = false;
    }
    if(getProject?.team){
      if(getProject?.team?.length > 0){
        comunityBand = true;
      }else {
       comunityBand = false
      }
    }else{
      comunityBand = false;
    }
    if(seriesBand || sessionBand || eventBand || channelBand || comunityBand){
      return true;
    }else{
      return false;
    }
  }

  async getProjectById(
    projectId: number | null,
    currentUserId: number | null,
  ): Promise<ProjectViewById> {
    const seriesArray: ExperienceObject | any = [];
    const eventArray: ExperienceObject | any = [];
    const sessionArray: ExperienceObject | any = [];
    const channelArray: ExperienceObject | any = [];
    let ownerProject, suscribeProject;
    const respository = getRepository(Projects);
    const getProject = (await respository.findOne({
      where: { id: projectId, finishedAt: null },
      relations: [
        'user',
        'series',
        'nft',
        'suscribeUsers',
        'event',
        'team',
        'session',
        'chatExperience',
      ],
    })) as ProjectView;
    if (!getProject) {
      throw new ApolloError(
        PROJECT_ERRORS.PROJECT_NOT_FOUND.MESSAGE,
        PROJECT_ERRORS.PROJECT_NOT_FOUND.CODE,
      );
    }
    if (getProject?.user?.id === currentUserId) {
      ownerProject = true;
    } else {
      ownerProject = false;
    }
    const suscribe = getProject.suscribeUsers?.some(
      (p) => p.id === currentUserId,
    );
    if (suscribe) {
      suscribeProject = true;
    } else {
      suscribeProject = false;
    }
    getProject?.chatExperience?.map((channel) => {
      if(channel?.finishedAt === null){
        channelArray.push({ ...channel, typeExperience: 'channel' });
      }
    });
    getProject?.series?.map((serie) => {
      if(serie?.finishedAt === null){
        seriesArray.push({ ...serie, typeExperience: 'series' });
      }
    });
    getProject?.event?.map((even) => {
      if(even?.finishedAt === null){
        eventArray.push({ ...even, typeExperience: 'events' });
      }
    });
    if (getProject.session) {
      await Promise.all(
        getProject.session.map(async (sessions) => {
          const sess = (await getRepository(Session).findOne({
            where: { id: sessions?.id },
          })) as Session;
          const viewers = this.sessionsService.countViewers(sess);
          const experienceSecret = await this.sessionsService.getSecretExperiences(
            currentUserId || 1,
          );
          const isAvailable = currentUserId
            ? await this.sessionsService.isPrivateAvailable(
                experienceSecret,
                sess,
                currentUserId,
              )
            : false;
            if(sessions?.finishedAt === null){
              sessionArray.push({
                ...sessions,
                isAvailable,
                viewers,
                typeExperience: 'sessions',
              });
            }
        }),
      );
    }
    const experience = [
      ...seriesArray,
      ...sessionArray,
      ...eventArray,
      ...channelArray,
    ];
    return { ...getProject, experience, ownerProject, suscribeProject };
  }

  async deleteProjectById(
    currentUserId: number,
    projectId: number,
  ): Promise<ProjectView> {
    const repository = getRepository(Projects);
    const project = await repository.findOne({
      where: { id: projectId },
      relations: ['user'],
    });
    if (!project) {
      throw new ApolloError(
        PROJECT_ERRORS.PROJECT_NOT_FOUND.MESSAGE,
        PROJECT_ERRORS.PROJECT_NOT_FOUND.CODE,
      );
    }
    if (project.finishedAt || project.finishedAt !== null) {
      throw new ApolloError(
        PROJECT_ERRORS.PROJECT_FINISHED.MESSAGE,
        PROJECT_ERRORS.PROJECT_FINISHED.CODE,
      );
    }
    if (project?.user?.id !== currentUserId) {
      throw new ApolloError(
        PROJECT_ERRORS.NOT_PROJECT_OWNER.MESSAGE,
        PROJECT_ERRORS.NOT_PROJECT_OWNER.CODE,
      );
    }

    const finishedAt = new Date(Date.now());
    await repository.save({ id: projectId, finishedAt });
    return (await repository.findOne(projectId, {
      relations: ['user', 'nft', 'series', 'event', 'team', 'session'],
    })) as ProjectView;
  }

  async joinProject(
    currentUserId: number,
    projectId: number,
  ): Promise<ProjectView | any> {
    const repository = getRepository(Projects);
    const userRepository = getRepository(User);
    const project = await repository.findOne(projectId, {
      relations: ['user', 'suscribeUsers'],
    });
    const userToAdd = await userRepository.findOne(currentUserId);

    if (!project) {
      throw new ApolloError(
        PROJECT_ERRORS.PROJECTS_NOT_FOUND.MESSAGE,
        PROJECT_ERRORS.PROJECTS_NOT_FOUND.CODE,
      );
    }

    if (!userToAdd) {
      throw new ApolloError(
        USERS_ERRORS.USER_NOT_FOUND.MESSAGE,
        USERS_ERRORS.USER_NOT_FOUND.CODE,
      );
    }

    if (project.finishedAt) {
      throw new ApolloError(
        PROJECT_ERRORS.PROJECT_FINISHED.MESSAGE,
        PROJECT_ERRORS.PROJECT_FINISHED.CODE,
      );
    }
    if (project.join === 'Open') {
      const suscribeUsers = project.suscribeUsers || [];
      suscribeUsers.push(userToAdd as User);
      await repository.save({ id: projectId, suscribeUsers });
      const savedProject = (await repository.findOne(projectId, {
        relations: ['user', 'suscribeUsers', 'nft'],
      })) as ProjectView;
      return savedProject;
    }
    if (project.join === 'Closed') {
      const inviteUser = await this.getInvitedUserIdProject(project.id);
      const userNft = await this.isUserNftRequired(currentUserId, project.id);
      if (inviteUser.length > 0 && userNft) {
        const suscribeUsers = project.suscribeUsers || [];
        suscribeUsers.push(userToAdd as User);
        await repository.save({ id: projectId, suscribeUsers });
        const savedProject = (await repository.findOne(projectId, {
          relations: ['user', 'suscribeUsers', 'nft'],
        })) as ProjectView;
        return savedProject;
      } else {
        throw new ApolloError(
          PROJECT_ERRORS.PROJECT_JOIN_ERROR.MESSAGE,
          PROJECT_ERRORS.PROJECT_JOIN_ERROR.CODE,
        );
      }
    }
    if (project.join === 'Exclusived') {
      const userNft = await this.isUserNftRequired(currentUserId, project.id);
      if (userNft) {
        const suscribeUsers = project.suscribeUsers || [];
        suscribeUsers.push(userToAdd as User);
        await repository.save({ id: projectId, suscribeUsers });
        const savedProject = (await repository.findOne(projectId, {
          relations: ['user', 'suscribeUsers', 'nft'],
        })) as ProjectView;
        return savedProject;
      } else {
        throw new ApolloError(
          PROJECT_ERRORS.PROJECT_JOIN_ERROR.MESSAGE,
          PROJECT_ERRORS.PROJECT_JOIN_ERROR.CODE,
        );
      }
    }
  }

  async leaveProject(
    currentUserId: number,
    projectId: number,
  ): Promise<ProjectView> {
    const repository = getRepository(Projects);
    const project = await repository.findOne({
      where: { id: projectId },
      relations: ['user', 'suscribeUsers'],
    });
    const userRepository = getRepository(User);
    const userToLeave = await userRepository.findOne(currentUserId);
    if (!project) {
      throw new ApolloError(
        PROJECT_ERRORS.PROJECT_NOT_FOUND.MESSAGE,
        PROJECT_ERRORS.PROJECT_NOT_FOUND.CODE,
      );
    }
    if (!userToLeave || !userToLeave.email) {
      throw new ApolloError(
        USERS_ERRORS.USER_NOT_FOUND.MESSAGE,
        USERS_ERRORS.USER_NOT_FOUND.CODE,
      );
    }
    const suscribeUsers = project?.suscribeUsers?.filter(
      (user) => user.id !== currentUserId,
    );
    await repository.save({ id: projectId, suscribeUsers });
    return (await repository.findOne({
      where: { id: projectId },
      relations: ['suscribeUsers'],
    })) as ProjectView;
  }

  async getInvitedUserIdProject(idProject: number): Promise<number[]> {
    const activities = await getRepository(Activity)
      .createQueryBuilder('activity')
      .leftJoinAndSelect('activity.sourceUser', 'sourceUser')
      .leftJoinAndSelect('activity.targetUser', 'targetUser')
      .where('activity.procedure_action=:ids AND activity.type_action=:type', {
        ids: idProject,
        type: ActivityActionTypes.CREATE_PROJECT,
      })
      .getMany();

    return activities.map((activity) => activity.targetUser.id);
  }

  async isUserNftRequired(
    currentUserId: number,
    projectId: number,
  ): Promise<boolean> {
    const project = await this.getProjectById(projectId, currentUserId);
    if (!project.nft) {
      return true;
    }
    for (const nft of project.nft) {
      if (nft.symbol) {
        const user = await this.sporeService.getNftByUser(
          currentUserId,
          nft.symbol,
        );
        if (user.length === 0) {
          return false;
        }
      }
    }
    return true;
  }
}
