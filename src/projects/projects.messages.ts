export const PROJECT_ERRORS = {
    PROJECT_NOT_FOUND: {
      CODE: 'ERR_PROJECT_NOT_FOUND',
      MESSAGE: 'Project not found',
    },
    PROJECT_JOIN_ERROR: {
      CODE: 'PROJECT_JOIN_ERROR',
      MESSAGE: 'Do not meet the requirements to join this project',
    },
    PROJECTS_NOT_FOUND: {
        CODE: 'ERR_PROJECTS_NOT_FOUND',
        MESSAGE: 'Projects not found',
      },
    PROJECT_FINISHED: {
      CODE: 'ERR_PROJECT_FINISHED',
      MESSAGE: 'Project has been finished',
    },
    NOT_PROJECT_OWNER: {
      CODE: 'ERR_NOT_PROJECT_OWNER',
      MESSAGE: 'User is not owner of the project',
    },
    PROJECT_IS_LIVE: {
      CODE: 'ERR_PROJECT_IS_LIVE',
      MESSAGE: 'The project is now live ',
    },
    PROJECT_IS_PRIVATE: {
      CODE: 'ERR_PROJECT_IS_PRIVATE',
      MESSAGE: 'The user does not have an invitation to this project',
    },
    PROJECT_REQUIRED_NFT: {
      CODE: 'ERR_REQUIRED_NFT',
      MESSAGE: 'The user does not have the required nft',
    },
  };
  