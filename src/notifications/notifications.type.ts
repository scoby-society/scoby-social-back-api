export enum NotificationsTypes {
  CREATE_TEAM = 'startedTeam',
  CREATE_SESSION = 'startedSession',
  CREATE_SERIE = 'startedSerie',
  CREATE_EVENT = 'startedEvent',
  FOLLOW_USER = 'followUser',
  SEND_OWNER_APP = 'sendOwnerAppTeam',
  UPDATE_TEAM = 'updateTeam',
  NEW_MESSAGE = 'newMessage',
}
