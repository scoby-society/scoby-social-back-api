export enum ActivityActionTypes {
  TEAM_STATUS_UPDATE = 'teamUpdate',
  CREATE_SESSION = 'session',
  CREATE_SERIE = 'serie',
  FOLLOW_USER = 'follow',
  TEAM_INVITE = 'teamInvite',
  CREATE_EVENT = 'event',
  TEAM_JOIN_REQUEST = 'joinRequest',
  USER_REQUEST_INVITATION = 'userRequestInvitation',
  CREATE_PROJECT = 'project',
  CREATE_CHANNEL = 'channel',
}
