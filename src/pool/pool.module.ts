import { Module } from '@nestjs/common';
import { PoolResolver } from './pool.resolver';
import { PoolService } from './pool.service';
import { S3Module } from '../lib/s3/s3.module';
import { JwtModule } from '../lib/jwt/jwt.module';
import { ImageProcessorModule } from '../lib/image-processor/image-processor.module';
@Module({
  imports: [
    JwtModule,
    ImageProcessorModule,
    S3Module,
  ],
  providers: [PoolResolver, PoolService],
  exports: [PoolService],
})
export class PoolModule {}
