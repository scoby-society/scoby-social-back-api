import { Resolver, Mutation, Args, Query } from '@nestjs/graphql';
import { PoolService } from './pool.service';
import { JwtGuard } from '../lib/jwt/jwt.guard';
import { UseGuards } from '@nestjs/common';
import { GraphQLUpload } from 'apollo-server-express';
import { FileUpload } from '../lib/common/common.interfaces';
import { PoolCreation, PoolObject } from './pool.graphql';
import { Pool } from './pool.entity';

@Resolver('Pool')
export class PoolResolver {
  constructor(private poolService: PoolService) {}

  @Mutation(() => PoolObject)
  @UseGuards(JwtGuard)
  async createPool(
    @Args('pool') pool: PoolCreation,
    @Args('cardProjectAvatar', { nullable: true, type: () => GraphQLUpload })
    cardProjectAvatar?: FileUpload,
    @Args('cardProjectHeader', { nullable: true, type: () => GraphQLUpload })
    cardProjectHeader?: FileUpload,
    @Args('projectImage', { nullable: true, type: () => GraphQLUpload })
    projectImage?: FileUpload,
    @Args('mintingImage', { nullable: true, type: () => GraphQLUpload })
    mintingImage?: FileUpload,
    @Args('avatarCreator', { nullable: true, type: () => GraphQLUpload })
    avatarCreator?: FileUpload,
    @Args('projectImageTop', { nullable: true, type: () => GraphQLUpload })
    projectImageTop?: FileUpload,
    @Args('projectImageLeft', { nullable: true, type: () => GraphQLUpload })
    projectImageLeft?: FileUpload,
    @Args('projectImageRight', { nullable: true, type: () => GraphQLUpload })
    projectImageRight?: FileUpload,
  ): Promise<PoolObject> {
    return this.poolService.createPool(
      pool,
      cardProjectAvatar,
      cardProjectHeader,
      projectImage,
      mintingImage,
      avatarCreator,
      projectImageTop,
      projectImageLeft,
      projectImageRight,
    );
  }

  @Query(() => PoolObject)
  async getPoolById(@Args('id') id: number): Promise<PoolObject> {
    return this.poolService.getPoolById(id);
  }

  @Query(() => [PoolObject])
  async getPools(): Promise<Pool[]> {
    return this.poolService.getPools();
  }

  @Mutation(() => PoolObject)
  @UseGuards(JwtGuard)
  async updatePoolById(
    @Args('id') id: number,
    @Args('pool') pool: PoolCreation,
    @Args('cardProjectAvatar', { nullable: true, type: () => GraphQLUpload })
    cardProjectAvatar?: FileUpload,
    @Args('cardProjectHeader', { nullable: true, type: () => GraphQLUpload })
    cardProjectHeader?: FileUpload,
    @Args('projectImage', { nullable: true, type: () => GraphQLUpload })
    projectImage?: FileUpload,
    @Args('mintingImage', { nullable: true, type: () => GraphQLUpload })
    mintingImage?: FileUpload,
    @Args('projectImageTop', { nullable: true, type: () => GraphQLUpload })
    projectImageTop?: FileUpload,
    @Args('projectImageLeft', { nullable: true, type: () => GraphQLUpload })
    projectImageLeft?: FileUpload,
    @Args('projectImageRight', { nullable: true, type: () => GraphQLUpload })
    projectImageRight?: FileUpload,
    @Args('avatarCreator', { nullable: true, type: () => GraphQLUpload })
    avatarCreator?: FileUpload,
  ): Promise<PoolObject> {
    return this.poolService.updatePool(
      id,
      pool,
      cardProjectAvatar,
      cardProjectHeader,
      projectImage,
      mintingImage,
      avatarCreator,
      projectImageTop,
      projectImageLeft,
      projectImageRight,
    );
  }

  @Query(() => PoolObject)
  async getPoolByTitle(@Args('title') title: string): Promise<PoolObject> {
    return this.poolService.getPoolByTitle(title);
  }

  @Mutation(() => PoolObject)
  async deletePoolById(@Args('id') id: number): Promise<PoolObject> {
    return this.poolService.deletePoolById(id);
  }
}
