import { Injectable } from '@nestjs/common';
import { ApolloError } from 'apollo-server-express';
import { S3Service } from 'src/lib/s3/s3.service';
import { getConnection, getRepository } from 'typeorm';
import { FileUpload } from '../lib/common/common.interfaces';
import { Pool } from './pool.entity';
import { PoolCreation, PoolObject } from './pool.graphql';
import { POOL_ERRORS } from './pool.messages';
import {
  ImageProcessorService,
  ImageTargetType,
} from '../lib/image-processor/image-processor.service';

@Injectable()
export class PoolService {
  constructor(
    private imageProcessorService: ImageProcessorService,
    private s3Service: S3Service,
  ) {}

  async createPool(
    poolObject: PoolCreation,
    cardProjectAvatar?: FileUpload,
    cardProjectHeader?: FileUpload,
    projectImage?: FileUpload,
    mintingImage?: FileUpload,
    avatarCreator?: FileUpload,
    projectImageTop?: FileUpload,
    projectImageLeft?: FileUpload,
    projectImageRight?: FileUpload,
  ): Promise<PoolObject> {
    const repository = getRepository(Pool);
    const {
      title,
      cardProjectDescription,
      ProjectSubtitle,
      viewWebsiteLink,
      viewCreatorProfile,
      projectDescription,
      externalLink,
      pool,
      symbol,
      programId,
      walletAdress,
      royalties,
    } = poolObject;

    const savedEvent = await repository.save({
      title,
      cardProjectDescription,
      ProjectSubtitle,
      viewWebsiteLink,
      viewCreatorProfile,
      projectDescription,
      externalLink,
      pool,
      symbol,
      programId,
      walletAdress,
      royalties,
    });

    await this.uploadFilePool(
      savedEvent.id,
      cardProjectAvatar,
      cardProjectHeader,
      projectImage,
      mintingImage,
      avatarCreator,
      projectImageTop,
      projectImageLeft,
      projectImageRight,
    );

    return (await repository.findOne(savedEvent.id)) as PoolObject;
  }

  async uploadFilePool(
    idPool: number,
    cardProjectAvatar?: FileUpload,
    cardProjectHeader?: FileUpload,
    projectImage?: FileUpload,
    mintingImage?: FileUpload,
    avatarCreator?: FileUpload,
    projectImageTop?: FileUpload,
    projectImageLeft?: FileUpload,
    projectImageRight?: FileUpload,
  ): Promise<boolean> {
    const repository = getRepository(Pool);
    const pool = (await repository.findOne(idPool)) as PoolObject;

    if (!pool) {
      throw new ApolloError(
        POOL_ERRORS.POOL_NOT_FOUND.MESSAGE,
        POOL_ERRORS.POOL_NOT_FOUND.CODE,
      );
    }

    const removeAssets = [];

    if (cardProjectAvatar && pool.cardProjectAvatar) {
      removeAssets.push(pool.cardProjectAvatar);
    }

    if (cardProjectHeader && pool.cardProjectHeader) {
      removeAssets.push(pool.cardProjectHeader);
    }

    if (projectImage && pool.projectImage) {
      removeAssets.push(pool.projectImage);
    }

    if (mintingImage && pool.mintingImage) {
      removeAssets.push(pool.mintingImage);
    }

    if (avatarCreator && pool.avatarCreator) {
      removeAssets.push(pool.avatarCreator);
    }

    if (projectImageLeft && pool.projectImageLeft) {
      removeAssets.push(pool.projectImageLeft);
    }

    if (projectImageRight && pool.projectImageRight) {
      removeAssets.push(pool.projectImageRight);
    }

    if (projectImageTop && pool.projectImageTop) {
      removeAssets.push(pool.projectImageTop);
    }

    if (removeAssets.length) {
      await this.s3Service.removeFiles(removeAssets);
    }

    let cardProjectHeaderUpload;
    let cardProjectAvatarUpload;
    let projectImageUpload;
    let mintingImageUpload;
    let avatarCreatorUpload;
    let projectImageTopUpload;
    let projectImageLeftUpload;
    let projectImageRightUpload;

    if (cardProjectAvatar) {
      cardProjectAvatarUpload = this.imageProcessorService.optimizeImage(
        cardProjectAvatar.createReadStream(),
        ImageTargetType.BACKGROUND_IMAGE,
      );
    }

    if (cardProjectHeader) {
      cardProjectHeaderUpload = this.imageProcessorService.optimizeImage(
        cardProjectHeader.createReadStream(),
        ImageTargetType.BACKGROUND_IMAGE,
      );
    }

    if (projectImage) {
      projectImageUpload = this.imageProcessorService.optimizeImage(
        projectImage.createReadStream(),
        ImageTargetType.BACKGROUND_IMAGE,
      );
    }

    if (mintingImage) {
      mintingImageUpload = this.imageProcessorService.optimizeImage(
        mintingImage.createReadStream(),
        ImageTargetType.BACKGROUND_IMAGE,
      );
    }

    if (avatarCreator) {
      avatarCreatorUpload = this.imageProcessorService.optimizeImage(
        avatarCreator.createReadStream(),
        ImageTargetType.BACKGROUND_IMAGE,
      );
    }

    if (projectImageTop) {
      projectImageTopUpload = this.imageProcessorService.optimizeImage(
        projectImageTop.createReadStream(),
        ImageTargetType.BACKGROUND_IMAGE,
      );
    }

    if (projectImageLeft) {
      projectImageLeftUpload = this.imageProcessorService.optimizeImage(
        projectImageLeft.createReadStream(),
        ImageTargetType.BACKGROUND_IMAGE,
      );
    }

    if (projectImageRight) {
      projectImageRightUpload = this.imageProcessorService.optimizeImage(
        projectImageRight.createReadStream(),
        ImageTargetType.BACKGROUND_IMAGE,
      );
    }

    const [
      cardProjectAvatarResult,
      cardProjectHeaderResult,
      projectImageResult,
      mintingImageResult,
      avatarCreatorResult,
      projectImageTopResult,
      projectImageLeftResult,
      projectImageRightResult,
    ] = await Promise.all([
      cardProjectAvatarUpload
        ? this.s3Service.uploadFile({
            extension: cardProjectAvatarUpload.extension,
            mime: cardProjectAvatarUpload.mime,
            stream: cardProjectAvatarUpload.stream,
          })
        : undefined,
      cardProjectHeaderUpload
        ? this.s3Service.uploadFile({
            extension: cardProjectHeaderUpload.extension,
            mime: cardProjectHeaderUpload.mime,
            stream: cardProjectHeaderUpload.stream,
          })
        : undefined,
      projectImageUpload
        ? this.s3Service.uploadFile({
            extension: projectImageUpload.extension,
            mime: projectImageUpload.mime,
            stream: projectImageUpload.stream,
          })
        : undefined,
      mintingImageUpload
        ? this.s3Service.uploadFile({
            extension: mintingImageUpload.extension,
            mime: mintingImageUpload.mime,
            stream: mintingImageUpload.stream,
          })
        : undefined,
      avatarCreatorUpload
        ? this.s3Service.uploadFile({
            extension: avatarCreatorUpload.extension,
            mime: avatarCreatorUpload.mime,
            stream: avatarCreatorUpload.stream,
          })
        : undefined,
      projectImageTopUpload
        ? this.s3Service.uploadFile({
            extension: projectImageTopUpload.extension,
            mime: projectImageTopUpload.mime,
            stream: projectImageTopUpload.stream,
          })
        : undefined,
      projectImageLeftUpload
        ? this.s3Service.uploadFile({
            extension: projectImageLeftUpload.extension,
            mime: projectImageLeftUpload.mime,
            stream: projectImageLeftUpload.stream,
          })
        : undefined,
      projectImageRightUpload
        ? this.s3Service.uploadFile({
            extension: projectImageRightUpload.extension,
            mime: projectImageRightUpload.mime,
            stream: projectImageRightUpload.stream,
          })
        : undefined,
    ]);

    await repository.save({
      id: pool.id,
      cardProjectAvatar: cardProjectAvatarResult?.Key,
      cardProjectHeader: cardProjectHeaderResult?.Key,
      projectImage: projectImageResult?.Key,
      mintingImage: mintingImageResult?.Key,
      avatarCreator: avatarCreatorResult?.Key,
      projectImageTop: projectImageTopResult?.Key,
      projectImageLeft: projectImageLeftResult?.Key,
      projectImageRight: projectImageRightResult?.Key,
    });

    return true;
  }

  async getPoolById(id: number): Promise<PoolObject> {
    const repository = getRepository(Pool);
    const pool = (await repository.findOne(id)) as PoolObject;

    if (!pool) {
      throw new ApolloError(
        POOL_ERRORS.POOL_NOT_FOUND.MESSAGE,
        POOL_ERRORS.POOL_NOT_FOUND.CODE,
      );
    }
    return pool;
  }

  async getPools(): Promise<Pool[]> {
    return await getRepository(Pool).createQueryBuilder('pool').getMany();
  }

  async updatePool(
    id: number,
    poolObject: PoolCreation,
    cardProjectAvatar?: FileUpload,
    cardProjectHeader?: FileUpload,
    projectImage?: FileUpload,
    mintingImage?: FileUpload,
    avatarCreator?: FileUpload,
    projectImageTop?: FileUpload,
    projectImageLeft?: FileUpload,
    projectImageRight?: FileUpload,
  ): Promise<PoolObject> {
    const repository = getRepository(Pool);

    const {
      title,
      cardProjectDescription,
      ProjectSubtitle,
      viewWebsiteLink,
      viewCreatorProfile,
      projectDescription,
      externalLink,
      pool,
      symbol,
      programId,
      walletAdress,
    } = poolObject;

    const poolRepository = await repository.findOne(id);

    if (!poolRepository) {
      throw new ApolloError(
        POOL_ERRORS.POOL_NOT_FOUND.MESSAGE,
        POOL_ERRORS.POOL_NOT_FOUND.CODE,
      );
    }

    const savedEvent = await repository.save({
      id: poolRepository.id,
      title,
      cardProjectDescription,
      ProjectSubtitle,
      viewWebsiteLink,
      viewCreatorProfile,
      projectDescription,
      externalLink,
      pool,
      symbol,
      programId,
      walletAdress,
    });

    await this.uploadFilePool(
      savedEvent.id,
      cardProjectAvatar,
      cardProjectHeader,
      projectImage,
      mintingImage,
      avatarCreator,
      projectImageTop,
      projectImageLeft,
      projectImageRight,
    );

    return (await repository.findOne(savedEvent.id)) as PoolObject;
  }

  async getPoolByTitle(title: string): Promise<PoolObject> {
    const pool = await getRepository(Pool)
      .createQueryBuilder('pool')
      .where('pool.title=:title', { title })
      .getOne();

    if (!pool) {
      throw new ApolloError(
        POOL_ERRORS.POOL_NOT_FOUND.MESSAGE,
        POOL_ERRORS.POOL_NOT_FOUND.CODE,
      );
    }

    return pool as PoolObject;
  }

  async deletePoolById(id: number): Promise<PoolObject> {
    const repository = getRepository(Pool);
    const pool = (await repository.findOne(id)) as PoolObject;
    await getConnection()
      .createQueryBuilder()
      .delete()
      .from(Pool)
      .where('id = :id', { id })
      .execute();

    return pool;
  }
}
