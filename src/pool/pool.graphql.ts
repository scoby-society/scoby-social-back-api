import { ObjectType, Int, Field, InputType } from '@nestjs/graphql';

@ObjectType()
export class PoolObject {
  @Field(() => Int)
  id: number;

  @Field(() => String)
  title: string;

  @Field(() => String)
  cardProjectDescription: string;

  @Field(() => String)
  ProjectSubtitle: string;

  @Field(() => String)
  viewWebsiteLink: string;

  @Field(() => String)
  viewCreatorProfile: string;

  @Field(() => String)
  projectDescription: string;

  @Field(() => String)
  externalLink: string;

  @Field(() => String)
  pool: string;

  @Field(() => String)
  programId: string;

  @Field(() => String)
  symbol: string;

  @Field(() => String, { nullable: true })
  walletAdress?: string | null;

  @Field(() => String, { nullable: true })
  cardProjectAvatar?: string | null;

  @Field(() => String, { nullable: true })
  cardProjectHeader?: string | null;

  @Field(() => String, { nullable: true })
  projectImage?: string | null;

  @Field(() => String, { nullable: true })
  mintingImage?: string | null;

  @Field(() => String, { nullable: true })
  avatarCreator?: string | null;

  @Field(() => String, { nullable: true })
  projectImageLeft?: string | null;

  @Field(() => String, { nullable: true })
  projectImageRight?: string | null;

  @Field(() => String, { nullable: true })
  projectImageTop?: string | null;
}

@InputType()
export class PoolCreation {
  @Field(() => String)
  title: string;

  @Field(() => String)
  cardProjectDescription: string;

  @Field(() => String)
  ProjectSubtitle: string;

  @Field(() => String)
  viewWebsiteLink: string;

  @Field(() => String)
  viewCreatorProfile: string;

  @Field(() => String)
  projectDescription: string;

  @Field(() => String)
  externalLink: string;

  @Field(() => String)
  pool: string;

  @Field(() => String)
  programId: string;

  @Field(() => String)
  symbol: string;

  @Field(() => String)
  walletAdress: string;

  @Field(() => String, { nullable: true })
  royalties?: string | null;
}
