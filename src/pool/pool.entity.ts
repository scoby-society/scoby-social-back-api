import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

function ImageLinkTransformerFrom(value: string | null): string | null {
  if (value == null) return value;
  const bucketName = process.env.AWS_S3_USER_PROFILE_ASSETS_BUCKET;
  const baseUrl = process.env.AWS_S3_BASE_URL;
  return `https://${bucketName}.${baseUrl}/${value}`;
}

function ImageLinkTransformerTo(value: string | null): string | null {
  if (value == null) return value;

  try {
    const url = new URL(value);
    const pathSplitted = url.pathname.split('/');
    return pathSplitted[pathSplitted.length - 1];
  } catch (e) {
    return value;
  }
}
@Entity()
export class Pool {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column({ type: 'varchar', name: 'card_project_description' })
  cardProjectDescription: string;

  @Column({ type: 'varchar', nullable: true, name: 'Project_subtitle' })
  ProjectSubtitle?: string | null;

  @Column({ type: 'varchar', name: 'view_website_link' })
  viewWebsiteLink: string;

  @Column({ type: 'varchar', name: 'view_creator_profile' })
  viewCreatorProfile: string;

  @Column({ type: 'varchar', name: 'project_description' })
  projectDescription: string;

  @Column({ type: 'varchar', name: 'external_link' })
  externalLink: string;

  @Column()
  pool: string;

  @Column()
  symbol: string;

  @Column({ type: 'varchar', name: 'program_id' })
  programId: string;

  @Column({ type: 'varchar', nullable: true, name: 'wallet_adress' })
  walletAdress?: string;

  @Column({
    type: 'varchar',
    nullable: true,
    name: 'card_project_avatar',
    transformer: {
      from: ImageLinkTransformerFrom,
      to: ImageLinkTransformerTo,
    },
  })
  cardProjectAvatar?: string | null;

  @Column({
    type: 'varchar',
    nullable: true,
    name: 'card_project_header',
    transformer: {
      from: ImageLinkTransformerFrom,
      to: ImageLinkTransformerTo,
    },
  })
  cardProjectHeader?: string | null;

  @Column({
    type: 'varchar',
    nullable: true,
    name: 'project_image',
    transformer: {
      from: ImageLinkTransformerFrom,
      to: ImageLinkTransformerTo,
    },
  })
  projectImage?: string | null;

  @Column({
    type: 'varchar',
    nullable: true,
    name: 'minting_image',
    transformer: {
      from: ImageLinkTransformerFrom,
      to: ImageLinkTransformerTo,
    },
  })
  mintingImage?: string | null;

  @Column({
    type: 'varchar',
    nullable: true,
    name: 'avatar_creator',
    transformer: {
      from: ImageLinkTransformerFrom,
      to: ImageLinkTransformerTo,
    },
  })
  avatarCreator?: string | null;

  @Column({
    type: 'varchar',
    nullable: true,
    name: 'project_image_left',
    transformer: {
      from: ImageLinkTransformerFrom,
      to: ImageLinkTransformerTo,
    },
  })
  projectImageLeft?: string | null;

  @Column({
    type: 'varchar',
    nullable: true,
    name: 'project_image_right',
    transformer: {
      from: ImageLinkTransformerFrom,
      to: ImageLinkTransformerTo,
    },
  })
  projectImageRight?: string | null;

  @Column({
    type: 'varchar',
    nullable: true,
    name: 'project_image_top',
    transformer: {
      from: ImageLinkTransformerFrom,
      to: ImageLinkTransformerTo,
    },
  })
  projectImageTop?: string | null;

  @Column({ type: 'varchar', nullable: true, name: 'royalties' })
  royalties?: string | null;
}
