export const POOL_ERRORS = {
  POOL_NOT_FOUND: {
    CODE: 'ERR_POOL_NOT_FOUND',
    MESSAGE: 'Pool not found',
  }
};
