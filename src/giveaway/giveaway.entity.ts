import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn,
  OneToOne,
  ManyToMany,
  JoinTable,
} from 'typeorm';
import { User } from '../users/user.entity';
import { Event } from '../events/events.entity';
import { GiveAwayType } from './giveaway.type';
import { Team } from '../team/team.entity';

@Entity()
export class GiveAway {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  description: string;

  @Column('int', { name: 'value', nullable: true })
  value?: number | null;

  @ManyToOne(() => User, { nullable: false })
  @JoinColumn({ name: 'owner_user_id' })
  ownerUser: User;

  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updated_at' })
  updatedAt: Date;

  @Column({ type: 'timestamp', nullable: true, name: 'finished_at' })
  finishedAt?: Date | null;

  @Column({
    type: 'enum',
    enum: GiveAwayType,
    default: GiveAwayType.NFT,
    name: 'type',
  })
  giveawayType: GiveAwayType;

  @ManyToMany(() => User, { nullable: true, cascade: true })
  @JoinTable({
    name: 'giveaway_subscribed_users',
    joinColumn: { name: 'giveaway_id' },
    inverseJoinColumn: { name: 'user_id' },
  })
  suscribeUsers: User[];

  @OneToOne(() => Event, { cascade: true, nullable: true })
  @JoinColumn()
  event?: Event | null;

  @ManyToMany(() => Team, { nullable: true, cascade: true })
  @JoinTable({
    name: 'community_giveaway`',
    joinColumn: { name: 'community_id' },
  })
  community: Team[];

  @Column('int', { array: true, name: 'nft_id', nullable: true })
  nfts?: number[] | null;

  @Column('int', { array: true, name: 'collections_id', nullable: true })
  collections?: number[] | null;

  @ManyToOne(() => User, { nullable: true })
  @JoinColumn({ name: 'winner_user_id' })
  userWinner?: User | null;

  @Column({
    type: 'boolean',
    name: 'is_all_community_required',
    default: false,
  })
  isAllCommunityRequired: boolean;

  @Column({
    type: 'boolean',
    name: 'is_all_collection_required',
    default: false,
  })
  isAllCollectionRequired: boolean;
}
