import { Module } from '@nestjs/common';
import { GiveAwayResolver } from './giveaway.resolver';
import { GiveAwayService } from './giveaway.service';
import { JwtModule } from '../lib/jwt/jwt.module';
import { S3Module } from '../lib/s3/s3.module';
import { EventsModule } from 'src/events/events.module';

@Module({
  imports: [JwtModule, S3Module, EventsModule],
  providers: [GiveAwayService, GiveAwayResolver],
  exports: [GiveAwayService],
})
export class GiveAwayModule {}
