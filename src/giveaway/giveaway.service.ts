import { Injectable } from '@nestjs/common';
import { ApolloError } from 'apollo-server-express';
import { Event } from 'src/events/events.entity';
import { Pool } from 'src/pool/pool.entity';
import { Session } from 'src/sessions/session.entity';
import { Team } from 'src/team/team.entity';
import { User } from 'src/users/user.entity';
import { USERS_ERRORS } from 'src/users/users.messages';
import { getRepository } from 'typeorm';
import { GiveAway } from './giveaway.entity';
import {
  EventAndGiveAway,
  GiveAwayCreation,
  GiveAwayObject,
  GiveAwayRequirementsList,
  GiveAwayUpdate,
} from './giveaway.graphql';
import { GIVEAWAYS_ERRORS } from './giveaway.messages';
import { GiveAwayType } from './giveaway.type';
import { EVENT_ERRORS } from 'src/events/event.messages';
import { PoolObject } from 'src/pool/pool.graphql';
@Injectable()
export class GiveAwayService {
  async createGiveAway(
    currentUserId: number,
    giveawayObject: GiveAwayCreation,
  ): Promise<GiveAwayObject> {
    const repository = getRepository(GiveAway);
    const repositoryEvent = getRepository(Event);
    const {
      name,
      description,
      communitysId,
      collectionsId,
      giveawayType,
      nftsIds,
      eventId,
      value,
      isAllCollectionRequired,
      isAllCommunityRequired,
    } = giveawayObject;

    const teams = await this.getTeams(communitysId);
    const event = await repositoryEvent.findOne(eventId);

    if (!event) {
      throw new ApolloError(
        EVENT_ERRORS.EVENT_NOT_FOUND.MESSAGE,
        EVENT_ERRORS.EVENT_NOT_FOUND.CODE,
      );
    }

    if (event.finishedAt) {
      throw new ApolloError(
        EVENT_ERRORS.EVENT_FINISHED.MESSAGE,
        EVENT_ERRORS.EVENT_FINISHED.CODE,
      );
    }

    const saved = await repository.save({
      name,
      description,
      collections: collectionsId,
      community: teams,
      giveawayType: this.getType(giveawayType),
      ownerUser: {
        id: currentUserId,
      },
      nfts: nftsIds,
      event: event,
      value,
      isAllCollectionRequired,
      isAllCommunityRequired,
    });

    await repositoryEvent.save({
      id: eventId,
      giveAway: saved,
    });

    return saved as GiveAwayObject;
  }

  async editGiveAway(
    currentUserId: number,
    giveAwayId: number,
    giveaway: GiveAwayUpdate,
  ): Promise<GiveAwayObject> {
    const repository = getRepository(GiveAway);
    const repositoryEvent = getRepository(Event);
    const giveAwayRepository = await repository.findOne(giveAwayId, {
      relations: ['ownerUser', 'event'],
    });

    if (!giveAwayRepository) {
      throw new ApolloError(
        GIVEAWAYS_ERRORS.GIVEAWAYS_NOT_FOUND.MESSAGE,
        GIVEAWAYS_ERRORS.GIVEAWAYS_NOT_FOUND.CODE,
      );
    }

    if (giveAwayRepository.ownerUser.id !== currentUserId) {
      throw new ApolloError(
        GIVEAWAYS_ERRORS.NOT_GIVEAWAYS_OWNER.MESSAGE,
        GIVEAWAYS_ERRORS.NOT_GIVEAWAYS_OWNER.CODE,
      );
    }

    if (giveAwayRepository.finishedAt) {
      throw new ApolloError(
        GIVEAWAYS_ERRORS.GIVEAWAYS_FINISHED.MESSAGE,
        GIVEAWAYS_ERRORS.GIVEAWAYS_FINISHED.CODE,
      );
    }

    const teams = giveaway.communitysId
      ? await this.getTeams(giveaway.communitysId)
      : giveAwayRepository.community;

    const event = giveaway.eventId
      ? await getRepository(Event).findOne(giveaway.eventId)
      : giveAwayRepository.event;

    if (!event) {
      throw new ApolloError(
        EVENT_ERRORS.EVENT_NOT_FOUND.MESSAGE,
        EVENT_ERRORS.EVENT_NOT_FOUND.CODE,
      );
    }

    if (event.finishedAt) {
      throw new ApolloError(
        EVENT_ERRORS.EVENT_FINISHED.MESSAGE,
        EVENT_ERRORS.EVENT_FINISHED.CODE,
      );
    }

    if (!giveAwayRepository.event) {
      throw new ApolloError(
        EVENT_ERRORS.EVENT_NOT_FOUND.MESSAGE,
        EVENT_ERRORS.EVENT_NOT_FOUND.CODE,
      );
    }

    if (giveAwayRepository.event.id) {
      await repositoryEvent.save({
        id: giveAwayRepository.event.id,
        giveAway: null,
      });
    }
    if (giveaway.eventId) {
      await repositoryEvent.save({
        id: giveaway.eventId,
        giveAway: { id: giveAwayId },
      });
    }

    await repository.save({
      id: giveAwayId,
      name: giveaway.name,
      description: giveaway.description,
      nfts: giveaway.nftsIds,
      giveawayType: this.getType(
        giveaway.giveawayType || giveAwayRepository.giveawayType,
      ),
      collections: giveaway.collectionsId,
      value: giveaway.value,
      event: event,
      community: teams,
      isAllCollectionRequired: giveaway.isAllCollectionRequired,
      isAllCommunityRequired: giveaway.isAllCommunityRequired,
    });

    return (await repository.findOne(giveAwayId, {
      relations: ['ownerUser', 'event'],
    })) as GiveAwayObject;
  }

  async getGiveAwayById(id: number): Promise<GiveAwayObject> {
    const repository = getRepository(GiveAway);
    const giveAway = (await repository.findOne(id, {
      relations: [
        'ownerUser',
        'community',
        'event',
        'suscribeUsers',
        'userWinner',
      ],
    })) as GiveAway;

    if (!giveAway) {
      throw new ApolloError(
        GIVEAWAYS_ERRORS.GIVEAWAYS_NOT_FOUND.MESSAGE,
        GIVEAWAYS_ERRORS.GIVEAWAYS_NOT_FOUND.CODE,
      );
    }
    return giveAway;
  }

  async endGiveAway(
    currentUserId: number,
    giveAwayId: number,
  ): Promise<GiveAwayObject> {
    const repository = getRepository(GiveAway);
    const giveawayRepository = await repository.findOne(giveAwayId, {
      relations: ['ownerUser', 'event'],
    });

    if (!giveawayRepository) {
      throw new ApolloError(
        GIVEAWAYS_ERRORS.GIVEAWAYS_NOT_FOUND.MESSAGE,
        GIVEAWAYS_ERRORS.GIVEAWAYS_NOT_FOUND.CODE,
      );
    }

    if (giveawayRepository.ownerUser.id !== currentUserId) {
      throw new ApolloError(
        GIVEAWAYS_ERRORS.NOT_GIVEAWAYS_OWNER.MESSAGE,
        GIVEAWAYS_ERRORS.NOT_GIVEAWAYS_OWNER.CODE,
      );
    }

    if (giveawayRepository.finishedAt) {
      throw new ApolloError(
        GIVEAWAYS_ERRORS.GIVEAWAYS_FINISHED.MESSAGE,
        GIVEAWAYS_ERRORS.GIVEAWAYS_FINISHED.CODE,
      );
    }

    const finishedAt = new Date(Date.now());
    await repository.save({ id: giveAwayId, finishedAt, event: null });
    const giveAwaySaved = await repository.findOne(giveAwayId, {
      relations: ['ownerUser', 'community', 'event', 'suscribeUsers'],
    });

    if (!giveAwaySaved) {
      throw new ApolloError(
        GIVEAWAYS_ERRORS.GIVEAWAYS_NOT_FOUND.MESSAGE,
        GIVEAWAYS_ERRORS.GIVEAWAYS_NOT_FOUND.CODE,
      );
    }

    return giveAwaySaved;
  }

  async getTeams(idsTeams: number[]): Promise<Team[]> {
    const team = await getRepository(Team)
      .createQueryBuilder('team')
      .where('team.id IN (:...ids)', {
        ids: idsTeams.length > 0 ? idsTeams : [undefined],
      })
      .getMany();
    return team;
  }

  getType(type: string): GiveAwayType {
    this.verifyTypeExperience(type);
    if (type === 'sol') {
      return GiveAwayType.SOL;
    } else {
      return GiveAwayType.NFT;
    }
  }

  verifyTypeExperience(type: string): void {
    if (type !== 'sol' && type !== 'nft') {
      throw new ApolloError(
        GIVEAWAYS_ERRORS.TYPE_NOT_FOUND.MESSAGE,
        GIVEAWAYS_ERRORS.TYPE_NOT_FOUND.CODE,
      );
    }
  }

  async joinGiveAway(
    currentUserId: number,
    giveAwayId: number,
  ): Promise<GiveAwayObject> {
    const repository = getRepository(GiveAway);
    const userRepository = getRepository(User);
    const giveAway = await repository.findOne(giveAwayId, {
      relations: ['ownerUser', 'suscribeUsers'],
    });
    const userToAdd = await userRepository.findOne(currentUserId);

    if (!giveAway) {
      throw new ApolloError(
        GIVEAWAYS_ERRORS.GIVEAWAYS_NOT_FOUND.MESSAGE,
        GIVEAWAYS_ERRORS.GIVEAWAYS_NOT_FOUND.CODE,
      );
    }

    if (!userToAdd) {
      throw new ApolloError(
        USERS_ERRORS.USER_NOT_FOUND.MESSAGE,
        USERS_ERRORS.USER_NOT_FOUND.CODE,
      );
    }

    if (giveAway.finishedAt) {
      throw new ApolloError(
        GIVEAWAYS_ERRORS.GIVEAWAYS_FINISHED.MESSAGE,
        GIVEAWAYS_ERRORS.GIVEAWAYS_FINISHED.CODE,
      );
    }

    const suscribeUsers = giveAway.suscribeUsers || [];
    suscribeUsers.push(userToAdd as User);

    await repository.save({ id: giveAwayId, suscribeUsers });
    const savedGiveAway = (await repository.findOne(giveAwayId, {
      relations: ['ownerUser', 'community', 'event', 'suscribeUsers'],
    })) as GiveAwayObject;

    return savedGiveAway;
  }

  async getRequirementsListGiveAway(
    giveAwayId: number,
  ): Promise<GiveAwayRequirementsList> {
    const repository = getRepository(GiveAway);
    const repositorySession = getRepository(Session);
    const giveAway = await repository.findOne(giveAwayId, {
      relations: ['ownerUser', 'event', 'suscribeUsers', 'community'],
    });

    if (!giveAway) {
      throw new ApolloError(
        GIVEAWAYS_ERRORS.GIVEAWAYS_NOT_FOUND.MESSAGE,
        GIVEAWAYS_ERRORS.GIVEAWAYS_NOT_FOUND.CODE,
      );
    }

    if (!giveAway.event) {
      throw new ApolloError(
        EVENT_ERRORS.EVENT_NOT_FOUND.MESSAGE,
        EVENT_ERRORS.EVENT_NOT_FOUND.CODE,
      );
    }

    const event = await getRepository(Event)
      .createQueryBuilder('event')
      .leftJoinAndSelect('event.session', 'session')
      .where('event.id = :idEvent', {
        idEvent: giveAway.event.id,
      })
      .getOne();

    const idTeam = giveAway.community.map((team) => team.id);

    const team = await getRepository(Team)
      .createQueryBuilder('team')
      .leftJoinAndSelect('team.participantUsers', 'participantUsers')
      .leftJoinAndSelect('team.ownerUser', 'ownerUser')
      .leftJoinAndSelect('team.members', 'members')
      .where('team.id IN (:...ids)', {
        ids: idTeam.length > 0 ? idTeam : [undefined],
      })
      .getMany();

    const pools = await getRepository(Pool)
      .createQueryBuilder('pool')
      .where('pool.id IN (:...ids)', {
        ids: giveAway.collections ? giveAway.collections : [undefined],
      })
      .getMany();

    let session;
    if (event?.session && event.session.finishedAt === null) {
      session = (await repositorySession.findOne(event.session.id, {
        relations: ['viewerUsers', 'greenRoomUsers', 'topics'],
      })) as Session;
    }
    return {
      community: team,
      event: giveAway.event,
      giveawayType: giveAway.giveawayType,
      session: session as any,
      suscribeUsers: giveAway.suscribeUsers,
      collections: pools as any,
    };
  }

  async userWinnerGiveAway(
    userId: number,
    giveAwayId: number,
  ): Promise<GiveAwayObject> {
    const repository = getRepository(GiveAway);
    const userRepository = getRepository(User);
    const giveAway = await repository.findOne(giveAwayId, {
      relations: ['ownerUser'],
    });
    const userToAdd = await userRepository.findOne(userId);

    if (!giveAway) {
      throw new ApolloError(
        GIVEAWAYS_ERRORS.GIVEAWAYS_NOT_FOUND.MESSAGE,
        GIVEAWAYS_ERRORS.GIVEAWAYS_NOT_FOUND.CODE,
      );
    }

    if (!userToAdd) {
      throw new ApolloError(
        USERS_ERRORS.USER_NOT_FOUND.MESSAGE,
        USERS_ERRORS.USER_NOT_FOUND.CODE,
      );
    }

    await repository.save({ id: giveAwayId, userWinner: userToAdd });
    const savedGiveAway = (await repository.findOne(giveAwayId, {
      relations: [
        'ownerUser',
        'community',
        'event',
        'suscribeUsers',
        'userWinner',
      ],
    })) as GiveAwayObject;

    return savedGiveAway;
  }

  async getEventAndGiveAwayById(id: number): Promise<EventAndGiveAway> {
    const repository = getRepository(GiveAway);
    const repositoryEvent = getRepository(Event);

    const event = (await repositoryEvent.findOne(id, {
      relations: ['giveAway', 'session', 'nft', 'suscribeUsers', 'topics'],
    })) as Event;

    const giveAway = (await repository.findOne(event?.giveAway?.id, {
      relations: [
        'ownerUser',
        'community',
        'event',
        'suscribeUsers',
        'userWinner',
      ],
    })) as GiveAway;

    const idCollection: number[] = giveAway.collections || [0];

    const pools = (await getRepository(Pool)
      .createQueryBuilder('pool')
      .where('pool.id IN (:...ids)', {
        ids: idCollection.length > 0 ? idCollection : [undefined],
      })
      .getMany()) as PoolObject[];

    const idTeam = giveAway.community.map((team) => team.id);

    const team = await getRepository(Team)
      .createQueryBuilder('team')
      .leftJoinAndSelect('team.participantUsers', 'participantUsers')
      .leftJoinAndSelect('team.ownerUser', 'ownerUser')
      .leftJoinAndSelect('team.members', 'members')
      .leftJoinAndSelect('team.topics', 'topics')
      .where('team.id IN (:...ids)', {
        ids: idTeam.length > 0 ? idTeam : [undefined],
      })
      .getMany();

    if (event === null || event === undefined) {
      return { giveAway: null, event: null };
    }
    if (event.giveAway === null) {
      return { giveAway: null, event };
    }
    return {
      giveAway: { ...giveAway, collections: pools, community: team },
      event,
    };
  }

  async leaveGiveAway(
    currentUserId: number,
    giveAwayId: number,
  ): Promise<GiveAwayObject> {
    const repository = getRepository(GiveAway);
    const giveAway = await repository.findOne(giveAwayId, {
      relations: ['ownerUser', 'suscribeUsers'],
    });
    const userRepository = getRepository(User);
    const userToLeave = await userRepository.findOne(currentUserId);
    if (!giveAway) {
      throw new ApolloError(
        GIVEAWAYS_ERRORS.GIVEAWAYS_NOT_FOUND.MESSAGE,
        GIVEAWAYS_ERRORS.GIVEAWAYS_NOT_FOUND.CODE,
      );
    }
    if (!userToLeave) {
      throw new ApolloError(
        USERS_ERRORS.USER_NOT_FOUND.MESSAGE,
        USERS_ERRORS.USER_NOT_FOUND.CODE,
      );
    }

    const suscribeUsers = giveAway.suscribeUsers.filter(
      (user) => user.id !== currentUserId,
    );
    await repository.save({ id: giveAwayId, suscribeUsers });

    return (await repository.findOne(giveAwayId)) as GiveAwayObject;
  }
}
