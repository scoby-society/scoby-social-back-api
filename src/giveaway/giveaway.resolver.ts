import { Mutation, Resolver, Args, Query } from '@nestjs/graphql';
import {
  EventAndGiveAway,
  GiveAwayCreation,
  GiveAwayObject,
  GiveAwayRequirementsList,
  GiveAwayUpdate,
} from './giveaway.graphql';
import { GiveAwayService } from './giveaway.service';
import { JwtGuard } from '../lib/jwt/jwt.guard';
import { CurrentUser } from '../lib/common/current-user.decorator';
import { BasePayload } from '../lib/jwt/jwt.service';
import { UseGuards } from '@nestjs/common';

@Resolver(() => 'GiveAwayResolver')
export class GiveAwayResolver {
  constructor(private giveAwayService: GiveAwayService) {}

  @Mutation(() => GiveAwayObject)
  @UseGuards(JwtGuard)
  async createGiveAway(
    @CurrentUser() currentUser: BasePayload,
    @Args('giveaway') giveaway: GiveAwayCreation,
  ): Promise<GiveAwayObject> {
    return this.giveAwayService.createGiveAway(currentUser.id, giveaway);
  }

  @Mutation(() => GiveAwayObject)
  @UseGuards(JwtGuard)
  async editGiveAway(
    @CurrentUser() currentUser: BasePayload,
    @Args('giveaway') giveaway: GiveAwayUpdate,
    @Args('id') giveAwayId: number,
  ): Promise<GiveAwayObject> {
    return this.giveAwayService.editGiveAway(
      currentUser.id,
      giveAwayId,
      giveaway,
    );
  }

  @Mutation(() => GiveAwayObject)
  @UseGuards(JwtGuard)
  async deleteGiveAway(
    @CurrentUser() currentUser: BasePayload,
    @Args('id') giveAwayId: number,
  ): Promise<GiveAwayObject> {
    return this.giveAwayService.endGiveAway(currentUser.id, giveAwayId);
  }

  @Mutation(() => GiveAwayObject)
  @UseGuards(JwtGuard)
  async joinGiveAway(
    @CurrentUser() currentUser: BasePayload,
    @Args('id') giveAwayId: number,
  ): Promise<GiveAwayObject> {
    return this.giveAwayService.joinGiveAway(currentUser.id, giveAwayId);
  }

  @Query(() => GiveAwayObject)
  @UseGuards(JwtGuard)
  async getGiveAwayById(
    @Args('id') giveAwayId: number,
  ): Promise<GiveAwayObject> {
    return this.giveAwayService.getGiveAwayById(giveAwayId);
  }

  @Query(() => GiveAwayRequirementsList)
  @UseGuards(JwtGuard)
  async getRequirementsListGiveAway(
    @Args('id') giveAwayId: number,
  ): Promise<GiveAwayRequirementsList> {
    return this.giveAwayService.getRequirementsListGiveAway(giveAwayId);
  }

  @Mutation(() => GiveAwayObject)
  @UseGuards(JwtGuard)
  async userWinnerGiveAway(
    @Args('userId') userid: number,
    @Args('giveAwayId') giveAwayId: number,
  ): Promise<GiveAwayObject> {
    return this.giveAwayService.userWinnerGiveAway(userid, giveAwayId);
  }

  @Query(() => EventAndGiveAway)
  @UseGuards(JwtGuard)
  async getEventAndGiveAway(
    @Args('idEvent') giveAwayId: number,
  ): Promise<EventAndGiveAway> {
    return this.giveAwayService.getEventAndGiveAwayById(giveAwayId);
  }

  @Mutation(() => GiveAwayObject)
  @UseGuards(JwtGuard)
  async leaveGiveAway(
    @CurrentUser() CurrentUser: BasePayload,
    @Args('id') serieId: number,
  ): Promise<GiveAwayObject> {
    return this.giveAwayService.leaveGiveAway(CurrentUser.id, serieId);
  }
}
