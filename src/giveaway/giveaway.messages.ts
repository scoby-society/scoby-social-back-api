export const GIVEAWAYS_ERRORS = {
  GIVEAWAYS_NOT_FOUND: {
    CODE: 'ERR_GIVEAWAYS_NOT_FOUND',
    MESSAGE: 'Giveaway not found',
  },
  GIVEAWAYS_FINISHED: {
    CODE: 'ERR_GIVEAWAYS_FINISHED',
    MESSAGE: 'Giveaway has been finished',
  },
  NOT_GIVEAWAYS_OWNER: {
    CODE: 'ERR_NOT_GIVEAWAYS_OWNER',
    MESSAGE: 'User is not owner of the giveaway',
  },
  TYPE_NOT_FOUND:{
    CODE: 'ERR_TYPE_NOT_FOUND',
    MESSAGE: 'Type not found',
  }
};
