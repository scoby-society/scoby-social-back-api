import { ObjectType, Int, Field, InputType } from '@nestjs/graphql';
import { UserProfile } from '../users/users.graphql';
import { EventsObject } from 'src/events/events.graphql';
import { TeamObject } from 'src/team/team.graphql';
import { SessionObject } from 'src/sessions/sessions.graphql';
import { PoolObject } from 'src/pool/pool.graphql';
@ObjectType()
export class GiveAwayObject {
  @Field(() => Int)
  id: number;

  @Field(() => String)
  name: string;

  @Field(() => String)
  description: string;

  @Field()
  ownerUser: UserProfile;

  @Field()
  createdAt: Date;

  @Field()
  updatedAt: Date;

  @Field(() => Date, { nullable: true })
  finishedAt?: Date | null;

  @Field(() => EventsObject, { nullable: true })
  event?: EventsObject | null;

  @Field(() => String)
  giveawayType: string;

  @Field(() => [TeamObject])
  community: TeamObject[];

  @Field(() => [Number], { nullable: true })
  collections?: number[] | null;

  @Field(() => [Number], { nullable: true })
  nfts?: number[] | null;

  @Field(() => [UserProfile])
  suscribeUsers: UserProfile[];

  @Field(() => UserProfile, { nullable: true })
  userWinner?: UserProfile | null;

  @Field(() => Boolean)
  isAllCommunityRequired: boolean;

  @Field(() => Boolean)
  isAllCollectionRequired: boolean;
}

@InputType()
export class GiveAwayCreation {
  @Field(() => String)
  name: string;

  @Field(() => String)
  description: string;

  @Field(() => [Int], { nullable: true })
  communitysId: [number];

  @Field(() => [Int], { nullable: true })
  collectionsId: [number];

  @Field(() => String)
  giveawayType: string;

  @Field(() => [Int], { nullable: true })
  nftsIds?: number[] | null;

  @Field(() => Int)
  eventId: number;

  @Field(() => Int, { nullable: true })
  value?: number;

  @Field(() => Boolean)
  isAllCommunityRequired: boolean;

  @Field(() => Boolean)
  isAllCollectionRequired: boolean;
}

@InputType()
export class GiveAwayUpdate {
  @Field(() => String, { nullable: true })
  name?: string;

  @Field(() => String, { nullable: true })
  description?: string;

  @Field(() => [Int], { nullable: true })
  communitysId?: [number];

  @Field(() => [Int], { nullable: true })
  collectionsId?: [number];

  @Field(() => String, { nullable: true })
  giveawayType?: string;

  @Field(() => [Int], { nullable: true })
  nftsIds?: number[] | null;

  @Field(() => Int, { nullable: true })
  eventId?: number;

  @Field(() => Int, { nullable: true })
  value?: number;

  @Field(() => Boolean, { nullable: true })
  isAllCommunityRequired?: boolean;

  @Field(() => Boolean, { nullable: true })
  isAllCollectionRequired?: boolean;
}

@ObjectType()
export class GiveAwayRequirementsList {
  @Field(() => EventsObject)
  event: EventsObject;

  @Field(() => String)
  giveawayType: string;

  @Field(() => [TeamObject])
  community: TeamObject[];

  @Field(() => [PoolObject], { nullable: true })
  collections?: PoolObject[] | null;

  @Field(() => [UserProfile])
  suscribeUsers: UserProfile[];

  @Field(() => SessionObject, { nullable: true })
  session?: SessionObject | null;
}

@ObjectType()
export class GiveAwayPoolObject {
  @Field(() => Int)
  id: number;

  @Field(() => String)
  name: string;

  @Field(() => String)
  description: string;

  @Field()
  ownerUser: UserProfile;

  @Field()
  createdAt: Date;

  @Field()
  updatedAt: Date;

  @Field(() => Date, { nullable: true })
  finishedAt?: Date | null;

  @Field(() => EventsObject, { nullable: true })
  event?: EventsObject | null;

  @Field(() => String)
  giveawayType: string;

  @Field(() => [TeamObject])
  community: TeamObject[];

  @Field(() => [PoolObject], { nullable: true })
  collections?: PoolObject[] | null;

  @Field(() => [Number], { nullable: true })
  nfts?: number[] | null;

  @Field(() => [UserProfile])
  suscribeUsers: UserProfile[];

  @Field(() => UserProfile, { nullable: true })
  userWinner?: UserProfile | null;

  @Field(() => Boolean)
  isAllCommunityRequired: boolean;

  @Field(() => Boolean)
  isAllCollectionRequired: boolean;
}

@ObjectType()
export class EventAndGiveAway {
  @Field(() => EventsObject, { nullable: true })
  event?: EventsObject | null;

  @Field(() => GiveAwayPoolObject, { nullable: true })
  giveAway?: GiveAwayPoolObject | null;
}
