import { Injectable } from '@nestjs/common';
import { SessionJoinObject, SessionObject } from './sessions.graphql';
import { Session } from './session.entity';
import { Series } from 'src/series/series.entity';
import { Event } from 'src/events/events.entity';
import { getConnection, getRepository } from 'typeorm';
import { ApolloError } from 'apollo-server-express';
import { SESSIONS_ERRORS } from './sessions.messages';
import { NotificationsService } from '../notifications/notifications.service';
import { User } from '../users/user.entity';
import { VonageService } from 'src/vonage/vonage.service';
import ms from 'ms';
import { USERS_ERRORS } from 'src/users/users.messages';
import { ActivityServices } from '../activity/activity.service';
import { Paging } from 'src/lib/common/common.interfaces';
import { ActivityActionTypes } from 'src/activity/activity.types';
import { Team, TeamMember } from 'src/team/team.entity';
import {
  ExperienceType,
  IInvitedUsers,
  visibilityExperience,
} from './session.types';
import { uniqueFilter } from 'src/lib/common/common.utils';
import { Activity } from 'src/activity/activity.entity';
import { SporeDs } from 'src/spore-nft/spore-ds.entity';
import { SporeService } from 'src/spore-nft/spore.service';
import { BlockedUsers } from 'src/users/blocked-users.entity';
import { Projects } from 'src/projects/projects.entity';

@Injectable()
export class SessionsService {
  constructor(
    private notificationsService: NotificationsService,
    private vonageService: VonageService,
    private activityServices: ActivityServices,
    private sporeService: SporeService,
  ) {}

  async getUserSessions(id: number): Promise<SessionObject[]> {
    const sessions = await getConnection().query(
      `SELECT * FROM session where owner_user_id = ${id}`,
    );

    return sessions.map(async (item: Session) => ({
      ...item,
      ownerUser: await this.getSessionOwner(id),
      viewers: this.countViewers(item),
    }));
  }

  async createSession(
    currentUserId: number,
    topics: [number],
    sessionType?: string | null,
    visibility?: string | null,
    title?: string | null,
    description?: string | null,
    invitedUsers?: [number] | null,
    isNotify?: boolean | null,
    secondScreenLink?: string | null,
    isPrivate?: boolean,
    isTeamsMembersInvited?: boolean,
    teamInvitation?: [number] | null,
    nft?: number,
    nftsInviteUser?: [number] | null,
    nftsRequired?: [number] | null,
    projectId?: number | null,
  ): Promise<SessionJoinObject> {
    const repository = getRepository(Session);
    const topicsCollection = topics.map((topicId) => ({ id: topicId }));
    const nftObject = nft ? await getRepository(SporeDs).findOne(nft) : null;
    if (nftsRequired) {
      await this.sporeService.verifyCollectionNfts(nftsRequired);
    }
    const { userObjectsIds, userIds } = await this.getInvitedUsers(
      invitedUsers ?? [],
      currentUserId,
      isTeamsMembersInvited,
      isTeamsMembersInvited ? null : teamInvitation,
      nftsInviteUser
        ? ((await this.sporeService.idUserCollectionNftByIds(
            nftsInviteUser,
          )) as [number])
        : null,
    );
    const project = await getRepository(Projects).findOne({
      where: { id: projectId },
    });
    const savedSession = await repository.save({
      title,
      description,
      topics: topicsCollection,
      ownerUser: {
        id: currentUserId,
      },
      secondScreenLink,
      isPrivate,
      invitedUsers: userObjectsIds,
      sessionType: this.getType(project?.join || ExperienceType.OPEN),
      visibility: this.getVisibility(project?.project || visibilityExperience.LISTED),
      nft: nftObject,
      nftsRequired,
      projects: project,
    });

    this.notificationsService.sendSessionCreatedNotifications(
      savedSession.id,
      isNotify,
      userIds as [number],
    );

    this.activityServices.sendGroupNotificationActivity(
      userIds,
      savedSession.id,
      currentUserId,
      ActivityActionTypes.CREATE_SESSION,
    );
    const {
      vonageSessionToken,
      vonageUserToken,
    } = await this.vonageService.createSession(currentUserId, savedSession.id);

    await new Promise((resolve) => setTimeout(resolve, 800));
    const vonageApiToken = process.env.OPENTOK_API_KEY || '';
    const newSession = (await repository.findOne(savedSession.id, {
      relations: ['viewerUsers', 'greenRoomUsers', 'projects', 'ownerUser'],
    })) as Session;
    return {
      vonageSessionToken,
      vonageUserToken,
      token: vonageUserToken,
      vonageApiToken,
      session: {
        ...newSession,
        viewers: this.countViewers(newSession),
        ownerUser: newSession.ownerUser,
      },
      projects: newSession.projects,
    };
  }

  async updateSession(
    currentUserId: number,
    sessionId: number,
    description?: string | null,
  ): Promise<SessionObject> {
    const repository = getRepository(Session);
    const foundSession = await repository.findOne(sessionId, {
      relations: ['ownerUser'],
    });

    if (!foundSession) {
      throw new ApolloError(
        SESSIONS_ERRORS.SESSION_NOT_FOUND.MESSAGE,
        SESSIONS_ERRORS.SESSION_NOT_FOUND.CODE,
      );
    }

    if (foundSession.ownerUser.id !== currentUserId) {
      throw new ApolloError(
        SESSIONS_ERRORS.NOT_SESSION_OWNER.MESSAGE,
        SESSIONS_ERRORS.NOT_SESSION_OWNER.CODE,
      );
    }

    await repository.save({
      id: sessionId,
      description,
    });

    const updatedSession = (await repository.findOne(sessionId, {
      relations: ['viewerUsers', 'greenRoomUsers'],
    })) as Session;

    return { ...updatedSession, viewers: this.countViewers(updatedSession) };
  }

  async getLiveSessions(
    currentUserId: number | undefined,
  ): Promise<SessionObject[]> {
    const notInclude: number[] = await this.getIdLiveEventsAndSeries();
    const experienceSecret = await this.getSecretExperiences(
      currentUserId || 0,
    );
    const idNotInclude = [];
    idNotInclude.push(...notInclude);
    idNotInclude.push(...experienceSecret);
    if (currentUserId) {
      const result = (
        await getConnection()
          .createQueryBuilder()
          .relation(User, 'inappropriateUsers')
          .of(currentUserId)
          .loadMany()
      ).map((u) => u.id);

      if (result.length) {
        idNotInclude.push(result);
      }
    }

    const liveSessions = await getRepository(Session)
      .createQueryBuilder('session')
      .where('session.finishedAt is null')
      .leftJoinAndSelect('session.greenRoomUsers', 'greenRoomUsers')
      .leftJoinAndSelect('session.viewerUsers', 'viewerUsers')
      .leftJoinAndSelect('session.topics', 'topics')
      .leftJoinAndSelect('session.projects', 'projects')
      .andWhere('session.id NOT IN (:...idsNotInclude)', {
        idsNotInclude: idNotInclude.length > 0 ? idNotInclude : [0],
      })
      .andWhere(
        'session.visibility != :sessionType OR session.id IN (:...ids)',
        {
          sessionType: visibilityExperience.UNLISTED,
          ids: experienceSecret.length > 0 ? experienceSecret : [0],
        },
      )
      .orderBy('session.createdAt', 'DESC')
      .getMany();

    return await Promise.all(
      liveSessions.map(async (session) => {
        const viewers = session ? this.countViewers(session) : 0;
        const isAvailable = currentUserId
          ? await this.isPrivateAvailable(
              experienceSecret,
              session,
              currentUserId,
            )
          : false;

        return {
          ...session,
          viewers,
          isAvailable,
        };
      }),
    );
  }

  async getLiveSessionsPaging(
    currentUserId: number,
    paging: Paging,
  ): Promise<SessionObject[]> {
    const blockUserId = await this.getUserBlockedUser(currentUserId);
    const experienceSecret = await this.getSecretExperiences(currentUserId);
    const notInclude: number[] = await this.getIdLiveEventsAndSeries();
    const idNotInclude = [];
    idNotInclude.push(...notInclude);

    if (currentUserId) {
      const result = (
        await getConnection()
          .createQueryBuilder()
          .relation(User, 'inappropriateUsers')
          .of(currentUserId)
          .loadMany()
      ).map((u) => u.id);

      if (result.length) {
        idNotInclude.push(result);
      }
    }

    const liveSessions = await getRepository(Session)
      .createQueryBuilder('session')
      .where('session.finishedAt is null')
      .leftJoinAndSelect('session.greenRoomUsers', 'greenRoomUsers')
      .leftJoinAndSelect('session.viewerUsers', 'viewerUsers')
      .leftJoinAndSelect('session.topics', 'topics')
      .leftJoinAndSelect('session.ownerUser', 'ownerUser')
      .leftJoinAndSelect('session.projects', 'projects')
      .andWhere(
        'session.id NOT IN (:...idsNotInclude) AND ownerUser.id NOT IN(:...block)',
        {
          idsNotInclude: idNotInclude.length > 0 ? idNotInclude : [0],
          block: blockUserId.length > 0 ? blockUserId : [0],
        },
      )
      .andWhere(
        'session.visibility != :sessionType  OR session.id IN (:...ids)',
        {
          sessionType: visibilityExperience.UNLISTED,
          ids: experienceSecret.length > 0 ? experienceSecret : [undefined],
        },
      )
      .skip(paging.limit * (paging.page - 1))
      .take(paging.limit)
      .orderBy('session.createdAt', 'ASC')
      .getMany();

    return await Promise.all(
      liveSessions.map(async (session) => {
        const viewers = session ? this.countViewers(session) : 0;
        const isAvailable = await this.isPrivateAvailable(
          experienceSecret,
          session,
          currentUserId,
        );

        return {
          ...session,
          viewers,
          isAvailable,
        };
      }),
    );
  }

  async getSecretExperiences(idUser: number): Promise<number[]> {
    const experiences = await getRepository(Session)
      .createQueryBuilder('session')
      .where('session.finishedAt is null')
      .getMany();

    const idExperiences = [];
    for (const event of experiences) {
      idExperiences.push(event.id);
    }
    return await this.getInvitationsExperiences(
      idExperiences,
      idUser,
      'session',
    );
  }

  async getIdLiveEventsAndSeries(): Promise<number[]> {
    const event = await getRepository(Event)
      .createQueryBuilder('event')
      .leftJoinAndSelect('event.session', 'session')
      .where('event.finishedAt is null')
      .andWhere('event.session is not null')
      .andWhere('session.finishedAt is null')
      .getMany();

    const serie = await getRepository(Series)
      .createQueryBuilder('series')
      .leftJoinAndSelect('series.session', 'session')
      .where('series.finishedAt is null')
      .andWhere('series.session is not null')
      .andWhere('session.finishedAt is null')
      .orderBy('series.createdAt', 'DESC')
      .getMany();

    const eventId: number[] = event.map((event) => event.session?.id || 0);
    const serieId: number[] = serie.map((serie) => serie.session?.id || 0);

    return eventId.concat(serieId);
  }

  async getSessionById(
    sessionId: number,
    idUser: number,
  ): Promise<SessionObject> {
    const experienceSecrets = await this.getSecretExperiences(idUser);
    const repository = getRepository(Session);
    const session = await repository.findOne(sessionId, {
      relations: ['viewerUsers', 'greenRoomUsers', 'invitedUsers', 'projects'],
    });

    if (!session) {
      throw new ApolloError(
        SESSIONS_ERRORS.SESSION_NOT_FOUND.MESSAGE,
        SESSIONS_ERRORS.SESSION_NOT_FOUND.CODE,
      );
    }

    return {
      ...session,
      viewers: this.countViewers(session),
      isAvailable: await this.isPrivateAvailable(
        experienceSecrets,
        session,
        idUser,
      ),
    };
  }

  async joinSession(
    currentUserId: number,
    sessionId: number,
    userId: number,
  ): Promise<SessionJoinObject> {
    const repository = getRepository(Session);
    const userRepository = getRepository(User);
    const session = await repository.findOne(sessionId, {
      relations: [
        'ownerUser',
        'blockedUsers',
        'viewerUsers',
        'greenRoomUsers',
        'participantUsers',
        'invitedUsers',
      ],
    });

    const userToAdd = await userRepository.findOne(userId);

    if (!session) {
      throw new ApolloError(
        SESSIONS_ERRORS.SESSION_NOT_FOUND.MESSAGE,
        SESSIONS_ERRORS.SESSION_NOT_FOUND.CODE,
      );
    }

    if (!userToAdd) {
      throw new ApolloError(
        USERS_ERRORS.USER_NOT_FOUND.MESSAGE,
        USERS_ERRORS.USER_NOT_FOUND.CODE,
      );
    }

    if (session.finishedAt) {
      throw new ApolloError(
        SESSIONS_ERRORS.SESSION_FINISHED.MESSAGE,
        SESSIONS_ERRORS.SESSION_FINISHED.CODE,
      );
    }

    if (session.blockedUsers.find((u) => u.id === currentUserId)) {
      throw new ApolloError(
        SESSIONS_ERRORS.USER_BLOCKED.CODE,
        SESSIONS_ERRORS.USER_BLOCKED.MESSAGE,
      );
    }

    const { vonageSessionToken } = session;
    const vonageUserToken = await this.vonageService.joinSession(
      currentUserId,
      vonageSessionToken,
    );

    const vonageApiToken = process.env.OPENTOK_API_KEY || '';
    const token = vonageUserToken;

    const participantUsers = session.participantUsers || [];
    participantUsers.push(userToAdd as User);
    const viewerUsers = (session.viewerUsers || []).filter(
      (user) => user.id !== userToAdd.id,
    );
    const greenRoomUsers = (session.greenRoomUsers || []).filter(
      (user) => user.id !== userToAdd.id,
    );

    await repository.save({
      id: sessionId,
      participantUsers,
      viewerUsers,
      greenRoomUsers,
    });

    const savedSession = (await repository.findOne(sessionId, {
      relations: ['viewerUsers', 'greenRoomUsers'],
    })) as Session;

    return {
      session: {
        ...savedSession,
        viewers: this.countViewers(savedSession),
      } as SessionObject,
      vonageSessionToken,
      vonageUserToken,
      token,
      vonageApiToken,
    };
  }

  async joinGreenRoomSession(
    currentUserId: number,
    sessionId: number,
    userId: number,
  ): Promise<SessionJoinObject> {
    const repository = getRepository(Session);
    const userRepository = getRepository(User);
    const userToAdd = await userRepository.findOne(userId || currentUserId);
    const session = await repository.findOne(sessionId, {
      relations: [
        'ownerUser',
        'blockedUsers',
        'viewerUsers',
        'greenRoomUsers',
        'participantUsers',
      ],
    });

    if (!session) {
      throw new ApolloError(
        SESSIONS_ERRORS.SESSION_NOT_FOUND.MESSAGE,
        SESSIONS_ERRORS.SESSION_NOT_FOUND.CODE,
      );
    }

    if (!userToAdd) {
      throw new ApolloError(
        USERS_ERRORS.USER_NOT_FOUND.MESSAGE,
        USERS_ERRORS.USER_NOT_FOUND.CODE,
      );
    }

    if (session.finishedAt) {
      throw new ApolloError(
        SESSIONS_ERRORS.SESSION_FINISHED.MESSAGE,
        SESSIONS_ERRORS.SESSION_FINISHED.CODE,
      );
    }

    if (session.blockedUsers.find((u) => u.id === currentUserId)) {
      throw new ApolloError(
        SESSIONS_ERRORS.USER_BLOCKED.CODE,
        SESSIONS_ERRORS.USER_BLOCKED.MESSAGE,
      );
    }

    const { vonageSessionToken } = session;
    const vonageUserToken = await this.vonageService.joinSession(
      currentUserId,
      vonageSessionToken,
    );

    const vonageApiToken = process.env.OPENTOK_API_KEY || '';
    const token = vonageUserToken;
    const greenRoomUsers = session.greenRoomUsers || [];
    const viewerUsers = (session.viewerUsers || []).filter(
      (user) => user.id !== userToAdd.id,
    );
    const participantUsers = (session.participantUsers || []).filter(
      (user) => user.id !== userToAdd.id,
    );

    greenRoomUsers.push(userToAdd as User);

    await repository.save({
      id: sessionId,
      greenRoomUsers,
      viewerUsers,
      participantUsers,
    });

    const savedSession = await repository.findOne(sessionId, {
      relations: ['viewerUsers', 'greenRoomUsers'],
    });

    if (!savedSession) {
      throw new ApolloError(
        SESSIONS_ERRORS.SESSION_NOT_FOUND.MESSAGE,
        SESSIONS_ERRORS.SESSION_NOT_FOUND.CODE,
      );
    }

    return {
      session: { ...savedSession, viewers: this.countViewers(savedSession) },
      vonageSessionToken,
      vonageUserToken,
      token,
      vonageApiToken,
    };
  }

  async viewSession(
    currentUserId: number,
    sessionId: number,
    userId: number,
  ): Promise<SessionJoinObject> {
    const repository = getRepository(Session);
    const userRepository = getRepository(User);
    const session = await repository.findOne(sessionId, {
      relations: [
        'blockedUsers',
        'viewerUsers',
        'greenRoomUsers',
        'participantUsers',
      ],
    });
    const userToAdd = await userRepository.findOne(userId || currentUserId);

    if (!session) {
      throw new ApolloError(
        SESSIONS_ERRORS.SESSION_NOT_FOUND.MESSAGE,
        SESSIONS_ERRORS.SESSION_NOT_FOUND.CODE,
      );
    }

    if (!userToAdd) {
      throw new ApolloError(
        USERS_ERRORS.USER_NOT_FOUND.MESSAGE,
        USERS_ERRORS.USER_NOT_FOUND.CODE,
      );
    }

    if (session.finishedAt) {
      throw new ApolloError(
        SESSIONS_ERRORS.SESSION_FINISHED.MESSAGE,
        SESSIONS_ERRORS.SESSION_FINISHED.CODE,
      );
    }

    if (session.blockedUsers.find((u) => u.id === currentUserId)) {
      throw new ApolloError(
        SESSIONS_ERRORS.USER_BLOCKED.CODE,
        SESSIONS_ERRORS.USER_BLOCKED.MESSAGE,
      );
    }

    const { vonageSessionToken } = session;
    const vonageUserToken = await this.vonageService.viewSession(
      currentUserId,
      vonageSessionToken,
    );
    const vonageApiToken = process.env.OPENTOK_API_KEY || '';
    const token = vonageUserToken;
    const viewerUsers = session.viewerUsers || [];
    viewerUsers.push(userToAdd);
    const participantUsers = session.participantUsers.filter(
      (user) => user.id !== userToAdd.id,
    );
    const greenRoomUsers = session.greenRoomUsers.filter(
      (user) => user.id !== userToAdd.id,
    );

    await repository.save({
      id: sessionId,
      viewerUsers,
      participantUsers,
      greenRoomUsers,
    });

    const savedSession = await repository.findOne(sessionId, {
      relations: ['viewerUsers', 'greenRoomUsers'],
    });

    if (!savedSession) {
      throw new ApolloError(
        SESSIONS_ERRORS.SESSION_NOT_FOUND.MESSAGE,
        SESSIONS_ERRORS.SESSION_NOT_FOUND.CODE,
      );
    }

    return {
      session: { ...savedSession, viewers: this.countViewers(savedSession) },
      vonageSessionToken,
      vonageUserToken,
      token,
      vonageApiToken,
    };
  }

  async shareSession(
    currentUserId: number,
    sessionId: number,
    invitedUsers: [number],
    notifyMyFollowers: boolean,
    teamInvitation?: [number] | null,
    isTeamsMembersInvited?: boolean,
  ): Promise<boolean> {
    const repository = getRepository(Session);
    const session = await repository.findOne(sessionId, {
      relations: ['ownerUser'],
    });
    if (!session) {
      throw new ApolloError(
        SESSIONS_ERRORS.SESSION_NOT_FOUND.MESSAGE,
        SESSIONS_ERRORS.SESSION_NOT_FOUND.CODE,
      );
    }
    const { userIds } = await this.getInvitedUsers(
      invitedUsers ?? [],
      currentUserId,
      isTeamsMembersInvited,
      teamInvitation,
    );

    this.notificationsService.sendSessionCreatedNotifications(
      sessionId,
      notifyMyFollowers,
      userIds as [number],
    );

    return true;
  }

  async endSession(
    currentUserId: number,
    sessionId: number,
  ): Promise<SessionObject> {
    const repository = getRepository(Session);
    const session = await repository.findOne(sessionId, {
      relations: ['ownerUser'],
    });

    if (!session) {
      throw new ApolloError(
        SESSIONS_ERRORS.SESSION_NOT_FOUND.MESSAGE,
        SESSIONS_ERRORS.SESSION_NOT_FOUND.CODE,
      );
    }

    if (session.ownerUser.id !== currentUserId) {
      if (currentUserId !== 481) {
        throw new ApolloError(
          SESSIONS_ERRORS.NOT_SESSION_OWNER.MESSAGE,
          SESSIONS_ERRORS.NOT_SESSION_OWNER.CODE,
        );
      }
    }

    if (session.finishedAt) {
      throw new ApolloError(
        SESSIONS_ERRORS.SESSION_FINISHED.MESSAGE,
        SESSIONS_ERRORS.SESSION_FINISHED.CODE,
      );
    }

    const finishedAt = new Date(Date.now() - ms('15m'));

    await repository.save({ id: sessionId, finishedAt });

    const savedSession = await repository.findOne(sessionId, {
      relations: ['viewerUsers', 'greenRoomUsers'],
    });

    if (!savedSession) {
      throw new ApolloError(
        SESSIONS_ERRORS.SESSION_NOT_FOUND.MESSAGE,
        SESSIONS_ERRORS.SESSION_NOT_FOUND.CODE,
      );
    }

    return { ...savedSession, viewers: this.countViewers(savedSession) };
  }

  async leaveSession(
    currentUserId: number,
    sessionId: number,
  ): Promise<SessionObject> {
    const repository = getRepository(Session);
    const session = await repository.findOne(sessionId, {
      relations: ['viewerUsers', 'greenRoomUsers', 'participantUsers'],
    });

    if (!session) {
      throw new ApolloError(
        SESSIONS_ERRORS.SESSION_NOT_FOUND.MESSAGE,
        SESSIONS_ERRORS.SESSION_NOT_FOUND.CODE,
      );
    }

    const participantUsers = session.participantUsers.filter(
      (user) => user.id !== currentUserId,
    );
    const viewerUsers = session.viewerUsers.filter(
      (user) => user.id !== currentUserId,
    );
    const greenRoomUsers = session.greenRoomUsers.filter(
      (user) => user.id !== currentUserId,
    );

    await repository.save({
      id: sessionId,
      participantUsers,
      viewerUsers,
      greenRoomUsers,
    });
    const savedSession = (await repository.findOne(sessionId, {
      relations: ['viewerUsers', 'greenRoomUsers'],
    })) as Session;

    return { ...savedSession, viewers: this.countViewers(session) };
  }

  async kickUserFromSession(
    currentUserId: number,
    sessionId: number,
    userId: number,
  ): Promise<SessionObject> {
    const repository = getRepository(Session);
    const session = await repository.findOne(sessionId);

    if (!session) {
      throw new ApolloError(
        SESSIONS_ERRORS.SESSION_NOT_FOUND.MESSAGE,
        SESSIONS_ERRORS.SESSION_NOT_FOUND.CODE,
      );
    }

    if (session.ownerUser.id !== currentUserId) {
      throw new ApolloError(
        SESSIONS_ERRORS.NOT_SESSION_OWNER.MESSAGE,
        SESSIONS_ERRORS.NOT_SESSION_OWNER.CODE,
      );
    }

    const participantUsers = (session.participantUsers || []).filter(
      (user) => user.id !== userId,
    );
    const viewerUsers = (session.viewerUsers || []).filter(
      (user) => user.id !== userId,
    );
    const greenRoomUsers = (session.greenRoomUsers || []).filter(
      (user) => user.id !== userId,
    );

    await repository.save({
      id: sessionId,
      participantUsers,
      viewerUsers,
      greenRoomUsers,
    });
    const savedSession = (await repository.findOne(sessionId, {
      relations: ['viewerUsers', 'greenRoomUsers'],
    })) as Session;

    return { ...savedSession, viewers: this.countViewers(session) };
  }

  async getSessionOwner(id: number): Promise<User> {
    return getConnection()
      .createQueryBuilder()
      .relation(Session, 'ownerUser')
      .of(id)
      .loadOne() as Promise<User>;
  }

  async getSessionParticipants(id: number): Promise<User[]> {
    return getConnection()
      .createQueryBuilder()
      .relation(Session, 'participantUsers')
      .of(id)
      .loadMany();
  }

  async getSessionGreenRoomUsers(id: number): Promise<User[]> {
    return getConnection()
      .createQueryBuilder()
      .relation(Session, 'greenRoomUsers')
      .of(id)
      .loadMany();
  }

  async getSessionViewers(id: number): Promise<User[]> {
    return getConnection()
      .createQueryBuilder()
      .relation(Session, 'viewerUsers')
      .of(id)
      .loadMany();
  }

  async handleUserJoined(
    sessionId: number,
    userId: number,
  ): Promise<SessionObject | undefined> {
    const repository = getRepository(Session);
    const session = await repository.findOne(sessionId, {
      relations: ['participantUsers'],
    });

    if (!session) {
      throw new ApolloError(
        SESSIONS_ERRORS.SESSION_NOT_FOUND.MESSAGE,
        SESSIONS_ERRORS.SESSION_NOT_FOUND.CODE,
      );
    }

    if (session.finishedAt) {
      throw new ApolloError(
        SESSIONS_ERRORS.SESSION_FINISHED.MESSAGE,
        SESSIONS_ERRORS.SESSION_FINISHED.CODE,
      );
    }

    if (session.participantUsers.find((u) => u.id === userId)) return;

    await getConnection()
      .createQueryBuilder()
      .relation(Session, 'participantUsers')
      .of(sessionId)
      .add(userId);

    const savedSession = (await repository.findOne(sessionId, {
      relations: ['viewerUsers', 'greenRoomUsers'],
    })) as Session;

    return { ...savedSession, viewers: this.countViewers(savedSession) };
  }

  async handleUserLeft(
    sessionId: number,
    userId: number,
  ): Promise<SessionObject | undefined> {
    const repository = getRepository(Session);
    const session = await repository.findOne(sessionId, {
      relations: ['participantUsers'],
    });

    if (!session) {
      throw new ApolloError(
        SESSIONS_ERRORS.SESSION_NOT_FOUND.MESSAGE,
        SESSIONS_ERRORS.SESSION_NOT_FOUND.CODE,
      );
    }

    if (!session.participantUsers.find((u) => u.id === userId)) return;

    await getConnection()
      .createQueryBuilder()
      .relation(Session, 'participantUsers')
      .of(sessionId)
      .remove(userId);

    const savedSession = (await repository.findOne(sessionId, {
      relations: ['viewerUsers', 'greenRoomUsers'],
    })) as Session;

    return { ...savedSession, viewers: this.countViewers(savedSession) };
  }

  async handleViewerJoined(
    sessionId: number,
    userId: number,
  ): Promise<SessionObject | undefined> {
    const repository = getRepository(Session);
    const session = await repository.findOne(sessionId, {
      relations: ['viewerUsers'],
    });

    if (!session) {
      throw new ApolloError(
        SESSIONS_ERRORS.SESSION_NOT_FOUND.MESSAGE,
        SESSIONS_ERRORS.SESSION_NOT_FOUND.CODE,
      );
    }

    if (session.finishedAt) {
      throw new ApolloError(
        SESSIONS_ERRORS.SESSION_FINISHED.MESSAGE,
        SESSIONS_ERRORS.SESSION_FINISHED.CODE,
      );
    }

    if (session.viewerUsers.find((u) => u.id === userId)) return;

    await getConnection()
      .createQueryBuilder()
      .relation(Session, 'viewerUsers')
      .of(sessionId)
      .add(userId);

    const savedSession = (await repository.findOne(sessionId, {
      relations: ['viewerUsers', 'greenRoomUsers'],
    })) as Session;

    return { ...savedSession, viewers: this.countViewers(savedSession) };
  }

  async handleViewerLeft(
    sessionId: number,
    userId: number,
  ): Promise<SessionObject | undefined> {
    const repository = getRepository(Session);
    const session = await repository.findOne(sessionId, {
      relations: ['viewerUsers'],
    });

    if (!session) {
      throw new ApolloError(
        SESSIONS_ERRORS.SESSION_NOT_FOUND.MESSAGE,
        SESSIONS_ERRORS.SESSION_NOT_FOUND.CODE,
      );
    }

    if (!session.viewerUsers.find((u) => u.id === userId)) return;

    await getConnection()
      .createQueryBuilder()
      .relation(Session, 'viewerUsers')
      .of(sessionId)
      .remove(userId);

    const savedSession = (await repository.findOne(sessionId, {
      relations: ['viewerUsers', 'greenRoomUsers'],
    })) as Session;

    return { ...savedSession, viewers: this.countViewers(savedSession) };
  }

  async handleGreenRoomUserJoined(
    sessionId: number,
    userId: number,
  ): Promise<SessionObject | undefined> {
    const repository = getRepository(Session);
    const session = await repository.findOne(sessionId, {
      relations: ['greenRoomUsers'],
    });

    if (!session) {
      throw new ApolloError(
        SESSIONS_ERRORS.SESSION_NOT_FOUND.MESSAGE,
        SESSIONS_ERRORS.SESSION_NOT_FOUND.CODE,
      );
    }

    if (session.finishedAt) {
      throw new ApolloError(
        SESSIONS_ERRORS.SESSION_FINISHED.MESSAGE,
        SESSIONS_ERRORS.SESSION_FINISHED.CODE,
      );
    }

    if (session.greenRoomUsers.find((u) => u.id === userId)) return;

    await getConnection()
      .createQueryBuilder()
      .relation(Session, 'greenRoomUsers')
      .of(sessionId)
      .add(userId);

    const savedSession = (await repository.findOne(sessionId, {
      relations: ['viewerUsers', 'greenRoomUsers'],
    })) as Session;

    return { ...savedSession, viewers: this.countViewers(savedSession) };
  }

  async handleGreenRoomUserLeft(
    sessionId: number,
    userId: number,
  ): Promise<SessionObject | undefined> {
    const repository = getRepository(Session);
    const session = await repository.findOne(sessionId, {
      relations: ['greenRoomUsers'],
    });

    if (!session) {
      throw new ApolloError(
        SESSIONS_ERRORS.SESSION_NOT_FOUND.MESSAGE,
        SESSIONS_ERRORS.SESSION_NOT_FOUND.CODE,
      );
    }

    if (!session.greenRoomUsers.find((u) => u.id === userId)) return;

    await getConnection()
      .createQueryBuilder()
      .relation(Session, 'greenRoomUsers')
      .of(sessionId)
      .remove(userId);

    const savedSession = (await repository.findOne(sessionId, {
      relations: ['viewerUsers', 'greenRoomUsers'],
    })) as Session;

    return { ...savedSession, viewers: this.countViewers(savedSession) };
  }

  async handleSessionClose(
    sessionId: number,
  ): Promise<SessionObject | undefined> {
    const repository = getRepository(Session);
    const session = await repository.findOne(sessionId, {
      relations: ['participantUsers', 'greenRoomUsers', 'viewerUsers'],
    });

    if (!session) {
      throw new ApolloError(
        SESSIONS_ERRORS.SESSION_NOT_FOUND.MESSAGE,
        SESSIONS_ERRORS.SESSION_NOT_FOUND.CODE,
      );
    }

    if (
      session.finishedAt &&
      !session.participantUsers.length &&
      !session.greenRoomUsers.length &&
      !session.viewerUsers.length
    ) {
      return;
    }

    await repository.save({
      id: sessionId,
      participantUsers: [],
      viewerUsers: [],
      finishedAt: session.finishedAt || new Date(),
    });

    const savedSession = (await repository.findOne(sessionId, {
      relations: ['viewerUsers', 'greenRoomUsers'],
    })) as Session;

    return { ...savedSession, viewers: this.countViewers(savedSession) };
  }

  async handleUserBlocked(sessionId: number, userId: number): Promise<void> {
    const repository = getRepository(Session);
    const session = await repository.findOne(sessionId, {
      relations: [
        'participantUsers',
        'greenRoomUsers',
        'viewerUsers',
        'blockedUsers',
      ],
    });

    if (!session) {
      throw new ApolloError(
        SESSIONS_ERRORS.SESSION_NOT_FOUND.MESSAGE,
        SESSIONS_ERRORS.SESSION_NOT_FOUND.CODE,
      );
    }

    const queries = [];

    if (session.participantUsers.find((u) => u.id === userId)) {
      queries.push(
        getConnection()
          .createQueryBuilder()
          .relation(Session, 'participantUsers')
          .of(sessionId)
          .remove(userId),
      );
    }

    if (session.participantUsers.find((u) => u.id === userId)) {
      queries.push(
        getConnection()
          .createQueryBuilder()
          .relation(Session, 'greenRoomUsers')
          .of(sessionId)
          .remove(userId),
      );
    }

    if (session.viewerUsers.find((u) => u.id === userId)) {
      queries.push(
        getConnection()
          .createQueryBuilder()
          .relation(Session, 'viewerUsers')
          .of(sessionId)
          .remove(userId),
      );
    }

    if (!session.blockedUsers.find((u) => u.id === userId)) {
      queries.push(
        getConnection()
          .createQueryBuilder()
          .relation(Session, 'blockedUsers')
          .of(sessionId)
          .add(userId),
      );
    }

    await Promise.all(queries);
  }

  countViewers(session: Session): number {
    const viewerUsers = !!session.viewerUsers ? session.viewerUsers.length : 0;
    const greenRoomUsers = !!session.greenRoomUsers
      ? session.greenRoomUsers.length
      : 0;
    return viewerUsers + greenRoomUsers;
  }

  async getInvitedUsers(
    invitedUsers: number[],
    currentUser: number,
    isAllTeamsMembersInvited?: boolean,
    teams?: [number] | null,
    userNfts?: [number] | null,
  ): Promise<IInvitedUsers> {
    const teamUsers = teams ? await this.getIdsMembersTeam(teams) : [];
    const teamMembers = isAllTeamsMembersInvited
      ? await this.getAllTeamMembersRelatedToUser(currentUser)
      : [];
    const initialUsersArray = uniqueFilter([
      ...invitedUsers,
      ...teamMembers,
      ...teamUsers,
      ...(userNfts || []),
    ]).filter((id) => id !== currentUser);
    return {
      userObjectsIds: initialUsersArray.map((userId) => ({ id: userId })),
      userIds: initialUsersArray,
    };
  }

  async getAllTeamMembersRelatedToUser(currentUser: number): Promise<number[]> {
    const repo = getRepository(Team);

    const teams = await repo
      .createQueryBuilder('team')
      .leftJoinAndSelect('team.members', 'members')
      .leftJoinAndSelect('team.ownerUser', 'ownerUser')
      .where(
        `ownerUser.id = :currentUser 
        OR (members.user.id = :currentUser 
        AND members.isAccepted = :isAccepted 
        AND team.membersAllowedToInvite = :isAccepted)`,
        { currentUser, isAccepted: true },
      )
      .leftJoinAndSelect(
        'team.members',
        'allMembers',
        'allMembers.isAccepted = :isAccepted',
        { isAccepted: true },
      )
      .orderBy('team.createdAt', 'DESC')
      .getMany();

    const getMember = await this.getMembersIds(teams);
    const ownerUser = await this.getOwnerTeamInvitation(teams);

    return getMember.concat(ownerUser);
  }

  async getOwnerTeamInvitation(teams: Team[]): Promise<number[]> {
    const teamMembersId = teams.map((team) => team.ownerUser.id);
    return [...new Set(teamMembersId)];
  }

  async getMembersIds(teams: Team[]): Promise<number[]> {
    const teamMembersId = teams.map(({ members }) =>
      members.map(({ id }) => id),
    );
    const membersId: any = [];
    teamMembersId.map((id) => membersId.push(...id));

    const users = await this.getMembers(membersId);
    return uniqueFilter(users.map(({ user }) => user.id));
  }

  async getMembers(membersIds: number[]): Promise<TeamMember[]> {
    const repository = getRepository(TeamMember);
    return await repository.findByIds(membersIds, { relations: ['user'] });
  }

  async getIdsMembersTeam(team: number[]): Promise<number[]> {
    const teamIds = team.length > 0 ? team : [0];
    const repo = getRepository(Team);
    const teams = await repo
      .createQueryBuilder('team')
      .leftJoinAndSelect('team.members', 'members')
      .leftJoinAndSelect('team.ownerUser', 'ownerUser')
      .where(`team.id IN (:...teamIds)`, { teamIds })
      .leftJoinAndSelect(
        'team.members',
        'allMembers',
        'allMembers.isAccepted = :isAccepted',
        { isAccepted: true },
      )
      .orderBy('team.createdAt', 'DESC')
      .getMany();

    const ownerUser = await this.getOwnerTeamInvitation(teams);
    const getMember = await this.getMembersIds(teams);
    return ownerUser.concat(getMember);
  }

  async getInvitationsExperiences(
    idExperiences: number[],
    targetUser: number,
    type: string,
  ): Promise<number[]> {
    const activities = await getRepository(Activity)
      .createQueryBuilder('activity')
      .leftJoinAndSelect('activity.targetUser', 'targetUser')
      .leftJoinAndSelect('activity.sourceUser', 'sourceUser')
      .where(
        'activity.procedure_action IN (:...ids) AND activity.type_action=:type',
        {
          ids: idExperiences.length > 0 ? idExperiences : [undefined],
          type,
        },
      )
      .andWhere('activity.targetUser =:targetUser', { targetUser })
      .getMany();

    const idEvents = [];
    for (const event of activities) {
      idEvents.push(event.procedure_action);
    }

    return idEvents;
  }

  getType(type: string): ExperienceType {
    this.verifyTypeExperience(type);
    if (type === 'Open') {
      return ExperienceType.OPEN;
    } else if (type === 'Closed') {
      return ExperienceType.CLOSED;
    } else {
      return ExperienceType.EXCLUSIVE;
    }
  }

  verifyTypeExperience(type: string): void {
    if (type !== 'Open' && type !== 'Closed' && type !== 'Exclusive') {
      throw new ApolloError(
        SESSIONS_ERRORS.TYPE_ERROR.MESSAGE,
        SESSIONS_ERRORS.TYPE_ERROR.CODE,
      );
    }
  }

  async isPrivateAvailable(
    experienceSecrets: number[],
    session: Session,
    idUser: number,
  ): Promise<boolean> {
    if (session.sessionType === ExperienceType.CLOSED) {
      const isAvailable = experienceSecrets.find(
        (element) => element === session.id,
      )
        ? true
        : false;
      if (isAvailable) {
        return true;
      }

      return await this.IsUserHasCollection(idUser, session);
    }

    if (session.sessionType === ExperienceType.EXCLUSIVE) {
      return await this.IsUserHasCollection(idUser, session);
    }

    return true;
  }

  async IsUserHasCollection(
    idUser: number,
    session: Session,
  ): Promise<boolean> {
    if (session.projects) {
      const project = await getRepository(Projects).findOne(
        session.projects.id,
        {
          relations: ['nft'],
        },
      );
      if (!project || !project.nft) {
        return true;
      }
      let isNftRequired = true;
      const idNftsRequired = project.nft.map((nft) => nft.id);

      const nameCollections = await this.sporeService.getNameCollectionNfts(
        idNftsRequired,
      );

      const idUserNft: number[] = [];
      for (const collection of nameCollections) {
        idUserNft.push(
          ...(await this.sporeService.idUserCollectionNft(collection || '')),
        );
      }

      isNftRequired = idUserNft.find((element) => element === idUser)
        ? true
        : false;

      if (!isNftRequired) {
        return false;
      } else {
        return true;
      }
    }
    return true;
  }

  async getUserBlockedUser(currentUser: number): Promise<number[]> {
    const blocked = await getRepository(BlockedUsers)
      .createQueryBuilder('user')
      .leftJoinAndSelect('user.sourceUser', 'sourceUser')
      .leftJoinAndSelect('user.targetUser', 'targetUser')
      .where('sourceUser.id = :id OR targetUser.id = :id', { id: currentUser })
      .getMany();

    const ids: number[] = [];
    blocked.map((user) => {
      if (user.sourceUser.id !== currentUser) ids.push(user.sourceUser.id);
      else ids.push(user.targetUser.id);
    });

    return ids;
  }

  getVisibility(type: string): visibilityExperience {
    if (type === 'Listed') {
      return visibilityExperience.LISTED;
    } else {
      return visibilityExperience.UNLISTED;
    }
  }
}
