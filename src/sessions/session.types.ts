export interface IInvitedUsers {
  userObjectsIds: { id: number }[];
  userIds: number[];
}

export enum ExperienceType {
  OPEN = 'Open',
  CLOSED = 'Closed',
  EXCLUSIVE = 'Exclusive',
}

export enum visibilityExperience {
  LISTED = 'Listed',
  UNLISTED = 'Unlisted',
}
