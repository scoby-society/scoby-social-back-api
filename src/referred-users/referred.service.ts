import { Injectable } from '@nestjs/common';
import { MetricsServices } from 'src/nfts-metrics/metrics.service';
import { SporeDs } from 'src/spore-nft/spore-ds.entity';
import { User } from 'src/users/user.entity';
import { UserProfileHost } from 'src/users/users.graphql';
import { getConnection, getRepository } from 'typeorm';
import { Referred } from './referred.entity';
import { ReferredObject } from './referred.graphql';

@Injectable()
export class ReferredService {
  constructor(private metricsServices: MetricsServices) {}

  async createUserReferred(
    targetUserId: number,
    currentUserId: number,
  ): Promise<boolean> {
    const createdAt = new Date(Date.now());

    await getConnection()
      .createQueryBuilder()
      .insert()
      .into(Referred)
      .values({
        sourceUser: {
          id: currentUserId,
        },
        targetUser: {
          id: targetUserId,
        },
        createdAt: createdAt,
      })
      .orIgnore()
      .execute();

    return true;
  }

  async getReferredUser(currentUserId: number): Promise<ReferredObject[]> {
    return (await getRepository(Referred)
      .createQueryBuilder('user')
      .leftJoinAndSelect('user.sourceUser', 'sourceUser')
      .leftJoinAndSelect('user.targetUser', 'targetUser')
      .where('user.targetUser = :currentUserId', { currentUserId })
      .orderBy('user.createdAt', 'DESC')
      .getMany()) as ReferredObject[];
  }

  async getMyParentReferrals(currentUserId: number): Promise<ReferredObject[]> {
    return (await getRepository(Referred)
      .createQueryBuilder('user')
      .leftJoinAndSelect('user.sourceUser', 'sourceUser')
      .leftJoinAndSelect('user.targetUser', 'targetUser')
      .where('user.sourceUser = :currentUserId', { currentUserId })
      .orderBy('user.createdAt', 'DESC')
      .getMany()) as ReferredObject[];
  }

  async getUserHost(userId: number): Promise<UserProfileHost[]> {
    const sporas = await getRepository(SporeDs)
      .createQueryBuilder('spore')
      .leftJoinAndSelect('spore.ownerUser', 'owner')
      .where('spore.symbol=:name', { name: 'SCOBYNYC' })
      .orderBy('spore.serialNumber', 'DESC')
      .getMany();

    const idUser: number[] = sporas.map((spora) => spora.ownerUser?.id || 0);

    const users = await getRepository(User)
      .createQueryBuilder('user')
      .where('user.id IN(:...ids) OR user.id = :userId', {
        ids: idUser.length > 0 ? idUser : [undefined],
        userId,
      })
      .getMany();

    const userHost: any[] = [];
    for (const user of users) {
      const userRoyalties = await this.metricsServices.getSocialNetworkUser(
        user.id,
      );
      const totalRoyalties =
        userRoyalties.length > 0 && userRoyalties[0].totalRoyalties
          ? userRoyalties[0].totalRoyalties
          : 0;

      if (idUser.find((id) => id === user.id)) {
        userHost.push({ ...user, host: true, totalRoyalties });
      } else {
        userHost.push({ ...user, host: false, totalRoyalties });
      }
    }
    const posUserId = userHost
      .map(function (e) {
        return e.id;
      })
      .indexOf(userId);

    if (posUserId > 0) {
      const burble = userHost[0];
      userHost[0] = userHost[posUserId];
      userHost[posUserId] = burble;
    }

    return userHost;
  }
}
