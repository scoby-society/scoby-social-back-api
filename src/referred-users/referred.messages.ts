export const REFERRED_MESSAGES = {
  REFERRED_NOT_FOUND: {
    CODE: 'ERR_REFERRED_NOT_FOUND',
    MESSAGE: 'Referred not found',
  },
  USER_REFERRED_NOT_FOUND: {
    CODE: 'ERR_REDERRED_USER_NOT_FOUND',
    MESSAGE: 'User not found',
  },
};
