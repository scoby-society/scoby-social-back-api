import { Module } from '@nestjs/common';
import { ReferredService } from './referred.service';
import { ReferredResolver } from './referred.resolver';
import { JwtModule } from '../lib/jwt/jwt.module';
import { MetricsModule } from 'src/nfts-metrics/metrics.module';

@Module({
  imports: [JwtModule, MetricsModule],
  providers: [ReferredService, ReferredResolver],
  exports: [ReferredService],
})
export class ReferredModule {}
