import {
  CreateDateColumn,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { User } from '../users/user.entity';
// sourceUser usuario referido a TargetUser
@Entity()
export class Referred {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User, {
    nullable: false,
    onDelete: 'CASCADE',
    primary: true,
  })
  @Index()
  @JoinColumn({ name: 'source_user_id' })
  sourceUser: User;

  @ManyToOne(() => User, {
    nullable: false,
    onDelete: 'CASCADE',
    primary: true,
  })
  @Index()
  @JoinColumn({ name: 'target_user_id' })
  targetUser: User;

  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;
}
