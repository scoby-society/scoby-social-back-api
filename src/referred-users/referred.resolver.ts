import { UseGuards } from '@nestjs/common';
import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { Query } from '@nestjs/graphql';
import { CurrentUser } from 'src/lib/common/current-user.decorator';
import { JwtGuard } from 'src/lib/jwt/jwt.guard';
import { BasePayload } from 'src/lib/jwt/jwt.service';
import { UserProfileHost } from 'src/users/users.graphql';
import { ReferredObject } from './referred.graphql';
import { ReferredService } from './referred.service';

@Resolver('Referred')
export class ReferredResolver {
  constructor(private referredService: ReferredService) {}

  @Mutation(() => Boolean)
  @UseGuards(JwtGuard)
  async createUserReferred(
    @CurrentUser() currentUser: BasePayload,
    @Args('sourceUserId', { nullable: true }) sourceUserId: number,
    @Args('referredUserId') referredUserId: number,
  ): Promise<boolean> {
    return this.referredService.createUserReferred(
      referredUserId,
      sourceUserId ?? currentUser.id,
    );
  }

  @Query(() => [ReferredObject])
  @UseGuards(JwtGuard)
  async getMyReferrals(
    @CurrentUser() currentUser: BasePayload,
    @Args('id', { nullable: true }) idUser: number,
  ): Promise<ReferredObject[]> {
    return this.referredService.getReferredUser(idUser ?? currentUser.id);
  }

  @Query(() => [ReferredObject])
  @UseGuards(JwtGuard)
  async getMyParentReferrals(
    @CurrentUser() currentUser: BasePayload,
    @Args('id', { nullable: true }) idUser: number,
  ): Promise<ReferredObject[]> {
    return this.referredService.getMyParentReferrals(idUser ?? currentUser.id);
  }

  @Query(() => [UserProfileHost])
  @UseGuards(JwtGuard)
  async getHostUser(
    @Args('id', { nullable: true }) idUser: number,
  ): Promise<UserProfileHost[]> {
    return this.referredService.getUserHost(idUser);
  }
}
