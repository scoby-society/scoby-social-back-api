import { Module } from '@nestjs/common';
import { ChatKittyService } from './chatKitty.service';

@Module({
  providers: [ChatKittyService],
  exports: [ChatKittyService],
})
export class ChatKittyModule {}
