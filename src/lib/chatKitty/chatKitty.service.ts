import { Injectable } from '@nestjs/common';
import FormData from 'form-data';
import { ConfigService } from '@nestjs/config';
import { ChatkittyConfig } from 'src/config/chatkitty.config';
import { UserProfile } from 'src/users/users.graphql';
import { getRepository } from 'typeorm';
import { User } from 'src/users/user.entity';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const axios = require('axios');
@Injectable()
export class ChatKittyService {
  config: ChatkittyConfig;

  constructor(private configService: ConfigService) {
    this.config = this.configService.get('chatkitty') as ChatkittyConfig;
  }

  async getToken(): Promise<any> {
    const token = await this.getAccessToken();
    return { headers: { Authorization: `Bearer ${token}` } };
  }

  async getAccessToken(): Promise<string> {
    const form = new FormData();
    form.append('grant_type', 'client_credentials');
    form.append('username', `${this.config.username}`);
    form.append('password', `${this.config.password}`);

    const response = await axios.post(
      'https://authorization.chatkitty.com/oauth/token',
      form,
      {
        headers: {
          ...form.getHeaders(),
        },
        auth: {
          username: `${this.config.username}`,
          password: `${this.config.password}`,
        },
      },
    );

    return response.data.access_token;
  }

  async updateAllUsers(id: number): Promise<number> {
    const configToken = await this.getToken();
    const config = {
      headers: configToken.headers,
      params: {
        page: id - 1,
      },
    };
    const result = await axios.get(
      `${this.config.apiUrl}/${this.config.idToken}/users`,
      config,
    );

    for (const user of result.data._embedded.users) {
      const userProfile = await getRepository(User)
        .createQueryBuilder('user')
        .where('user.username =:username', { username: user.displayName })
        .getOne();
      if (userProfile) {
        await this.updatedUser(userProfile, user.id);
      }
    }

    return [...result.data._embedded.users].length;
  }

  async updatedUser(user: UserProfile, idUser: number): Promise<void> {
    const configToken = await this.getToken();
    const data = {
      id: idUser,
      type: 'PERSON',
      name: 'string',
      displayName: `${user.username}`,
      displayPictureUrl: 'string',
      isGuest: false,
      presence: {
        status: 'AVAILABLE',
        online: true,
      },
      callStatus: 'AVAILABLE',
      properties: {
        fullName: `${user.fullName}`,
      },
    };

    const config = {
      method: 'put',
      url: `https://api.chatkitty.com/v1/applications/19158/users/${idUser}`,
      headers: {
        'Content-Type': 'application/json; charset=utf8',
        Authorization: `${configToken.headers.Authorization}`,
      },
      data: data,
    };

    axios(config)
      .then(function (response: any) {
        console.log(JSON.stringify(response.data));
      })
      .catch(function (error: any) {
        console.log(error);
      });

    if (user.avatar) {
      this.updateImageUser(idUser, user.avatar);
    }
  }

  async searchAndUpdateUser(id: number, idKitty: number): Promise<void> {
    const repository = getRepository(User);
    const userProfile = await repository.findOne(id);
    if (userProfile) {
      await this.updatedUser(userProfile, idKitty);
    }
  }

  async updateImageUser(idUser: number, urlImage: string): Promise<void> {
    const configToken = await this.getToken();
    const data = {
      url: urlImage,
      name: `profile${idUser}.jpg`,
      contentType: 'image/jpg',
      size: 32768,
    };

    const config = {
      method: 'post',
      url: `${this.config.apiUrl}/${this.config.idToken}/users/${idUser}/display-picture`,
      headers: {
        'Content-Type': 'application/json; charset=utf8',
        Authorization: `${configToken.headers.Authorization}`,
      },
      data: data,
    };

    await axios(config);
  }

  async getUserById(id: number): Promise<void> {
    const configToken = await this.getToken();
    const result = await axios.get(
      `${this.config.apiUrl}/${this.config.idToken}/users/${id}`,
      configToken,
    );
    console.log('------------------------');
    console.log(result.data);
  }

  async getUseChannels(id: number): Promise<void> {
    const configToken = await this.getToken();
    const result = await axios.get(
      `${this.config.apiUrl}/${this.config.idToken}/users/${id}/channels`,
      configToken,
    );
    console.log('------------------------');
    console.log(result.data);
  }

  async createUser(user: UserProfile): Promise<void> {
    const configToken = await this.getToken();
    const params = {
      name: user.id,
      displayName: user.username,
      isGuest: false,
      properties: {
        fullName: `${user.fullName}`,
      },
    };
    const result = await axios.post(
      `${this.config.apiUrl}/${this.config.idToken}/users`,
      params,
      configToken,
    );
    console.log('----------------------');
    console.log(configToken);

    if (user.avatar) {
      await this.updateImageUser(result.data.id, user.avatar);
    }
  }
}
