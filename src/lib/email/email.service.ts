import { Injectable } from '@nestjs/common';
import { ApolloError } from 'apollo-server-express';
import { EMAIL_ERROR } from './email.messages';
import { UserProfile } from 'src/users/users.graphql';
// eslint-disable-next-line @typescript-eslint/no-var-requires
const SibApiV3Sdk = require('sib-api-v3-sdk');

@Injectable()
export class EmailService {
  async getApiKey(): Promise<any> {
    const defaultClient = SibApiV3Sdk.ApiClient.instance;
    const apiKey = defaultClient.authentications['api-key'];

    apiKey.apiKey =
      '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';
  }

  async sendEmail(): Promise<void> {
    await this.getApiKey();
    const apiInstance = new SibApiV3Sdk.TransactionalEmailsApi();

    const sendSmtpEmail = new SibApiV3Sdk.SendSmtpEmail();

    sendSmtpEmail.subject = 'My {{params.subject}}';
    sendSmtpEmail.htmlContent =
      '<html><body><h1>Probando {{params.parameter}}</h1></body></html>';
    sendSmtpEmail.sender = {
      name: 'Scoby Social',
      email: 'system@scoby.social',
    };
    sendSmtpEmail.to = [
      { email: 'ajosue998@gmail.com', name: 'Ali Colmenares' },
    ];
    sendSmtpEmail.cc = [
      { email: 'ajosue998@gmail.com', name: 'Ali Colmenares' },
    ];
    sendSmtpEmail.replyTo = {
      email: 'system@scoby.social',
      name: 'Scoby Social',
    };
    sendSmtpEmail.headers = { 'Some-Custom-Name': 'unique-id-1234' };
    sendSmtpEmail.params = {
      parameter: 'Lorem Impsu',
      subject: 'Holiwis',
    };

    try {
      await apiInstance.sendTransacEmail(sendSmtpEmail);
    } catch (e) {
      throw new ApolloError(
        EMAIL_ERROR.MESSAGE_SEND_FAILED.MESSAGE,
        EMAIL_ERROR.MESSAGE_SEND_FAILED.CODE,
      );
    }
  }

  async sendReport(
    sourceUser: UserProfile,
    targetUser: UserProfile,
  ): Promise<void> {
    await this.getApiKey();
    const apiInstance = new SibApiV3Sdk.TransactionalEmailsApi();

    const sendSmtpEmail = new SibApiV3Sdk.SendSmtpEmail();

    sendSmtpEmail.subject = 'Report - {{params.subject}}';
    sendSmtpEmail.htmlContent = `<html><body><h1> from ${sourceUser.username} to ${targetUser.username}</h1>
      <p>id: ${targetUser.id}</p>
      <p>name: ${targetUser.fullName}</p>
      <p>phone: ${targetUser.phone}</p>    
      <p>username: ${targetUser.username}</p>
      <p>wallet: ${targetUser.publicKey}</p></body></html>`;
    sendSmtpEmail.sender = {
      name: 'Scoby Social',
      email: 'system@scoby.social',
    };
    sendSmtpEmail.to = [
      { email: 'ajosue998@gmail.com', name: 'Ali Colmenares' },
    ];
    sendSmtpEmail.cc = [
      { email: 'ajosue998@gmail.com', name: 'Ali Colmenares' },
    ];
    sendSmtpEmail.replyTo = {
      email: 'system@scoby.social',
      name: 'Scoby Social',
    };
    sendSmtpEmail.headers = { 'Some-Custom-Name': 'unique-id-1234' };
    sendSmtpEmail.params = {
      parameter: `${sourceUser.username}`,
      subject: `${targetUser.username}`,
    };

    try {
      await apiInstance.sendTransacEmail(sendSmtpEmail);
    } catch (e) {
      throw new ApolloError(
        EMAIL_ERROR.MESSAGE_SEND_FAILED.MESSAGE,
        EMAIL_ERROR.MESSAGE_SEND_FAILED.CODE,
      );
    }
  }
}
