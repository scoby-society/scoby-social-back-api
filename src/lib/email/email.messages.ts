export const EMAIL_ERROR = {
  MESSAGE_SEND_FAILED: {
    CODE: 'ERR_EMAIL_SEND_FAILED',
    MESSAGE: 'Failed to send email message',
  },
};
