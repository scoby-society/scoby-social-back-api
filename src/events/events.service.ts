import { Injectable } from '@nestjs/common';
import { getConnection, getRepository } from 'typeorm';
import { EventCreation, EventsObject, EventUpdate } from './events.graphql';
import { Event } from './events.entity';
import { ActivityServices } from '../activity/activity.service';
import { ActivityActionTypes } from 'src/activity/activity.types';
import { FileUpload } from '../lib/common/common.interfaces';
import { S3Service } from '../lib/s3/s3.service';
import { EVENT_ERRORS } from './event.messages';
import { USERS_ERRORS } from 'src/users/users.messages';
import { ApolloError } from 'apollo-server-express';
import { NotificationsService } from '../notifications/notifications.service';
import {
  ImageProcessorService,
  ImageTargetType,
} from '../lib/image-processor/image-processor.service';
import { User } from 'src/users/user.entity';
import { SessionsService } from 'src/sessions/sessions.service';
import { Session } from 'src/sessions/session.entity';
import { SessionJoinObject } from 'src/sessions/sessions.graphql';
import { Topic } from '../topics/topic.entity';
import { ExperienceEventType } from './event.types';
import { SporeDs } from 'src/spore-nft/spore-ds.entity';
import {
  ExperienceType,
  visibilityExperience,
} from 'src/sessions/session.types';
import { SporeService } from 'src/spore-nft/spore.service';
import { Projects } from 'src/projects/projects.entity';
@Injectable()
export class EventsService {
  constructor(
    private activityServices: ActivityServices,
    private s3Service: S3Service,
    private imageProcessorService: ImageProcessorService,
    private notificationsService: NotificationsService,
    private sessionsService: SessionsService,
    private sporeService: SporeService,
  ) {}

  async createEvent(
    currentUserId: number,
    event: EventCreation,
    avatar?: FileUpload,
    backgroundImage?: FileUpload,
  ): Promise<EventsObject> {
    const repository = getRepository(Event);
    const {
      title,
      description,
      day,
      start,
      end,
      invitedUsers,
      isTeamsMembersInvited,
      teamInvitation,
      nft,
      nftsRequired,
      nftsInviteUser,
      eventType,
      projectId,
      visibility,
    } = event;

    const nftObject = nft ? await getRepository(SporeDs).findOne(nft) : null;
    if (nftsRequired) {
      await this.sporeService.verifyCollectionNfts(nftsRequired);
    }
    const project = await getRepository(Projects).findOne({
      where: { id: projectId },
    });

    const { userIds } = await this.sessionsService.getInvitedUsers(
      invitedUsers,
      currentUserId,
      isTeamsMembersInvited,
      teamInvitation,
      nftsInviteUser
        ? ((await this.sporeService.idUserCollectionNftByIds(
            nftsInviteUser,
          )) as [number])
        : null,
    );
    const topicsCollection = event.topics.map((topicId) => ({ id: topicId }));
    const savedEvent = await repository.save({
      title,
      description,
      day,
      start,
      end,
      topics: topicsCollection,
      eventType: this.getEventType(project?.join || ExperienceEventType.OPEN),
      visibility: this.getVisibility(project?.project || visibilityExperience.LISTED),
      ownerUser: {
        id: currentUserId,
      },
      nft: nftObject,
      nftsRequired,
      projects: project,
    });

    this.activityServices.sendGroupNotificationActivity(
      userIds,
      savedEvent.id,
      currentUserId,
      ActivityActionTypes.CREATE_EVENT,
    );

    this.notificationsService.sendEventCreatedNotifications(
      savedEvent.id,
      currentUserId,
      userIds as [number],
    );

    await this.uploadFileEvent(
      currentUserId,
      savedEvent.id,
      avatar,
      backgroundImage,
    );

    return (await repository.findOne(savedEvent.id, {
      relations: ['ownerUser', 'projects'],
    })) as Event;
  }

  async uploadFileEvent(
    currentUserId: number,
    idEvent: number,
    avatar?: FileUpload,
    backgroundImage?: FileUpload,
  ): Promise<EventsObject> {
    const repository = getRepository(Event);
    const event = (await repository.findOne(idEvent, {
      relations: ['ownerUser'],
    })) as Event;

    if (!event) {
      throw new ApolloError(
        EVENT_ERRORS.EVENT_NOT_FOUND.MESSAGE,
        EVENT_ERRORS.EVENT_NOT_FOUND.CODE,
      );
    }

    if (event.ownerUser.id !== currentUserId) {
      throw new ApolloError(
        EVENT_ERRORS.NOT_EVENT_OWNER.MESSAGE,
        EVENT_ERRORS.NOT_EVENT_OWNER.CODE,
      );
    }

    const removeAssets = [];

    if (avatar && event.avatar) {
      removeAssets.push(event.avatar);
    }

    if (backgroundImage && event.backgroundImage) {
      removeAssets.push(event.backgroundImage);
    }

    if (removeAssets.length) {
      await this.s3Service.removeFiles(removeAssets);
    }

    let avatarUpload;
    let backgroundImageUpload;

    if (avatar) {
      avatarUpload = this.imageProcessorService.optimizeImage(
        avatar.createReadStream(),
        ImageTargetType.AVATAR,
      );
    }

    if (backgroundImage) {
      backgroundImageUpload = this.imageProcessorService.optimizeImage(
        backgroundImage.createReadStream(),
        ImageTargetType.BACKGROUND_IMAGE,
      );
    }

    const [avatarResult, backgroundImageResult] = await Promise.all([
      avatarUpload
        ? this.s3Service.uploadFile({
            extension: avatarUpload.extension,
            mime: avatarUpload.mime,
            stream: avatarUpload.stream,
          })
        : undefined,
      backgroundImageUpload
        ? this.s3Service.uploadFile({
            extension: backgroundImageUpload.extension,
            mime: backgroundImageUpload.mime,
            stream: backgroundImageUpload.stream,
          })
        : undefined,
    ]);

    await repository.save({
      id: idEvent,
      avatar: avatarResult?.Key,
      backgroundImage: backgroundImageResult?.Key,
    });

    return (await repository.findOne(idEvent)) as Event;
  }

  async getUserEvents(
    ownerId: number,
    userId?: number,
  ): Promise<EventsObject[]> {
    const id = userId ? userId : ownerId;
    return await getRepository(Event)
      .createQueryBuilder('event')
      .leftJoinAndSelect('event.session', 'session')
      .where('event.ownerUser = :id', { id })
      .andWhere('event.finishedAt is null')
      .orderBy('event.createdAt', 'DESC')
      .getMany();
  }

  async invitedUsersEvent(
    currentUserId: number,
    invitedUsers: [number],
    idEvent: number,
    teamInvitation?: [number] | null,
    isTeamsMembersInvited?: boolean,
  ): Promise<EventsObject> {
    const repository = getRepository(Event);
    const event = (await repository.findOne(idEvent, {
      relations: ['ownerUser'],
    })) as Event;

    if (!event) {
      throw new ApolloError(
        EVENT_ERRORS.EVENT_NOT_FOUND.MESSAGE,
        EVENT_ERRORS.EVENT_NOT_FOUND.CODE,
      );
    }

    const { userIds } = await this.sessionsService.getInvitedUsers(
      invitedUsers ?? [],
      currentUserId,
      isTeamsMembersInvited,
      teamInvitation,
    );

    this.activityServices.sendGroupNotificationActivity(
      userIds,
      event.id,
      currentUserId,
      ActivityActionTypes.CREATE_EVENT,
    );

    this.notificationsService.sendEventCreatedNotifications(
      event.id,
      currentUserId,
      userIds as [number],
    );

    return event;
  }

  async joinEvent(
    currentUserId: number,
    idEvent: number,
  ): Promise<EventsObject> {
    const repository = getRepository(Event);
    const userRepository = getRepository(User);
    const event = await repository.findOne(idEvent, {
      relations: ['ownerUser', 'suscribeUsers'],
    });
    const userToAdd = await userRepository.findOne(currentUserId);

    if (!event) {
      throw new ApolloError(
        EVENT_ERRORS.EVENT_NOT_FOUND.MESSAGE,
        EVENT_ERRORS.EVENT_NOT_FOUND.CODE,
      );
    }

    if (!userToAdd) {
      throw new ApolloError(
        USERS_ERRORS.USER_NOT_FOUND.MESSAGE,
        USERS_ERRORS.USER_NOT_FOUND.CODE,
      );
    }

    if (event.finishedAt) {
      throw new ApolloError(
        EVENT_ERRORS.EVENT_FINISHED.MESSAGE,
        EVENT_ERRORS.EVENT_FINISHED.CODE,
      );
    }
    const verifyNft = await this.verifyUserNft(event, currentUserId);
    const idPrivateExperiences = await this.getPrivateExperiences(
      currentUserId,
    );
    if (
      !idPrivateExperiences.find((element) => element === idEvent) &&
      event.eventType === ExperienceEventType.CLOSED &&
      !verifyNft
    ) {
      throw new ApolloError(
        EVENT_ERRORS.EVENT_IS_PRIVATE.MESSAGE,
        EVENT_ERRORS.EVENT_IS_PRIVATE.CODE,
      );
    }

    const suscribeUsers = event.suscribeUsers || [];
    suscribeUsers.push(userToAdd as User);

    await repository.save({ id: idEvent, suscribeUsers });
    return (await repository.findOne(idEvent, {
      relations: ['ownerUser'],
    })) as Event;
  }

  async verifyUserNft(event: Event, idUser: number): Promise<boolean> {
    if (
      event.nftsRequired &&
      event.nftsRequired.length > 0 &&
      event.eventType !== ExperienceEventType.OPEN
    ) {
      const ids = [];
      for (const idNft of event.nftsRequired) {
        const nft = await this.sporeService.getSporeDsById(idNft);

        ids.push(
          ...(await this.sporeService.getNftByUser(
            idUser,
            nft.symbol || 'undefined',
          )),
        );
      }

      if (ids.length <= 0) {
        throw new ApolloError(
          EVENT_ERRORS.EVENT_REQUIRED_NFT.MESSAGE,
          EVENT_ERRORS.EVENT_REQUIRED_NFT.CODE,
        );
      }
      return true;
    }
    return false;
  }

  async leaveEvent(
    currentUserId: number,
    idEvent: number,
  ): Promise<EventsObject> {
    const repository = getRepository(Event);
    const event = await repository.findOne(idEvent, {
      relations: ['ownerUser', 'suscribeUsers'],
    });
    if (!event) {
      throw new ApolloError(
        EVENT_ERRORS.EVENT_NOT_FOUND.MESSAGE,
        EVENT_ERRORS.EVENT_NOT_FOUND.CODE,
      );
    }
    const suscribeUsers = event.suscribeUsers.filter(
      (user) => user.id !== currentUserId,
    );
    await repository.save({ id: idEvent, suscribeUsers });

    return (await repository.findOne(idEvent)) as Event;
  }

  async liveEvent(
    currentUser: number,
    idEvent: number,
  ): Promise<SessionJoinObject> {
    const repository = getRepository(Event);
    const event = await repository.findOne(idEvent, {
      relations: ['ownerUser', 'suscribeUsers', 'session'],
    });
    const sessionRepository = getRepository(Session);

    if (!event) {
      throw new ApolloError(
        EVENT_ERRORS.EVENT_NOT_FOUND.MESSAGE,
        EVENT_ERRORS.EVENT_NOT_FOUND.CODE,
      );
    }

    if (event.ownerUser.id !== currentUser) {
      throw new ApolloError(
        EVENT_ERRORS.NOT_EVENT_OWNER.MESSAGE,
        EVENT_ERRORS.NOT_EVENT_OWNER.CODE,
      );
    }

    if (event.session && !event.session.finishedAt) {
      throw new ApolloError(
        EVENT_ERRORS.EVENT_IS_LIVE.MESSAGE,
        EVENT_ERRORS.EVENT_IS_LIVE.CODE,
      );
    }

    const topics = event.topics.map((topicId) => topicId.id);
    const {
      vonageSessionToken,
      vonageUserToken,
      token,
      vonageApiToken,
      session,
    } = await this.sessionsService.createSession(
      currentUser,
      [topics[0]],
      ExperienceType.OPEN,
      event.title,
      event.description,
    );

    const sessionSave = (await sessionRepository.findOne(
      session.id,
    )) as Session;

    await repository.save({
      id: idEvent,
      session: sessionSave,
    });

    return {
      vonageSessionToken,
      vonageUserToken,
      token,
      vonageApiToken,
      session: {
        ...sessionSave,
        viewers: this.sessionsService.countViewers(sessionSave),
      },
    };
  }

  async getEventById(id: number, idUser: number): Promise<EventsObject> {
    const experienceSecrets = await this.getSecretExperiences(idUser);
    const repository = getRepository(Event);
    const event = (await repository.findOne(id, {
      relations: ['session', 'nft'],
    })) as Event;

    if (!event) {
      throw new ApolloError(
        EVENT_ERRORS.EVENT_NOT_FOUND.MESSAGE,
        EVENT_ERRORS.EVENT_NOT_FOUND.CODE,
      );
    }
    return {
      ...event,
      isAvailable: await this.isPrivateAvailable(
        experienceSecrets,
        event,
        idUser,
      ),
    };
  }

  async editEvent(
    currentUserId: number,
    idEvent: number,
    event: EventUpdate,
    avatar?: FileUpload,
    backgroundImage?: FileUpload,
  ): Promise<EventsObject> {
    const repository = getRepository(Event);
    const eventRepository = await repository.findOne(idEvent, {
      relations: ['ownerUser'],
    });
    if (!eventRepository) {
      throw new ApolloError(
        EVENT_ERRORS.EVENT_NOT_FOUND.MESSAGE,
        EVENT_ERRORS.EVENT_NOT_FOUND.CODE,
      );
    }

    if (eventRepository.ownerUser.id !== currentUserId) {
      throw new ApolloError(
        EVENT_ERRORS.NOT_EVENT_OWNER.MESSAGE,
        EVENT_ERRORS.NOT_EVENT_OWNER.CODE,
      );
    }

    if (eventRepository.finishedAt) {
      throw new ApolloError(
        EVENT_ERRORS.EVENT_FINISHED.MESSAGE,
        EVENT_ERRORS.EVENT_FINISHED.CODE,
      );
    }
    await this.uploadFileEvent(
      currentUserId,
      eventRepository.id,
      avatar,
      backgroundImage,
    );

    const topicsCollection = event.topics.map((topicId) => ({ id: topicId }));
    return await repository.save({
      id: idEvent,
      title: event.title,
      description: event.description,
      day: event.day || eventRepository.day,
      start: event.start || eventRepository.start,
      end: event.end || eventRepository.end,
      topics: topicsCollection,
    });
  }

  async endEvent(
    currentUserId: number,
    idEvent: number,
  ): Promise<EventsObject> {
    const repository = getRepository(Event);
    const event = await repository.findOne(idEvent, {
      relations: ['ownerUser', 'session'],
    });
    if (!event) {
      throw new ApolloError(
        EVENT_ERRORS.EVENT_NOT_FOUND.MESSAGE,
        EVENT_ERRORS.EVENT_NOT_FOUND.CODE,
      );
    }

    if (event.ownerUser.id !== currentUserId) {
      throw new ApolloError(
        EVENT_ERRORS.NOT_EVENT_OWNER.MESSAGE,
        EVENT_ERRORS.NOT_EVENT_OWNER.CODE,
      );
    }

    if (event.finishedAt) {
      throw new ApolloError(
        EVENT_ERRORS.EVENT_FINISHED.MESSAGE,
        EVENT_ERRORS.EVENT_FINISHED.CODE,
      );
    }

    if (event.session) {
      this.sessionsService.endSession(currentUserId, event.session.id);
    }
    const finishedAt = new Date(Date.now());
    return await repository.save({ id: idEvent, finishedAt });
  }

  async getLiveEvents(idUser: number): Promise<EventsObject[]> {
    const experienceSecrets = await this.getSecretExperiences(idUser);
    const events = await getRepository(Event)
      .createQueryBuilder('event')
      .leftJoinAndSelect('event.session', 'session')
      .where('event.finishedAt is null')
      .andWhere('event.session is not null')
      .andWhere('session.finishedAt is null')
      .orderBy('event.createdAt', 'DESC')
      .getMany();

    return await Promise.all(
      events.map(async (session) => {
        const isAvailable = await this.isPrivateAvailable(
          experienceSecrets,
          session,
          idUser,
        );

        return {
          ...session,
          isAvailable,
        };
      }),
    );
  }

  async getEventsPaging(
    limit: number,
    page: number,
    idUser: number,
  ): Promise<EventsObject[]> {
    const blockUserId = await this.sessionsService.getUserBlockedUser(idUser);
    await this.closeExpiredEvents();
    await this.endEvents();
    const experienceSecrets = await this.getSecretExperiences(idUser);
    const events = await getRepository(Event)
      .createQueryBuilder('event')
      .leftJoinAndSelect('event.session', 'session')
      .leftJoinAndSelect('event.suscribeUsers', 'suscribeUsers')
      .leftJoinAndSelect('event.ownerUser', 'ownerUser')
      .leftJoinAndSelect('event.projects', 'projects')
      .where('event.finishedAt is null')
      .andWhere(
        'session.finishedAt is not null OR event.session is null AND event.finishedAt is null',
      )
      .andWhere(
        'event.visibility != :eventType AND ownerUser.id NOT IN(:...block) OR event.id IN (:...ids) AND event.session is null AND ownerUser.id NOT IN(:...block)',
        {
          eventType: visibilityExperience.UNLISTED,
          ids: experienceSecrets.length > 0 ? experienceSecrets : [undefined],
          block: blockUserId.length > 0 ? blockUserId : [0],
        },
      )

      .skip(limit * (page - 1))
      .take(limit)
      .orderBy('event.day', 'ASC')
      .getMany();

    return await Promise.all(
      events.map(async (session) => {
        const isAvailable = await this.isPrivateAvailable(
          experienceSecrets,
          session,
          idUser,
        );

        return {
          ...session,
          isAvailable,
        };
      }),
    );
  }

  async getSecretExperiences(idUser: number): Promise<number[]> {
    const experiences = await getRepository(Event)
      .createQueryBuilder('event')
      .leftJoinAndSelect('event.session', 'session')
      .where('event.finishedAt is null')
      .andWhere(
        'session.finishedAt is not null OR event.session is null AND event.finishedAt is null',
      )
      .getMany();

    const idExperiences = [];
    for (const event of experiences) {
      idExperiences.push(event.id);
    }
    return await this.sessionsService.getInvitationsExperiences(
      idExperiences,
      idUser,
      'event',
    );
  }

  async isPrivateAvailable(
    experienceSecrets: number[],
    event: Event,
    idUser: number,
  ): Promise<boolean> {
    if (event.eventType === ExperienceEventType.CLOSED) {
      const isAvailable = experienceSecrets.find(
        (element) => element === event.id,
      )
        ? true
        : false;
      if (isAvailable) {
        return true;
      }

      return await this.IsUserHasCollection(idUser, event);
    }

    if (event.eventType === ExperienceEventType.EXCLUSIVE) {
      return await this.IsUserHasCollection(idUser, event);
    }

    return true;
  }

  async IsUserHasCollection(idUser: number, event: Event): Promise<boolean> {
    if (event.projects) {
      const project = await getRepository(Projects).findOne(event.projects.id, {
        relations: ['nft'],
      });
      if (!project || !project.nft) {
        return true;
      }
      let isNftRequired = true;
      const idNftsRequired = project.nft.map((nft) => nft.id);

      const nameCollections = await this.sporeService.getNameCollectionNfts(
        idNftsRequired,
      );

      const idUserNft: number[] = [];
      for (const collection of nameCollections) {
        idUserNft.push(
          ...(await this.sporeService.idUserCollectionNft(collection || '')),
        );
      }

      isNftRequired = idUserNft.find((element) => element === idUser)
        ? true
        : false;

      if (!isNftRequired) {
        return false;
      } else {
        return true;
      }
    }
    return true;
  }

  async getPrivateExperiences(idUser: number): Promise<number[]> {
    const experiences = await getRepository(Event)
      .createQueryBuilder('event')
      .leftJoinAndSelect('event.session', 'session')
      .where('event.finishedAt is null')
      .andWhere(
        'session.finishedAt is not null OR event.session is null AND event.finishedAt is null',
      )
      .andWhere('event.eventType = :eventType', {
        eventType: ExperienceEventType.CLOSED,
      })
      .getMany();

    const idExperiences = [];
    for (const event of experiences) {
      idExperiences.push(event.id);
    }

    return await this.sessionsService.getInvitationsExperiences(
      idExperiences,
      idUser,
      'event',
    );
  }

  async endEvents(): Promise<void> {
    const repository = getRepository(Event);
    const events = await getRepository(Event)
      .createQueryBuilder('event')
      .leftJoinAndSelect('event.session', 'session')
      .where('event.finishedAt is null')
      .andWhere('event.session is not null AND session.finishedAt is not null')
      .getMany();

    events.map((event) => {
      repository.save({
        id: event.id,
        session: null,
      });
    });
  }

  async closeExpiredEvents(): Promise<void> {
    const dataNow = new Date(Date.now());
    const events = await getRepository(Event)
      .createQueryBuilder('event')
      .leftJoinAndSelect('event.session', 'session')
      .where('event.finishedAt is null')
      .andWhere(
        'session.finishedAt is not null OR event.session is null AND event.finishedAt is null',
      )
      .andWhere('event.day < :dataNow', { dataNow })
      .getMany();

    events.map(async (event) => {
      await getRepository(Event).save({
        id: event.id,
        finishedAt: dataNow,
      });
    });
  }

  async getLiveEventsPaging(
    limit: number,
    page: number,
    idUser: number,
  ): Promise<EventsObject[]> {
    const blockUserId = await this.sessionsService.getUserBlockedUser(idUser);
    const experienceSecrets = await this.getSecretExperiences(idUser);
    const idLiveExperience = await this.getLiveExperienceSecrets(
      experienceSecrets,
    );
    const events = await getRepository(Event)
      .createQueryBuilder('event')
      .leftJoinAndSelect('event.session', 'session')
      .leftJoinAndSelect('event.ownerUser', 'ownerUser')
      .leftJoinAndSelect('event.projects', 'projects')
      .where('event.finishedAt is null')
      .andWhere('event.session is not null')
      .andWhere('session.finishedAt is null')
      .andWhere(
        'event.visibility != :eventType AND ownerUser.id NOT IN(:...block) OR event.id IN (:...ids) AND ownerUser.id NOT IN(:...block)',
        {
          eventType: visibilityExperience.UNLISTED,
          ids: idLiveExperience.length > 0 ? idLiveExperience : [undefined],
          block: blockUserId.length > 0 ? blockUserId : [0],
        },
      )
      .orderBy('event.day', 'ASC')
      .skip(limit * (page - 1))
      .take(limit)
      .getMany();

    return await Promise.all(
      events.map(async (session) => {
        const isAvailable = await this.isPrivateAvailable(
          experienceSecrets,
          session,
          idUser,
        );

        return {
          ...session,
          isAvailable,
        };
      }),
    );
  }

  async getLiveExperienceSecrets(ids: number[]): Promise<number[]> {
    const series = await getRepository(Event)
      .createQueryBuilder('event')
      .leftJoinAndSelect('event.session', 'session')
      .where('event.finishedAt is null')
      .andWhere('event.session is not null')
      .andWhere('event.finishedAt is null')
      .getMany();

    const idEvents: number[] = [];
    series.map((event) => idEvents.push(event.id));

    const eventLives = ids.map((element) => {
      return idEvents.find((x) => x === element) ? element : 0;
    });

    const noNull = eventLives.filter((element) => element !== 0);
    return noNull;
  }

  async getsuscribeUsers(id: number): Promise<User[]> {
    return getConnection()
      .createQueryBuilder()
      .relation(Event, 'suscribeUsers')
      .of(id)
      .loadMany();
  }

  async getUserTopics(idEvent: number): Promise<Topic[]> {
    return getConnection()
      .createQueryBuilder()
      .relation(Event, 'topics')
      .of(idEvent)
      .loadMany();
  }

  async getEventOwner(id: number): Promise<User> {
    return getConnection()
      .createQueryBuilder()
      .relation(Event, 'ownerUser')
      .of(id)
      .loadOne() as Promise<User>;
  }

  async getViewers(idEvent: number): Promise<number> {
    const repository = getRepository(Event);
    const repositorySession = getRepository(Session);
    const event = await repository.findOne(idEvent, { relations: ['session'] });
    const session =
      event && event.session
        ? await repositorySession.findOne(event.session.id, {
            relations: ['viewerUsers', 'greenRoomUsers'],
          })
        : 0;
    return session ? this.sessionsService.countViewers(session) : 0;
  }

  async subscribed(idEvent: number, currentUserId: number): Promise<boolean> {
    const repository = getRepository(Event);
    const { suscribeUsers } = (await repository.findOne(idEvent, {
      relations: ['suscribeUsers'],
    })) as Event;
    return [...suscribeUsers].some((User) => User.id === currentUserId);
  }

  getEventType(type: string): ExperienceEventType {
    this.sessionsService.verifyTypeExperience(type);
    if (type === 'Open') {
      return ExperienceEventType.OPEN;
    } else if (type === 'Closed') {
      return ExperienceEventType.CLOSED;
    } else {
      return ExperienceEventType.EXCLUSIVE;
    }
  }

  async deleteEvent(eventId: number): Promise<EventsObject> {
    const repository = getRepository(Event);
    const event = await repository.findOne(eventId, {
      relations: ['ownerUser', 'suscribeUsers', 'session'],
    });

    if (!event) {
      throw new ApolloError(
        EVENT_ERRORS.EVENT_NOT_FOUND.MESSAGE,
        EVENT_ERRORS.EVENT_NOT_FOUND.CODE,
      );
    }

    if (event.finishedAt) {
      throw new ApolloError(
        EVENT_ERRORS.EVENT_FINISHED.MESSAGE,
        EVENT_ERRORS.EVENT_FINISHED.CODE,
      );
    }

    if (event.session) {
      this.sessionsService.endSession(event.ownerUser.id, event.session.id);
    }

    const finishedAt = new Date(Date.now());
    await repository.save({ id: event.id, finishedAt });
    const savedEvent = await repository.findOne(event.id, {
      relations: ['ownerUser'],
    });

    if (!savedEvent) {
      throw new ApolloError(
        EVENT_ERRORS.EVENT_NOT_FOUND.MESSAGE,
        EVENT_ERRORS.EVENT_NOT_FOUND.CODE,
      );
    }

    return savedEvent;
  }

  getVisibility(type: string): visibilityExperience {
    if (type === 'Listed') {
      return visibilityExperience.LISTED;
    } else {
      return visibilityExperience.UNLISTED;
    }
  }
}
