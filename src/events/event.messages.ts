export const EVENT_ERRORS = {
  EVENT_NOT_FOUND: {
    CODE: 'ERR_EVENT_NOT_FOUND',
    MESSAGE: 'Event not found',
  },
  EVENT_FINISHED: {
    CODE: 'ERR_EVENT_FINISHED',
    MESSAGE: 'Event has been finished',
  },
  NOT_EVENT_OWNER: {
    CODE: 'ERR_NOT_EVENT_OWNER',
    MESSAGE: 'User is not owner of the event',
  },
  EVENT_IS_LIVE: {
    CODE: 'ERR_EVENT_IS_LIVE',
    MESSAGE: 'The event is now live ',
  },
  EVENT_IS_PRIVATE: {
    CODE: 'ERR_EVENT_IS_PRIVATE',
    MESSAGE: 'The user does not have an invitation to this event',
  },
  EVENT_REQUIRED_NFT: {
    CODE: 'ERR_REQUIRED_NFT',
    MESSAGE: 'The user does not have the required nft',
  },
  EVENT_NOT_FOUND_PROJECT: {
    CODE: 'ERR_EVENT_NOT_FOUND_PROJECT',
    MESSAGE: 'The project not found'
  }
};
