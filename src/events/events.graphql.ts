import { ObjectType, Int, Field, InputType } from '@nestjs/graphql';
import { UserProfile } from '../users/users.graphql';
import { SessionLiveToSerie } from 'src/sessions/sessions.graphql';
import { Topic } from '../topics/topics.graphql';
import { SporeUsersDs } from 'src/spore-nft/spore.graphql';
import { ProjectView } from 'src/projects/projects.graphql';
import { Projects } from 'src/projects/projects.entity';
@ObjectType()
export class EventsObject {
  @Field(() => Int)
  id: number;

  @Field(() => String)
  title: string;

  @Field(() => String)
  description: string;

  @Field()
  ownerUser: UserProfile;

  @Field(() => String, { nullable: true })
  avatar?: string | null;

  @Field(() => String, { nullable: true })
  backgroundImage?: string | null;

  @Field(() => [UserProfile])
  suscribeUsers: UserProfile[];

  @Field(() => SessionLiveToSerie, { nullable: true })
  session?: SessionLiveToSerie | null;

  @Field(() => [Topic], { nullable: true })
  topics: Topic[];

  @Field()
  createdAt: Date;

  @Field()
  updatedAt: Date;

  @Field(() => String)
  start: Date;

  @Field(() => String)
  end: Date;

  @Field(() => String)
  day: Date;

  @Field(() => Date, { nullable: true })
  finishedAt?: Date | null;

  @Field(() => Boolean)
  subscribed?: boolean | null;

  @Field(() => Number)
  viewers?: number | null;

  @Field(() => String)
  eventType: string;

  @Field(() => SporeUsersDs, { nullable: true })
  nft?: SporeUsersDs | null;

  @Field(() => Boolean, { nullable: true })
  isAvailable?: boolean | true;

  @Field(() => [Number], { nullable: true })
  nftsRequired?: number[] | null;

  @Field(() => ProjectView, { nullable:true })
  projects?: Projects | null;
}

@InputType()
export class EventCreation {
  @Field(() => String)
  title: string;

  @Field(() => String)
  description: string;

  @Field(() => [Int])
  topics: number[];

  @Field(() => [Int], { nullable: true })
  invitedUsers: [number];

  @Field(() => String)
  day: string;

  @Field(() => String)
  start: string;

  @Field(() => String)
  end: string;

  @Field(() => Boolean, { nullable: true })
  isTeamsMembersInvited: boolean;

  @Field(() => [Int], { nullable: true })
  teamInvitation: [number];

  @Field(() => String,{ nullable: true })
  eventType: string | null;

  @Field(() => Int, { nullable: true })
  nft?: number | null;

  @Field(() => [Int], { nullable: true })
  nftsRequired?: number[] | null;

  @Field(() => [Int], { nullable: true })
  nftsInviteUser?: number[] | null;

  @Field(() => Int, {nullable:true})
  projectId: number | null;

  @Field(() => String, {nullable:true})
  visibility: string | null;
}

@InputType()
export class EventUpdate {
  @Field(() => String)
  title: string;

  @Field(() => String)
  description: string;

  @Field(() => [Int])
  topics: number[];

  @Field(() => String, { nullable: true })
  day?: string | null;

  @Field(() => String, { nullable: true })
  start?: string | null;

  @Field(() => String, { nullable: true })
  end?: string | null;
}
