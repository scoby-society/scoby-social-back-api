export enum ExperienceEventType {
  OPEN = 'Open',
  CLOSED = 'Closed',
  EXCLUSIVE = 'Exclusive',
}
