export const CHAT_ERRORS = {
  CHAT_NOT_FOUND: {
    CODE: 'ERR_CHAT_NOT_FOUND',
    MESSAGE: 'Chat not found',
  },
  CHAT_FINISHED: {
    CODE: 'ERR_CHAT_FINISHED',
    MESSAGE: 'Chat has been finished',
  },
  NOT_CHAT_OWNER: {
    CODE: 'ERR_NOT_CHAT_OWNER',
    MESSAGE: 'User is not owner of the chat',
  },
  CHAT_IS_LIVE: {
    CODE: 'ERR_CHAT_IS_LIVE',
    MESSAGE: 'The chat is now live ',
  },
  CHAT_IS_PRIVATE: {
    CODE: 'ERR_CHAT_IS_PRIVATE',
    MESSAGE: 'The user does not have an invitation to this chat',
  },
  CHAT_REQUIRED_NFT: {
    CODE: 'ERR_REQUIRED_NFT',
    MESSAGE: 'The user does not have the required nft',
  },
};
