import { UseGuards, UsePipes } from '@nestjs/common';
import {
  Mutation,
  Resolver,
  Args,
  Query,
  ResolveField,
  Parent,
  Int,
} from '@nestjs/graphql';
import { JwtGuard } from 'src/lib/jwt/jwt.guard';
import { CurrentUser } from '../lib/common/current-user.decorator';
import { BasePayload } from '../lib/jwt/jwt.service';
import { GraphQLUpload } from 'apollo-server-express';
import { FileUpload, Paging } from '../lib/common/common.interfaces';
import {
  ChatExperienceObject,
  ChatExperienceUpdate,
  CreateChatExperience,
} from './chatExperience.graphql';
import { ChatExperienceService } from './chatExperience.service';
import { PagingPipe } from '../lib/common/common.pipe';
import { PagingInput } from '../lib/common/common.graphql';
import { UserProfile } from '../users/users.graphql';
import { Topic } from '../topics/topic.entity';

@Resolver(() => ChatExperienceObject)
export class ChatExperienceResolver {
  constructor(private chatExperienceService: ChatExperienceService) {}

  @Mutation(() => ChatExperienceObject)
  @UseGuards(JwtGuard)
  async createChat(
    @CurrentUser() currentUser: BasePayload,
    @Args('chat') chat: CreateChatExperience,
    @Args('avatar', { nullable: true, type: () => GraphQLUpload })
    avatar?: FileUpload,
    @Args('backgroundImage', { nullable: true, type: () => GraphQLUpload })
    backgroundImage?: FileUpload,
  ): Promise<ChatExperienceObject> {
    return this.chatExperienceService.createChat(
      currentUser.id,
      chat,
      avatar,
      backgroundImage,
    );
  }

  @Mutation(() => ChatExperienceObject)
  @UseGuards(JwtGuard)
  async editChat(
    @CurrentUser() currentUser: BasePayload,
    @Args('id') idChat: number,
    @Args('chat') chat: ChatExperienceUpdate,
    @Args('avatar', { nullable: true, type: () => GraphQLUpload })
    avatar?: FileUpload,
    @Args('backgroundImage', { nullable: true, type: () => GraphQLUpload })
    backgroundImage?: FileUpload,
  ): Promise<ChatExperienceObject> {
    return this.chatExperienceService.editChatExperience(
      currentUser.id,
      idChat,
      chat,
      avatar,
      backgroundImage,
    );
  }

  @Query(() => ChatExperienceObject)
  @UseGuards(JwtGuard)
  async getChatById(@Args('id') idChat: number): Promise<ChatExperienceObject> {
    return this.chatExperienceService.getChatById(idChat);
  }

  @Query(() => [ChatExperienceObject])
  @UseGuards(JwtGuard)
  @UsePipes(PagingPipe)
  async getChatPaging(
    @Args('paging', { type: () => PagingInput, nullable: true }) paging: Paging,
  ): Promise<ChatExperienceObject[]> {
    return this.chatExperienceService.getChatPaging(paging.limit, paging.page);
  }

  @Query(() => [ChatExperienceObject])
  @UseGuards(JwtGuard)
  @UsePipes(PagingPipe)
  async getAllChatExperience(): Promise<ChatExperienceObject[]> {
    return this.chatExperienceService.getAllChatExperience();
  }

  @Query(() => [ChatExperienceObject])
  @UseGuards(JwtGuard)
  async getChatExperienceUser(
    @CurrentUser() currentUser: BasePayload,
    @Args('id', { nullable: true }) idUser?: number,
  ): Promise<ChatExperienceObject[]> {
    return this.chatExperienceService.getChatExperienceUser(
      idUser ?? currentUser.id,
    );
  }

  @Mutation(() => ChatExperienceObject)
  @UseGuards(JwtGuard)
  async joinChatExperience(
    @CurrentUser() currentUser: BasePayload,
    @Args('id') idChat: number,
  ): Promise<ChatExperienceObject> {
    return this.chatExperienceService.joinChat(currentUser.id, idChat);
  }

  @Mutation(() => ChatExperienceObject)
  @UseGuards(JwtGuard)
  async leaveChatExperience(
    @CurrentUser() currentUser: BasePayload,
    @Args('id') idChat: number,
  ): Promise<ChatExperienceObject> {
    return this.chatExperienceService.leaveChat(currentUser.id, idChat);
  }

  @ResolveField('ownerUser', () => UserProfile)
  async ownerUser(
    @Parent() chatExperienceObject: ChatExperienceObject,
  ): Promise<UserProfile> {
    return this.chatExperienceService.getChatOwner(chatExperienceObject.id);
  }

  @Mutation(() => Boolean)
  @UseGuards(JwtGuard)
  async createUserChatKitty(): Promise<boolean> {
    await this.chatExperienceService.createUsersInChat();
    return true;
  }

  @Mutation(() => ChatExperienceObject)
  @UseGuards(JwtGuard)
  async endChatExperience(
    @CurrentUser() currentUser: BasePayload,
    @Args('idChat') id: number,
  ): Promise<ChatExperienceObject> {
    return this.chatExperienceService.endChat(currentUser.id, id);
  }

  @Mutation(() => Boolean)
  @UseGuards(JwtGuard)
  async updateAllUserKitty(@Args('page') page: number): Promise<boolean> {
    await this.chatExperienceService.updateAllUserKitty(page);
    return true;
  }

  @Query(() => String)
  @UseGuards(JwtGuard)
  async getTokenChatKitty(): Promise<string> {
    return this.chatExperienceService.getToken();
  }

  @ResolveField('subscribed', () => Boolean)
  async subscribed(
    @Parent() chatExperienceObject: ChatExperienceObject,
    @CurrentUser() currentUser: BasePayload,
  ): Promise<boolean> {
    return this.chatExperienceService.subscribed(
      chatExperienceObject.id,
      currentUser.id,
    );
  }

  @ResolveField('suscribeUsers', () => [UserProfile])
  async suscribeUsers(
    @Parent() chatObject: ChatExperienceObject,
  ): Promise<UserProfile[]> {
    return this.chatExperienceService.getsuscribeUsers(chatObject.id);
  }

  @ResolveField('topics', () => [Topic])
  async topics(@Parent() chatObject: ChatExperienceObject): Promise<Topic[]> {
    return this.chatExperienceService.getTopics(chatObject.id);
  }

  @Mutation(() => ChatExperienceObject)
  @UseGuards(JwtGuard)
  async sendNotificationChannel(
    @Args('idChannel') id: number,
    @Args('idUsers', { type: () => [Int] })
    idUsers: [number],
  ): Promise<ChatExperienceObject> {
    return this.chatExperienceService.sendNotificationChannel(
      id,
      idUsers,
    );
  }
}
