import { Module } from '@nestjs/common';
import { ChatExperienceResolver } from './chatExperience.resolver';
import { ChatExperienceService } from './chatExperience.service';
import { JwtModule } from '../lib/jwt/jwt.module';
import { NotificationsModule } from 'src/notifications/notifications.module';
import { ImageProcessorModule } from 'src/lib/image-processor/image-processor.module';
import { ActivityModule } from 'src/activity/activity.module';
import { S3Module } from 'src/lib/s3/s3.module';
import { SessionsModule } from 'src/sessions/sessions.module';
import { SporeModule } from 'src/spore-nft/spore.module';
import { ChatKittyModule } from 'src/lib/chatKitty/chatKitty.module';

@Module({
  imports: [
    JwtModule,
    NotificationsModule,
    ImageProcessorModule,
    ActivityModule,
    S3Module,
    SessionsModule,
    SporeModule,
    ChatKittyModule,
  ],
  providers: [ChatExperienceService, ChatExperienceResolver],
  exports: [ChatExperienceService],
})
export class ChatExperienceModule {}
