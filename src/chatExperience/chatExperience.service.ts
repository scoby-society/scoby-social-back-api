import { Injectable } from '@nestjs/common';
import { ActivityServices } from 'src/activity/activity.service';
import { S3Service } from 'src/lib/s3/s3.service';
import {
  ImageProcessorService,
  ImageTargetType,
} from '../lib/image-processor/image-processor.service';
import { NotificationsService } from '../notifications/notifications.service';
import { SporeService } from 'src/spore-nft/spore.service';
import { ChatExperience } from './chatExperience.entity';
import { getConnection, getRepository } from 'typeorm';
import {
  ChatExperienceObject,
  ChatExperienceUpdate,
  CreateChatExperience,
} from './chatExperience.graphql';
import { FileUpload } from '../lib/common/common.interfaces';
import { SporeDs } from 'src/spore-nft/spore-ds.entity';
import { SessionsService } from 'src/sessions/sessions.service';
import { ExperienceChatType } from './chat.types';
import { CHAT_ERRORS } from './chatExperience.messages';
import { ApolloError } from 'apollo-server-express';
import { User } from 'src/users/user.entity';
import { ChatKittyService } from 'src/lib/chatKitty/chatKitty.service';
import { RegistrationStatusEnum } from 'src/users/registration-status.entity';
import { USERS_ERRORS } from 'src/users/users.messages';
import { Topic } from '../topics/topic.entity';
import { Projects } from 'src/projects/projects.entity';
import { ActivityActionTypes } from 'src/activity/activity.types';

@Injectable()
export class ChatExperienceService {
  constructor(
    private activityServices: ActivityServices,
    private s3Service: S3Service,
    private imageProcessorService: ImageProcessorService,
    private notificationsService: NotificationsService,
    private sporeService: SporeService,
    private sessionsService: SessionsService,
    private chatKittyService: ChatKittyService,
  ) {}

  async createChat(
    currentUserId: number,
    chat: CreateChatExperience,
    avatar?: FileUpload,
    backgroundImage?: FileUpload,
  ): Promise<ChatExperienceObject> {
    const repository = getRepository(ChatExperience);
    const {
      title,
      description,
      invitedUsers,
      isTeamsMembersInvited,
      teamInvitation,
      nft,
      nftsRequired,
      nftsInviteUser,
      chatType,
      projectId,
    } = chat;

    const project = await getRepository(Projects).findOne({
      where: { id: projectId },
    });

    const nftObject = nft ? await getRepository(SporeDs).findOne(nft) : null;
    if (nftsRequired) {
      await this.sporeService.verifyCollectionNfts(nftsRequired);
    }

    const { userIds } = await this.sessionsService.getInvitedUsers(
      invitedUsers,
      currentUserId,
      isTeamsMembersInvited,
      teamInvitation,
      nftsInviteUser
        ? ((await this.sporeService.idUserCollectionNftByIds(
            nftsInviteUser,
          )) as [number])
        : null,
    );

    const topicsCollection = chat.topics.map((topicId) => ({ id: topicId }));

    const savedChat = await repository.save({
      title,
      description,
      topics: topicsCollection,
      chatType: this.getChatType(project?.join || ExperienceChatType.OPEN),
      ownerUser: {
        id: currentUserId,
      },
      nft: nftObject,
      nftsRequired,
      projects: project,
    });

    this.activityServices.sendGroupNotificationActivity(
      userIds,
      savedChat.id,
      currentUserId,
      ActivityActionTypes.CREATE_CHANNEL,
    );

    await this.uploadFileChat(
      currentUserId,
      savedChat.id,
      avatar,
      backgroundImage,
    );

    return (await repository.findOne(savedChat.id, {
      relations: ['ownerUser'],
    })) as ChatExperienceObject;
  }

  async editChatExperience(
    currentUserId: number,
    idChat: number,
    chat: ChatExperienceUpdate,
    avatar?: FileUpload,
    backgroundImage?: FileUpload,
  ): Promise<ChatExperienceObject> {
    const repository = getRepository(ChatExperience);
    const chatRepository = await repository.findOne(idChat, {
      relations: ['ownerUser'],
    });

    if (!chatRepository) {
      throw new ApolloError(
        CHAT_ERRORS.CHAT_NOT_FOUND.MESSAGE,
        CHAT_ERRORS.CHAT_NOT_FOUND.CODE,
      );
    }

    if (chatRepository.ownerUser.id !== currentUserId) {
      throw new ApolloError(
        CHAT_ERRORS.NOT_CHAT_OWNER.MESSAGE,
        CHAT_ERRORS.NOT_CHAT_OWNER.CODE,
      );
    }

    await this.uploadFileChat(
      currentUserId,
      chatRepository.id,
      avatar,
      backgroundImage,
    );

    const topicsCollection = chat.topics.map((topicId) => ({ id: topicId }));
    return await repository.save({
      id: idChat,
      title: chat.title,
      description: chat.description,
      topics: topicsCollection,
    });
  }

  async uploadFileChat(
    currentUserId: number,
    idChat: number,
    avatar?: FileUpload,
    backgroundImage?: FileUpload,
  ): Promise<ChatExperienceObject> {
    const repository = getRepository(ChatExperience);
    const chat = (await repository.findOne(idChat, {
      relations: ['ownerUser'],
    })) as ChatExperienceObject;

    if (!chat) {
      throw new ApolloError(
        CHAT_ERRORS.CHAT_NOT_FOUND.MESSAGE,
        CHAT_ERRORS.CHAT_NOT_FOUND.CODE,
      );
    }

    if (chat.ownerUser.id !== currentUserId) {
      throw new ApolloError(
        CHAT_ERRORS.NOT_CHAT_OWNER.MESSAGE,
        CHAT_ERRORS.NOT_CHAT_OWNER.CODE,
      );
    }

    const removeAssets = [];

    if (avatar && chat.avatar) {
      removeAssets.push(chat.avatar);
    }

    if (backgroundImage && chat.backgroundImage) {
      removeAssets.push(chat.backgroundImage);
    }

    if (removeAssets.length) {
      await this.s3Service.removeFiles(removeAssets);
    }

    let avatarUpload;
    let backgroundImageUpload;

    if (avatar) {
      avatarUpload = this.imageProcessorService.optimizeImage(
        avatar.createReadStream(),
        ImageTargetType.AVATAR,
      );
    }

    if (backgroundImage) {
      backgroundImageUpload = this.imageProcessorService.optimizeImage(
        backgroundImage.createReadStream(),
        ImageTargetType.BACKGROUND_IMAGE,
      );
    }

    const [avatarResult, backgroundImageResult] = await Promise.all([
      avatarUpload
        ? this.s3Service.uploadFile({
            extension: avatarUpload.extension,
            mime: avatarUpload.mime,
            stream: avatarUpload.stream,
          })
        : undefined,
      backgroundImageUpload
        ? this.s3Service.uploadFile({
            extension: backgroundImageUpload.extension,
            mime: backgroundImageUpload.mime,
            stream: backgroundImageUpload.stream,
          })
        : undefined,
    ]);

    await repository.save({
      id: idChat,
      avatar: avatarResult?.Key,
      backgroundImage: backgroundImageResult?.Key,
    });

    return (await repository.findOne(idChat)) as ChatExperienceObject;
  }

  getChatType(type: string): ExperienceChatType {
    this.sessionsService.verifyTypeExperience(type);
    if (type === 'Open') {
      return ExperienceChatType.OPEN;
    } else if (type === 'Closed') {
      return ExperienceChatType.CLOSED;
    } else {
      return ExperienceChatType.EXCLUSIVE;
    }
  }

  async getChatById(id: number): Promise<ChatExperienceObject> {
    const repository = getRepository(ChatExperience);
    const chat = await repository.findOne(id, {
      relations: ['nft', 'topics','projects'],
    });

    if (!chat) {
      throw new ApolloError(
        CHAT_ERRORS.CHAT_NOT_FOUND.MESSAGE,
        CHAT_ERRORS.CHAT_NOT_FOUND.CODE,
      );
    }

    return {
      ...chat,
    };
  }

  async getChatExperienceUser(idUser: number): Promise<ChatExperienceObject[]> {
    return await getRepository(ChatExperience)
      .createQueryBuilder('chat')
      .where('chat.ownerUser = :id', { id: idUser })
      .andWhere('chat.finishedAt is null')
      .orderBy('chat.createdAt', 'DESC')
      .getMany();
  }

  async getToken(): Promise<string> {
    const token = await this.chatKittyService.getToken();
    return token.headers.Authorization;
  }

  async getChatPaging(
    limit: number,
    page: number,
  ): Promise<ChatExperienceObject[]> {
    return await getRepository(ChatExperience)
      .createQueryBuilder('chat')
      .leftJoinAndSelect('chat.suscribeUsers', 'suscribeUsers')
      .leftJoinAndSelect('chat.topics', 'topics')
      .where('chat.finishedAt is null')
      .skip(limit * (page - 1))
      .take(limit)
      .orderBy('chat.id', 'DESC')
      .getMany();
  }

  async getAllChatExperience(): Promise<ChatExperienceObject[]> {
    return await getRepository(ChatExperience)
      .createQueryBuilder('chat')
      .leftJoinAndSelect('chat.suscribeUsers', 'suscribeUsers')
      .leftJoinAndSelect('chat.topics', 'topics')
      .where('chat.finishedAt is null')
      .orderBy('chat.createdAt', 'DESC')
      .getMany();
  }

  async getChatOwner(id: number): Promise<User> {
    return getConnection()
      .createQueryBuilder()
      .relation(ChatExperience, 'ownerUser')
      .of(id)
      .loadOne() as Promise<User>;
  }

  async subscribed(idChat: number, currentUserId: number): Promise<boolean> {
    const repository = getRepository(ChatExperience);
    const { suscribeUsers } = (await repository.findOne(idChat, {
      relations: ['suscribeUsers'],
    })) as ChatExperienceObject;
    return [...suscribeUsers].some((User) => User.id === currentUserId);
  }

  async updateAllUserKitty(page: number): Promise<void> {
    await this.chatKittyService.updateAllUsers(page);
  }

  async createUsersInChat(): Promise<void> {
    const users = await getRepository(User)
      .createQueryBuilder('user')
      .where('user.registrationStatus = :status ', {
        status: RegistrationStatusEnum.COMPLETED,
      })
      .orderBy('user.id', 'DESC')
      .getMany();

    for (const user of users) {
      await this.chatKittyService.createUser(user);
    }
  }

  async joinChat(
    currentUserId: number,
    idChat: number,
  ): Promise<ChatExperienceObject> {
    const repository = getRepository(ChatExperience);
    const userRepository = getRepository(User);
    const chat = await repository.findOne(idChat, {
      relations: ['ownerUser', 'suscribeUsers'],
    });
    const userToAdd = await userRepository.findOne(currentUserId);

    if (!chat) {
      throw new ApolloError(
        CHAT_ERRORS.CHAT_NOT_FOUND.MESSAGE,
        CHAT_ERRORS.CHAT_NOT_FOUND.CODE,
      );
    }

    if (!userToAdd) {
      throw new ApolloError(
        USERS_ERRORS.USER_NOT_FOUND.MESSAGE,
        USERS_ERRORS.USER_NOT_FOUND.CODE,
      );
    }

    if (chat.finishedAt) {
      throw new ApolloError(
        CHAT_ERRORS.CHAT_FINISHED.MESSAGE,
        CHAT_ERRORS.CHAT_FINISHED.CODE,
      );
    }

    const suscribeUsers = chat.suscribeUsers || [];
    suscribeUsers.push(userToAdd as User);

    await repository.save({ id: idChat, suscribeUsers });
    return (await repository.findOne(idChat, {
      relations: ['ownerUser'],
    })) as ChatExperienceObject;
  }

  async leaveChat(
    currentUserId: number,
    idChat: number,
  ): Promise<ChatExperienceObject> {
    const repository = getRepository(ChatExperience);
    const chat = await repository.findOne(idChat, {
      relations: ['ownerUser', 'suscribeUsers'],
    });
    if (!chat) {
      throw new ApolloError(
        CHAT_ERRORS.CHAT_NOT_FOUND.MESSAGE,
        CHAT_ERRORS.CHAT_NOT_FOUND.CODE,
      );
    }
    const suscribeUsers = chat.suscribeUsers.filter(
      (user) => user.id !== currentUserId,
    );
    await repository.save({ id: idChat, suscribeUsers });

    return (await repository.findOne(idChat)) as ChatExperience;
  }

  async endChat(
    currentUserId: number,
    idChat: number,
  ): Promise<ChatExperienceObject> {
    const repository = getRepository(ChatExperience);
    const chat = await repository.findOne(idChat, {
      relations: ['ownerUser'],
    });
    if (!chat) {
      throw new ApolloError(
        CHAT_ERRORS.CHAT_NOT_FOUND.MESSAGE,
        CHAT_ERRORS.CHAT_NOT_FOUND.CODE,
      );
    }

    if (chat.ownerUser.id !== currentUserId) {
      throw new ApolloError(
        CHAT_ERRORS.NOT_CHAT_OWNER.MESSAGE,
        CHAT_ERRORS.NOT_CHAT_OWNER.CODE,
      );
    }

    if (chat.finishedAt) {
      throw new ApolloError(
        CHAT_ERRORS.CHAT_FINISHED.MESSAGE,
        CHAT_ERRORS.CHAT_FINISHED.CODE,
      );
    }

    const finishedAt = new Date(Date.now());
    return await repository.save({ id: idChat, finishedAt });
  }

  async getTopics(id: number): Promise<Topic[]> {
    return getConnection()
      .createQueryBuilder()
      .relation(ChatExperience, 'topics')
      .of(id)
      .loadMany();
  }

  async getsuscribeUsers(id: number): Promise<User[]> {
    return getConnection()
      .createQueryBuilder()
      .relation(ChatExperience, 'suscribeUsers')
      .of(id)
      .loadMany();
  }

  async sendNotificationChannel(
    idChannel: number,
    idUsers: number[],
  ): Promise<ChatExperienceObject> {
    const repository = getRepository(ChatExperience);
    const chat = await repository.findOne(idChannel, {
      relations: ['ownerUser'],
    });

    if (!chat) {
      throw new ApolloError(
        CHAT_ERRORS.CHAT_NOT_FOUND.MESSAGE,
        CHAT_ERRORS.CHAT_NOT_FOUND.CODE,
      );
    }

    this.notificationsService.sendChannelCreatedNotifications(
      idChannel,
      chat.ownerUser.id,
      idUsers as [number],
    );

    return chat;
  }

  async isAvailable(
    experienceSecrets: number[],
    chat: ChatExperience,
    idUser: number,
  ): Promise<boolean> {
    if (chat.chatType === ExperienceChatType.CLOSED) {
      const isAvailable = experienceSecrets.find(
        (element) => element === chat.id,
      )
        ? true
        : false;
      if (isAvailable) {
        return true;
      }

      return await this.IsUserHasCollection(idUser, chat);
    }

    if (chat.chatType === ExperienceChatType.EXCLUSIVE) {
      return await this.IsUserHasCollection(idUser, chat);
    }

    return true;
  }

  async IsUserHasCollection(
    idUser: number,
    chat: ChatExperience,
  ): Promise<boolean> {
    if (chat.projects) {
      const project = await getRepository(Projects).findOne(chat.projects.id, {
        relations: ['nft'],
      });
      if (!project || !project.nft) {
        return true;
      }
      let isNftRequired = true;
      const idNftsRequired = project.nft.map((nft) => nft.id);

      const nameCollections = await this.sporeService.getNameCollectionNfts(
        idNftsRequired,
      );

      const idUserNft: number[] = [];
      for (const collection of nameCollections) {
        idUserNft.push(
          ...(await this.sporeService.idUserCollectionNft(collection || '')),
        );
      }

      isNftRequired = idUserNft.find((element) => element === idUser)
        ? true
        : false;

      if (!isNftRequired) {
        return false;
      } else {
        return true;
      }
    }
    return true;
  }

  async getSecretExperiences(idUser: number): Promise<number[]> {
    const experiences = await getRepository(ChatExperience)
      .createQueryBuilder('chat')
      .where('chat.finishedAt is null')
      .getMany();

    const idExperiences = experiences.map((chat) => chat.id);

    return await this.sessionsService.getInvitationsExperiences(
      idExperiences,
      idUser,
      'channel',
    );
  }
}
