export enum ExperienceChatType {
  OPEN = 'Open',
  CLOSED = 'Closed',
  EXCLUSIVE = 'Exclusive',
}
