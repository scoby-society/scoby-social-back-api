FROM node:14.20.0

WORKDIR /app
ADD . /app

RUN yarn install
RUN yarn build

EXPOSE 3000
CMD ["yarn", "start:prod"]
